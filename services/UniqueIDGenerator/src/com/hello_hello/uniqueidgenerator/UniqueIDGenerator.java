/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.uniqueidgenerator;

/* Import the necessary packages*/
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

@ExposeToClient
public class UniqueIDGenerator {

    private static final Logger logger = LoggerFactory.getLogger(UniqueIDGenerator.class);

    /* Autowire the Security Service*/
    @Autowired
    private SecurityService securityService;
    
    public String getUUID(HttpServletRequest request){
        
        // Have a DB entity that maintains userName vs unique ID
        
        /* Fetch the logged in user's userName from the SecurityService*/
    	String userName = securityService.getUserName();
    	
    	//check if the unique Id for the respective userName fetched above exists. 
    	
    	// in case the unique ID does not exist for the fetched userName, create it using HTTPServletRequest and return it
    	return request.getSession(true).getId();
    }

}
