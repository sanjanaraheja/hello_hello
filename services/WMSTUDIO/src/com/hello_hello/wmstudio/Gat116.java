/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Gat116 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`GAT116`")
@IdClass(Gat116Id.class)
public class Gat116 implements Serializable {

    private String ordnum;
    private String gmtseq;
    private Byte itemseq;
    private Byte pddszeseq;
    private Byte brkseq;
    private Byte szeseq;
    private String acsdim;
    private Float nopce;
    private String position;
    private String usrid;
    private LocalDateTime usrdate;
    private String cmbflag;

    @Id
    @Column(name = "`ORDNUM`", nullable = false, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Id
    @Column(name = "`GMTSEQ`", nullable = false, length = 2)
    public String getGmtseq() {
        return this.gmtseq;
    }

    public void setGmtseq(String gmtseq) {
        this.gmtseq = gmtseq;
    }

    @Id
    @Column(name = "`ITEMSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getItemseq() {
        return this.itemseq;
    }

    public void setItemseq(Byte itemseq) {
        this.itemseq = itemseq;
    }

    @Id
    @Column(name = "`PDDSZESEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getPddszeseq() {
        return this.pddszeseq;
    }

    public void setPddszeseq(Byte pddszeseq) {
        this.pddszeseq = pddszeseq;
    }

    @Id
    @Column(name = "`BRKSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getBrkseq() {
        return this.brkseq;
    }

    public void setBrkseq(Byte brkseq) {
        this.brkseq = brkseq;
    }

    @Column(name = "`SZESEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getSzeseq() {
        return this.szeseq;
    }

    public void setSzeseq(Byte szeseq) {
        this.szeseq = szeseq;
    }

    @Column(name = "`ACSDIM`", nullable = true, length = 10)
    public String getAcsdim() {
        return this.acsdim;
    }

    public void setAcsdim(String acsdim) {
        this.acsdim = acsdim;
    }

    @Column(name = "`NOPCE`", nullable = true, scale = 1, precision = 2)
    public Float getNopce() {
        return this.nopce;
    }

    public void setNopce(Float nopce) {
        this.nopce = nopce;
    }

    @Column(name = "`POSITION`", nullable = true, length = 4)
    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Column(name = "`USRID`", nullable = true, length = 6)
    public String getUsrid() {
        return this.usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    @Column(name = "`USRDATE`", nullable = true)
    public LocalDateTime getUsrdate() {
        return this.usrdate;
    }

    public void setUsrdate(LocalDateTime usrdate) {
        this.usrdate = usrdate;
    }

    @Column(name = "`CMBFLAG`", nullable = true, length = 2)
    public String getCmbflag() {
        return this.cmbflag;
    }

    public void setCmbflag(String cmbflag) {
        this.cmbflag = cmbflag;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gat116)) return false;
        final Gat116 gat116 = (Gat116) o;
        return Objects.equals(getOrdnum(), gat116.getOrdnum()) &&
                Objects.equals(getGmtseq(), gat116.getGmtseq()) &&
                Objects.equals(getItemseq(), gat116.getItemseq()) &&
                Objects.equals(getPddszeseq(), gat116.getPddszeseq()) &&
                Objects.equals(getBrkseq(), gat116.getBrkseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getGmtseq(),
                getItemseq(),
                getPddszeseq(),
                getBrkseq());
    }
}

