/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Ttt350Id implements Serializable {

    private String intrefno;
    private String shadeopt;

    public String getIntrefno() {
        return this.intrefno;
    }

    public void setIntrefno(String intrefno) {
        this.intrefno = intrefno;
    }

    public String getShadeopt() {
        return this.shadeopt;
    }

    public void setShadeopt(String shadeopt) {
        this.shadeopt = shadeopt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ttt350)) return false;
        final Ttt350 ttt350 = (Ttt350) o;
        return Objects.equals(getIntrefno(), ttt350.getIntrefno()) &&
                Objects.equals(getShadeopt(), ttt350.getShadeopt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIntrefno(),
                getShadeopt());
    }
}
