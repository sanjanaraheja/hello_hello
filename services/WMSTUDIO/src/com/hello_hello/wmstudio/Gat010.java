/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Gat010 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`GAT010`")
public class Gat010 implements Serializable {

    private String dyemethodcod;
    private String dyesubtype;
    private String methoddes;
    private String bathdye;
    private String contdye;
    private String potdye;
    private String notdye;
    private String sts;
    private String intdyeingrem;
    private String nonskuUseAllowed;
    private String skuUseAllowed;
    private Byte priority;
    private String fbulkmatchreq;
    private String coldpaddye;

    @Id
    @Column(name = "`DYEMETHODCOD`", nullable = false, length = 10)
    public String getDyemethodcod() {
        return this.dyemethodcod;
    }

    public void setDyemethodcod(String dyemethodcod) {
        this.dyemethodcod = dyemethodcod;
    }

    @Column(name = "`DYESUBTYPE`", nullable = true, length = 50)
    public String getDyesubtype() {
        return this.dyesubtype;
    }

    public void setDyesubtype(String dyesubtype) {
        this.dyesubtype = dyesubtype;
    }

    @Column(name = "`METHODDES`", nullable = true, length = 50)
    public String getMethoddes() {
        return this.methoddes;
    }

    public void setMethoddes(String methoddes) {
        this.methoddes = methoddes;
    }

    @Column(name = "`BATHDYE`", nullable = true, length = 1)
    public String getBathdye() {
        return this.bathdye;
    }

    public void setBathdye(String bathdye) {
        this.bathdye = bathdye;
    }

    @Column(name = "`CONTDYE`", nullable = true, length = 1)
    public String getContdye() {
        return this.contdye;
    }

    public void setContdye(String contdye) {
        this.contdye = contdye;
    }

    @Column(name = "`POTDYE`", nullable = true, length = 1)
    public String getPotdye() {
        return this.potdye;
    }

    public void setPotdye(String potdye) {
        this.potdye = potdye;
    }

    @Column(name = "`NOTDYE`", nullable = true, length = 1)
    public String getNotdye() {
        return this.notdye;
    }

    public void setNotdye(String notdye) {
        this.notdye = notdye;
    }

    @Column(name = "`STS`", nullable = true, length = 1)
    public String getSts() {
        return this.sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    @Column(name = "`INTDYEINGREM`", nullable = true, length = 200)
    public String getIntdyeingrem() {
        return this.intdyeingrem;
    }

    public void setIntdyeingrem(String intdyeingrem) {
        this.intdyeingrem = intdyeingrem;
    }

    @Column(name = "`NONSKU_USE_ALLOWED`", nullable = true, length = 1)
    public String getNonskuUseAllowed() {
        return this.nonskuUseAllowed;
    }

    public void setNonskuUseAllowed(String nonskuUseAllowed) {
        this.nonskuUseAllowed = nonskuUseAllowed;
    }

    @Column(name = "`SKU_USE_ALLOWED`", nullable = true, length = 1)
    public String getSkuUseAllowed() {
        return this.skuUseAllowed;
    }

    public void setSkuUseAllowed(String skuUseAllowed) {
        this.skuUseAllowed = skuUseAllowed;
    }

    @Column(name = "`PRIORITY`", nullable = true, scale = 0, precision = 2)
    public Byte getPriority() {
        return this.priority;
    }

    public void setPriority(Byte priority) {
        this.priority = priority;
    }

    @Column(name = "`FBULKMATCHREQ`", nullable = true, length = 1)
    public String getFbulkmatchreq() {
        return this.fbulkmatchreq;
    }

    public void setFbulkmatchreq(String fbulkmatchreq) {
        this.fbulkmatchreq = fbulkmatchreq;
    }

    @Column(name = "`COLDPADDYE`", nullable = true, length = 1)
    public String getColdpaddye() {
        return this.coldpaddye;
    }

    public void setColdpaddye(String coldpaddye) {
        this.coldpaddye = coldpaddye;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gat010)) return false;
        final Gat010 gat010 = (Gat010) o;
        return Objects.equals(getDyemethodcod(), gat010.getDyemethodcod());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDyemethodcod());
    }
}

