/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Drt520Id implements Serializable {

    private String clrref;
    private Short cornum;

    public String getClrref() {
        return this.clrref;
    }

    public void setClrref(String clrref) {
        this.clrref = clrref;
    }

    public Short getCornum() {
        return this.cornum;
    }

    public void setCornum(Short cornum) {
        this.cornum = cornum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Drt520)) return false;
        final Drt520 drt520 = (Drt520) o;
        return Objects.equals(getClrref(), drt520.getClrref()) &&
                Objects.equals(getCornum(), drt520.getCornum());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClrref(),
                getCornum());
    }
}
