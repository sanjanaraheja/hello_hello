/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Ort160Id implements Serializable {

    private String ordnum;
    private Byte clrseq;
    private Byte fabseq;

    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    public Byte getClrseq() {
        return this.clrseq;
    }

    public void setClrseq(Byte clrseq) {
        this.clrseq = clrseq;
    }

    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ort160)) return false;
        final Ort160 ort160 = (Ort160) o;
        return Objects.equals(getOrdnum(), ort160.getOrdnum()) &&
                Objects.equals(getClrseq(), ort160.getClrseq()) &&
                Objects.equals(getFabseq(), ort160.getFabseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getClrseq(),
                getFabseq());
    }
}
