/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Gat114 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`GAT114`")
@IdClass(Gat114Id.class)
public class Gat114 implements Serializable {

    private String ordnum;
    private String gmtseq;
    private Byte itemseq;
    private String itemcod;
    private String position;
    private String dimen1;
    private String dimen2;
    private String dimen3;
    private String dimen4;
    private String dimen5;
    private String dimen6;
    private String dimen7;
    private String dimen8;
    private String dimen9;
    private String dimen10;
    private String dimen11;
    private String dimen12;
    private String dimen13;
    private String dimen14;
    private String dimen15;
    private String dimen16;
    private String usrid;
    private LocalDateTime usrdate;
    private String sts;
    private Byte usrsortseq;

    @Id
    @Column(name = "`ORDNUM`", nullable = false, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Id
    @Column(name = "`GMTSEQ`", nullable = false, length = 2)
    public String getGmtseq() {
        return this.gmtseq;
    }

    public void setGmtseq(String gmtseq) {
        this.gmtseq = gmtseq;
    }

    @Id
    @Column(name = "`ITEMSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getItemseq() {
        return this.itemseq;
    }

    public void setItemseq(Byte itemseq) {
        this.itemseq = itemseq;
    }

    @Column(name = "`ITEMCOD`", nullable = true, length = 5)
    public String getItemcod() {
        return this.itemcod;
    }

    public void setItemcod(String itemcod) {
        this.itemcod = itemcod;
    }

    @Column(name = "`POSITION`", nullable = true, length = 4)
    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Column(name = "`DIMEN1`", nullable = true, length = 10)
    public String getDimen1() {
        return this.dimen1;
    }

    public void setDimen1(String dimen1) {
        this.dimen1 = dimen1;
    }

    @Column(name = "`DIMEN2`", nullable = true, length = 10)
    public String getDimen2() {
        return this.dimen2;
    }

    public void setDimen2(String dimen2) {
        this.dimen2 = dimen2;
    }

    @Column(name = "`DIMEN3`", nullable = true, length = 10)
    public String getDimen3() {
        return this.dimen3;
    }

    public void setDimen3(String dimen3) {
        this.dimen3 = dimen3;
    }

    @Column(name = "`DIMEN4`", nullable = true, length = 10)
    public String getDimen4() {
        return this.dimen4;
    }

    public void setDimen4(String dimen4) {
        this.dimen4 = dimen4;
    }

    @Column(name = "`DIMEN5`", nullable = true, length = 10)
    public String getDimen5() {
        return this.dimen5;
    }

    public void setDimen5(String dimen5) {
        this.dimen5 = dimen5;
    }

    @Column(name = "`DIMEN6`", nullable = true, length = 10)
    public String getDimen6() {
        return this.dimen6;
    }

    public void setDimen6(String dimen6) {
        this.dimen6 = dimen6;
    }

    @Column(name = "`DIMEN7`", nullable = true, length = 10)
    public String getDimen7() {
        return this.dimen7;
    }

    public void setDimen7(String dimen7) {
        this.dimen7 = dimen7;
    }

    @Column(name = "`DIMEN8`", nullable = true, length = 10)
    public String getDimen8() {
        return this.dimen8;
    }

    public void setDimen8(String dimen8) {
        this.dimen8 = dimen8;
    }

    @Column(name = "`DIMEN9`", nullable = true, length = 10)
    public String getDimen9() {
        return this.dimen9;
    }

    public void setDimen9(String dimen9) {
        this.dimen9 = dimen9;
    }

    @Column(name = "`DIMEN10`", nullable = true, length = 10)
    public String getDimen10() {
        return this.dimen10;
    }

    public void setDimen10(String dimen10) {
        this.dimen10 = dimen10;
    }

    @Column(name = "`DIMEN11`", nullable = true, length = 10)
    public String getDimen11() {
        return this.dimen11;
    }

    public void setDimen11(String dimen11) {
        this.dimen11 = dimen11;
    }

    @Column(name = "`DIMEN12`", nullable = true, length = 10)
    public String getDimen12() {
        return this.dimen12;
    }

    public void setDimen12(String dimen12) {
        this.dimen12 = dimen12;
    }

    @Column(name = "`DIMEN13`", nullable = true, length = 10)
    public String getDimen13() {
        return this.dimen13;
    }

    public void setDimen13(String dimen13) {
        this.dimen13 = dimen13;
    }

    @Column(name = "`DIMEN14`", nullable = true, length = 10)
    public String getDimen14() {
        return this.dimen14;
    }

    public void setDimen14(String dimen14) {
        this.dimen14 = dimen14;
    }

    @Column(name = "`DIMEN15`", nullable = true, length = 10)
    public String getDimen15() {
        return this.dimen15;
    }

    public void setDimen15(String dimen15) {
        this.dimen15 = dimen15;
    }

    @Column(name = "`DIMEN16`", nullable = true, length = 10)
    public String getDimen16() {
        return this.dimen16;
    }

    public void setDimen16(String dimen16) {
        this.dimen16 = dimen16;
    }

    @Column(name = "`USRID`", nullable = true, length = 6)
    public String getUsrid() {
        return this.usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    @Column(name = "`USRDATE`", nullable = true)
    public LocalDateTime getUsrdate() {
        return this.usrdate;
    }

    public void setUsrdate(LocalDateTime usrdate) {
        this.usrdate = usrdate;
    }

    @Column(name = "`STS`", nullable = true, length = 3)
    public String getSts() {
        return this.sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    @Column(name = "`USRSORTSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getUsrsortseq() {
        return this.usrsortseq;
    }

    public void setUsrsortseq(Byte usrsortseq) {
        this.usrsortseq = usrsortseq;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gat114)) return false;
        final Gat114 gat114 = (Gat114) o;
        return Objects.equals(getOrdnum(), gat114.getOrdnum()) &&
                Objects.equals(getGmtseq(), gat114.getGmtseq()) &&
                Objects.equals(getItemseq(), gat114.getItemseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getGmtseq(),
                getItemseq());
    }
}

