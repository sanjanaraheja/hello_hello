/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Ymt026Id implements Serializable {

    private String nyrnclr;
    private String yrnshade;

    public String getNyrnclr() {
        return this.nyrnclr;
    }

    public void setNyrnclr(String nyrnclr) {
        this.nyrnclr = nyrnclr;
    }

    public String getYrnshade() {
        return this.yrnshade;
    }

    public void setYrnshade(String yrnshade) {
        this.yrnshade = yrnshade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ymt026)) return false;
        final Ymt026 ymt026 = (Ymt026) o;
        return Objects.equals(getNyrnclr(), ymt026.getNyrnclr()) &&
                Objects.equals(getYrnshade(), ymt026.getYrnshade());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNyrnclr(),
                getYrnshade());
    }
}
