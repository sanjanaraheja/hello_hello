/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Ort137;
import com.hello_hello.wmstudio.Ort137Id;
import com.hello_hello.wmstudio.service.Ort137Service;


/**
 * Controller object for domain model class Ort137.
 * @see Ort137
 */
@RestController("WMSTUDIO.Ort137Controller")
@Api(value = "Ort137Controller", description = "Exposes APIs to work with Ort137 resource.")
@RequestMapping("/WMSTUDIO/Ort137")
public class Ort137Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ort137Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Ort137Service")
	private Ort137Service ort137Service;

	@ApiOperation(value = "Creates a new Ort137 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort137 createOrt137(@RequestBody Ort137 ort137) {
		LOGGER.debug("Create Ort137 with information: {}" , ort137);

		ort137 = ort137Service.create(ort137);
		LOGGER.debug("Created Ort137 with information: {}" , ort137);

	    return ort137;
	}

@ApiOperation(value = "Returns the Ort137 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort137 getOrt137(@RequestParam("ordnum") String ordnum,@RequestParam("animgp") String animgp,@RequestParam("pancod") String pancod,@RequestParam("scncod") String scncod,@RequestParam("proccode") String proccode) throws EntityNotFoundException {

        Ort137Id ort137Id = new Ort137Id();
        ort137Id.setOrdnum(ordnum);
        ort137Id.setAnimgp(animgp);
        ort137Id.setPancod(pancod);
        ort137Id.setScncod(scncod);
        ort137Id.setProccode(proccode);

        LOGGER.debug("Getting Ort137 with id: {}" , ort137Id);
        Ort137 ort137 = ort137Service.getById(ort137Id);
        LOGGER.debug("Ort137 details with id: {}" , ort137);

        return ort137;
    }



    @ApiOperation(value = "Updates the Ort137 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort137 editOrt137(@RequestParam("ordnum") String ordnum,@RequestParam("animgp") String animgp,@RequestParam("pancod") String pancod,@RequestParam("scncod") String scncod,@RequestParam("proccode") String proccode, @RequestBody Ort137 ort137) throws EntityNotFoundException {

        ort137.setOrdnum(ordnum);
        ort137.setAnimgp(animgp);
        ort137.setPancod(pancod);
        ort137.setScncod(scncod);
        ort137.setProccode(proccode);

        LOGGER.debug("Ort137 details with id is updated with: {}" , ort137);

        return ort137Service.update(ort137);
    }


    @ApiOperation(value = "Deletes the Ort137 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteOrt137(@RequestParam("ordnum") String ordnum,@RequestParam("animgp") String animgp,@RequestParam("pancod") String pancod,@RequestParam("scncod") String scncod,@RequestParam("proccode") String proccode) throws EntityNotFoundException {

        Ort137Id ort137Id = new Ort137Id();
        ort137Id.setOrdnum(ordnum);
        ort137Id.setAnimgp(animgp);
        ort137Id.setPancod(pancod);
        ort137Id.setScncod(scncod);
        ort137Id.setProccode(proccode);

        LOGGER.debug("Deleting Ort137 with id: {}" , ort137Id);
        Ort137 ort137 = ort137Service.delete(ort137Id);

        return ort137 != null;
    }


    /**
     * @deprecated Use {@link #findOrt137s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Ort137 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort137> searchOrt137sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Ort137s list");
        return ort137Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort137 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort137> findOrt137s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort137s list");
        return ort137Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort137 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort137> filterOrt137s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort137s list");
        return ort137Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportOrt137s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return ort137Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Ort137 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countOrt137s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Ort137s");
		return ort137Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getOrt137AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return ort137Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Ort137Service instance
	 */
	protected void setOrt137Service(Ort137Service service) {
		this.ort137Service = service;
	}

}

