/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Apt150;
import com.hello_hello.wmstudio.Apt150Id;
import com.hello_hello.wmstudio.service.Apt150Service;


/**
 * Controller object for domain model class Apt150.
 * @see Apt150
 */
@RestController("WMSTUDIO.Apt150Controller")
@Api(value = "Apt150Controller", description = "Exposes APIs to work with Apt150 resource.")
@RequestMapping("/WMSTUDIO/Apt150")
public class Apt150Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Apt150Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Apt150Service")
	private Apt150Service apt150Service;

	@ApiOperation(value = "Creates a new Apt150 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Apt150 createApt150(@RequestBody Apt150 apt150) {
		LOGGER.debug("Create Apt150 with information: {}" , apt150);

		apt150 = apt150Service.create(apt150);
		LOGGER.debug("Created Apt150 with information: {}" , apt150);

	    return apt150;
	}

@ApiOperation(value = "Returns the Apt150 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Apt150 getApt150(@RequestParam("fabdcode") String fabdcode,@RequestParam("fabdcodevar") Short fabdcodevar) throws EntityNotFoundException {

        Apt150Id apt150Id = new Apt150Id();
        apt150Id.setFabdcode(fabdcode);
        apt150Id.setFabdcodevar(fabdcodevar);

        LOGGER.debug("Getting Apt150 with id: {}" , apt150Id);
        Apt150 apt150 = apt150Service.getById(apt150Id);
        LOGGER.debug("Apt150 details with id: {}" , apt150);

        return apt150;
    }



    @ApiOperation(value = "Updates the Apt150 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Apt150 editApt150(@RequestParam("fabdcode") String fabdcode,@RequestParam("fabdcodevar") Short fabdcodevar, @RequestBody Apt150 apt150) throws EntityNotFoundException {

        apt150.setFabdcode(fabdcode);
        apt150.setFabdcodevar(fabdcodevar);

        LOGGER.debug("Apt150 details with id is updated with: {}" , apt150);

        return apt150Service.update(apt150);
    }


    @ApiOperation(value = "Deletes the Apt150 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteApt150(@RequestParam("fabdcode") String fabdcode,@RequestParam("fabdcodevar") Short fabdcodevar) throws EntityNotFoundException {

        Apt150Id apt150Id = new Apt150Id();
        apt150Id.setFabdcode(fabdcode);
        apt150Id.setFabdcodevar(fabdcodevar);

        LOGGER.debug("Deleting Apt150 with id: {}" , apt150Id);
        Apt150 apt150 = apt150Service.delete(apt150Id);

        return apt150 != null;
    }


    /**
     * @deprecated Use {@link #findApt150s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Apt150 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Apt150> searchApt150sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Apt150s list");
        return apt150Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Apt150 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Apt150> findApt150s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Apt150s list");
        return apt150Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Apt150 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Apt150> filterApt150s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Apt150s list");
        return apt150Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportApt150s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return apt150Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Apt150 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countApt150s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Apt150s");
		return apt150Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getApt150AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return apt150Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Apt150Service instance
	 */
	protected void setApt150Service(Apt150Service service) {
		this.apt150Service = service;
	}

}

