/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Ort127;
import com.hello_hello.wmstudio.Ort127Id;
import com.hello_hello.wmstudio.service.Ort127Service;


/**
 * Controller object for domain model class Ort127.
 * @see Ort127
 */
@RestController("WMSTUDIO.Ort127Controller")
@Api(value = "Ort127Controller", description = "Exposes APIs to work with Ort127 resource.")
@RequestMapping("/WMSTUDIO/Ort127")
public class Ort127Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ort127Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Ort127Service")
	private Ort127Service ort127Service;

	@ApiOperation(value = "Creates a new Ort127 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort127 createOrt127(@RequestBody Ort127 ort127) {
		LOGGER.debug("Create Ort127 with information: {}" , ort127);

		ort127 = ort127Service.create(ort127);
		LOGGER.debug("Created Ort127 with information: {}" , ort127);

	    return ort127;
	}

@ApiOperation(value = "Returns the Ort127 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort127 getOrt127(@RequestParam("ordnum") String ordnum,@RequestParam("bdseq") Byte bdseq,@RequestParam("szeseq") Byte szeseq,@RequestParam("fabseq") Byte fabseq) throws EntityNotFoundException {

        Ort127Id ort127Id = new Ort127Id();
        ort127Id.setOrdnum(ordnum);
        ort127Id.setBdseq(bdseq);
        ort127Id.setSzeseq(szeseq);
        ort127Id.setFabseq(fabseq);

        LOGGER.debug("Getting Ort127 with id: {}" , ort127Id);
        Ort127 ort127 = ort127Service.getById(ort127Id);
        LOGGER.debug("Ort127 details with id: {}" , ort127);

        return ort127;
    }



    @ApiOperation(value = "Updates the Ort127 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort127 editOrt127(@RequestParam("ordnum") String ordnum,@RequestParam("bdseq") Byte bdseq,@RequestParam("szeseq") Byte szeseq,@RequestParam("fabseq") Byte fabseq, @RequestBody Ort127 ort127) throws EntityNotFoundException {

        ort127.setOrdnum(ordnum);
        ort127.setBdseq(bdseq);
        ort127.setSzeseq(szeseq);
        ort127.setFabseq(fabseq);

        LOGGER.debug("Ort127 details with id is updated with: {}" , ort127);

        return ort127Service.update(ort127);
    }


    @ApiOperation(value = "Deletes the Ort127 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteOrt127(@RequestParam("ordnum") String ordnum,@RequestParam("bdseq") Byte bdseq,@RequestParam("szeseq") Byte szeseq,@RequestParam("fabseq") Byte fabseq) throws EntityNotFoundException {

        Ort127Id ort127Id = new Ort127Id();
        ort127Id.setOrdnum(ordnum);
        ort127Id.setBdseq(bdseq);
        ort127Id.setSzeseq(szeseq);
        ort127Id.setFabseq(fabseq);

        LOGGER.debug("Deleting Ort127 with id: {}" , ort127Id);
        Ort127 ort127 = ort127Service.delete(ort127Id);

        return ort127 != null;
    }


    /**
     * @deprecated Use {@link #findOrt127s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Ort127 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort127> searchOrt127sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Ort127s list");
        return ort127Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort127 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort127> findOrt127s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort127s list");
        return ort127Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort127 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort127> filterOrt127s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort127s list");
        return ort127Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportOrt127s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return ort127Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Ort127 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countOrt127s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Ort127s");
		return ort127Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getOrt127AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return ort127Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Ort127Service instance
	 */
	protected void setOrt127Service(Ort127Service service) {
		this.ort127Service = service;
	}

}

