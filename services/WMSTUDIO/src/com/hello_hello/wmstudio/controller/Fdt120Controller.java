/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Fdt120;
import com.hello_hello.wmstudio.Fdt120Id;
import com.hello_hello.wmstudio.service.Fdt120Service;


/**
 * Controller object for domain model class Fdt120.
 * @see Fdt120
 */
@RestController("WMSTUDIO.Fdt120Controller")
@Api(value = "Fdt120Controller", description = "Exposes APIs to work with Fdt120 resource.")
@RequestMapping("/WMSTUDIO/Fdt120")
public class Fdt120Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Fdt120Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Fdt120Service")
	private Fdt120Service fdt120Service;

	@ApiOperation(value = "Creates a new Fdt120 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Fdt120 createFdt120(@RequestBody Fdt120 fdt120) {
		LOGGER.debug("Create Fdt120 with information: {}" , fdt120);

		fdt120 = fdt120Service.create(fdt120);
		LOGGER.debug("Created Fdt120 with information: {}" , fdt120);

	    return fdt120;
	}

@ApiOperation(value = "Returns the Fdt120 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Fdt120 getFdt120(@RequestParam("ordnum") String ordnum,@RequestParam("fabseq") Byte fabseq,@RequestParam("clrseq") Byte clrseq,@RequestParam("fabinstcode") String fabinstcode) throws EntityNotFoundException {

        Fdt120Id fdt120Id = new Fdt120Id();
        fdt120Id.setOrdnum(ordnum);
        fdt120Id.setFabseq(fabseq);
        fdt120Id.setClrseq(clrseq);
        fdt120Id.setFabinstcode(fabinstcode);

        LOGGER.debug("Getting Fdt120 with id: {}" , fdt120Id);
        Fdt120 fdt120 = fdt120Service.getById(fdt120Id);
        LOGGER.debug("Fdt120 details with id: {}" , fdt120);

        return fdt120;
    }



    @ApiOperation(value = "Updates the Fdt120 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Fdt120 editFdt120(@RequestParam("ordnum") String ordnum,@RequestParam("fabseq") Byte fabseq,@RequestParam("clrseq") Byte clrseq,@RequestParam("fabinstcode") String fabinstcode, @RequestBody Fdt120 fdt120) throws EntityNotFoundException {

        fdt120.setOrdnum(ordnum);
        fdt120.setFabseq(fabseq);
        fdt120.setClrseq(clrseq);
        fdt120.setFabinstcode(fabinstcode);

        LOGGER.debug("Fdt120 details with id is updated with: {}" , fdt120);

        return fdt120Service.update(fdt120);
    }


    @ApiOperation(value = "Deletes the Fdt120 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteFdt120(@RequestParam("ordnum") String ordnum,@RequestParam("fabseq") Byte fabseq,@RequestParam("clrseq") Byte clrseq,@RequestParam("fabinstcode") String fabinstcode) throws EntityNotFoundException {

        Fdt120Id fdt120Id = new Fdt120Id();
        fdt120Id.setOrdnum(ordnum);
        fdt120Id.setFabseq(fabseq);
        fdt120Id.setClrseq(clrseq);
        fdt120Id.setFabinstcode(fabinstcode);

        LOGGER.debug("Deleting Fdt120 with id: {}" , fdt120Id);
        Fdt120 fdt120 = fdt120Service.delete(fdt120Id);

        return fdt120 != null;
    }


    /**
     * @deprecated Use {@link #findFdt120s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Fdt120 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Fdt120> searchFdt120sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Fdt120s list");
        return fdt120Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Fdt120 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Fdt120> findFdt120s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Fdt120s list");
        return fdt120Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Fdt120 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Fdt120> filterFdt120s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Fdt120s list");
        return fdt120Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportFdt120s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return fdt120Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Fdt120 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countFdt120s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Fdt120s");
		return fdt120Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getFdt120AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return fdt120Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Fdt120Service instance
	 */
	protected void setFdt120Service(Fdt120Service service) {
		this.fdt120Service = service;
	}

}

