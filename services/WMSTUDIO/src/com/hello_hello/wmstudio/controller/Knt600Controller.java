/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Knt600;
import com.hello_hello.wmstudio.Knt600Id;
import com.hello_hello.wmstudio.service.Knt600Service;


/**
 * Controller object for domain model class Knt600.
 * @see Knt600
 */
@RestController("WMSTUDIO.Knt600Controller")
@Api(value = "Knt600Controller", description = "Exposes APIs to work with Knt600 resource.")
@RequestMapping("/WMSTUDIO/Knt600")
public class Knt600Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Knt600Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Knt600Service")
	private Knt600Service knt600Service;

	@ApiOperation(value = "Creates a new Knt600 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Knt600 createKnt600(@RequestBody Knt600 knt600) {
		LOGGER.debug("Create Knt600 with information: {}" , knt600);

		knt600 = knt600Service.create(knt600);
		LOGGER.debug("Created Knt600 with information: {}" , knt600);

	    return knt600;
	}

@ApiOperation(value = "Returns the Knt600 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Knt600 getKnt600(@RequestParam("ordnum") String ordnum,@RequestParam("fabseq") Byte fabseq,@RequestParam("kmchcod") String kmchcod,@RequestParam("dyejob") String dyejob,@RequestParam("plnseq") Byte plnseq) throws EntityNotFoundException {

        Knt600Id knt600Id = new Knt600Id();
        knt600Id.setOrdnum(ordnum);
        knt600Id.setFabseq(fabseq);
        knt600Id.setKmchcod(kmchcod);
        knt600Id.setDyejob(dyejob);
        knt600Id.setPlnseq(plnseq);

        LOGGER.debug("Getting Knt600 with id: {}" , knt600Id);
        Knt600 knt600 = knt600Service.getById(knt600Id);
        LOGGER.debug("Knt600 details with id: {}" , knt600);

        return knt600;
    }



    @ApiOperation(value = "Updates the Knt600 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Knt600 editKnt600(@RequestParam("ordnum") String ordnum,@RequestParam("fabseq") Byte fabseq,@RequestParam("kmchcod") String kmchcod,@RequestParam("dyejob") String dyejob,@RequestParam("plnseq") Byte plnseq, @RequestBody Knt600 knt600) throws EntityNotFoundException {

        knt600.setOrdnum(ordnum);
        knt600.setFabseq(fabseq);
        knt600.setKmchcod(kmchcod);
        knt600.setDyejob(dyejob);
        knt600.setPlnseq(plnseq);

        LOGGER.debug("Knt600 details with id is updated with: {}" , knt600);

        return knt600Service.update(knt600);
    }


    @ApiOperation(value = "Deletes the Knt600 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteKnt600(@RequestParam("ordnum") String ordnum,@RequestParam("fabseq") Byte fabseq,@RequestParam("kmchcod") String kmchcod,@RequestParam("dyejob") String dyejob,@RequestParam("plnseq") Byte plnseq) throws EntityNotFoundException {

        Knt600Id knt600Id = new Knt600Id();
        knt600Id.setOrdnum(ordnum);
        knt600Id.setFabseq(fabseq);
        knt600Id.setKmchcod(kmchcod);
        knt600Id.setDyejob(dyejob);
        knt600Id.setPlnseq(plnseq);

        LOGGER.debug("Deleting Knt600 with id: {}" , knt600Id);
        Knt600 knt600 = knt600Service.delete(knt600Id);

        return knt600 != null;
    }


    /**
     * @deprecated Use {@link #findKnt600s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Knt600 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Knt600> searchKnt600sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Knt600s list");
        return knt600Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Knt600 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Knt600> findKnt600s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Knt600s list");
        return knt600Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Knt600 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Knt600> filterKnt600s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Knt600s list");
        return knt600Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportKnt600s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return knt600Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Knt600 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countKnt600s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Knt600s");
		return knt600Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getKnt600AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return knt600Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Knt600Service instance
	 */
	protected void setKnt600Service(Knt600Service service) {
		this.knt600Service = service;
	}

}

