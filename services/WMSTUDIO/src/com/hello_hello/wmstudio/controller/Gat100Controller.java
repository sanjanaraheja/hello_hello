/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Gat100;
import com.hello_hello.wmstudio.Gat100Id;
import com.hello_hello.wmstudio.service.Gat100Service;


/**
 * Controller object for domain model class Gat100.
 * @see Gat100
 */
@RestController("WMSTUDIO.Gat100Controller")
@Api(value = "Gat100Controller", description = "Exposes APIs to work with Gat100 resource.")
@RequestMapping("/WMSTUDIO/Gat100")
public class Gat100Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Gat100Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Gat100Service")
	private Gat100Service gat100Service;

	@ApiOperation(value = "Creates a new Gat100 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Gat100 createGat100(@RequestBody Gat100 gat100) {
		LOGGER.debug("Create Gat100 with information: {}" , gat100);

		gat100 = gat100Service.create(gat100);
		LOGGER.debug("Created Gat100 with information: {}" , gat100);

	    return gat100;
	}

@ApiOperation(value = "Returns the Gat100 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Gat100 getGat100(@RequestParam("ordnum") String ordnum,@RequestParam("acsseq") Short acsseq) throws EntityNotFoundException {

        Gat100Id gat100Id = new Gat100Id();
        gat100Id.setOrdnum(ordnum);
        gat100Id.setAcsseq(acsseq);

        LOGGER.debug("Getting Gat100 with id: {}" , gat100Id);
        Gat100 gat100 = gat100Service.getById(gat100Id);
        LOGGER.debug("Gat100 details with id: {}" , gat100);

        return gat100;
    }



    @ApiOperation(value = "Updates the Gat100 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Gat100 editGat100(@RequestParam("ordnum") String ordnum,@RequestParam("acsseq") Short acsseq, @RequestBody Gat100 gat100) throws EntityNotFoundException {

        gat100.setOrdnum(ordnum);
        gat100.setAcsseq(acsseq);

        LOGGER.debug("Gat100 details with id is updated with: {}" , gat100);

        return gat100Service.update(gat100);
    }


    @ApiOperation(value = "Deletes the Gat100 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteGat100(@RequestParam("ordnum") String ordnum,@RequestParam("acsseq") Short acsseq) throws EntityNotFoundException {

        Gat100Id gat100Id = new Gat100Id();
        gat100Id.setOrdnum(ordnum);
        gat100Id.setAcsseq(acsseq);

        LOGGER.debug("Deleting Gat100 with id: {}" , gat100Id);
        Gat100 gat100 = gat100Service.delete(gat100Id);

        return gat100 != null;
    }


    /**
     * @deprecated Use {@link #findGat100s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Gat100 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Gat100> searchGat100sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Gat100s list");
        return gat100Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Gat100 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Gat100> findGat100s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Gat100s list");
        return gat100Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Gat100 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Gat100> filterGat100s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Gat100s list");
        return gat100Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportGat100s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return gat100Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Gat100 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countGat100s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Gat100s");
		return gat100Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getGat100AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return gat100Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Gat100Service instance
	 */
	protected void setGat100Service(Gat100Service service) {
		this.gat100Service = service;
	}

}

