/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Gat560;
import com.hello_hello.wmstudio.Gat560Id;
import com.hello_hello.wmstudio.service.Gat560Service;


/**
 * Controller object for domain model class Gat560.
 * @see Gat560
 */
@RestController("WMSTUDIO.Gat560Controller")
@Api(value = "Gat560Controller", description = "Exposes APIs to work with Gat560 resource.")
@RequestMapping("/WMSTUDIO/Gat560")
public class Gat560Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Gat560Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Gat560Service")
	private Gat560Service gat560Service;

	@ApiOperation(value = "Creates a new Gat560 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Gat560 createGat560(@RequestBody Gat560 gat560) {
		LOGGER.debug("Create Gat560 with information: {}" , gat560);

		gat560 = gat560Service.create(gat560);
		LOGGER.debug("Created Gat560 with information: {}" , gat560);

	    return gat560;
	}

@ApiOperation(value = "Returns the Gat560 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Gat560 getGat560(@RequestParam("bulkpo") String bulkpo,@RequestParam("rsvseq") Short rsvseq,@RequestParam("acsseq") Short acsseq,@RequestParam("ponum") String ponum,@RequestParam("poseq") Short poseq,@RequestParam("fstrcod") String fstrcod,@RequestParam("flcncod") String flcncod) throws EntityNotFoundException {

        Gat560Id gat560Id = new Gat560Id();
        gat560Id.setBulkpo(bulkpo);
        gat560Id.setRsvseq(rsvseq);
        gat560Id.setAcsseq(acsseq);
        gat560Id.setPonum(ponum);
        gat560Id.setPoseq(poseq);
        gat560Id.setFstrcod(fstrcod);
        gat560Id.setFlcncod(flcncod);

        LOGGER.debug("Getting Gat560 with id: {}" , gat560Id);
        Gat560 gat560 = gat560Service.getById(gat560Id);
        LOGGER.debug("Gat560 details with id: {}" , gat560);

        return gat560;
    }



    @ApiOperation(value = "Updates the Gat560 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Gat560 editGat560(@RequestParam("bulkpo") String bulkpo,@RequestParam("rsvseq") Short rsvseq,@RequestParam("acsseq") Short acsseq,@RequestParam("ponum") String ponum,@RequestParam("poseq") Short poseq,@RequestParam("fstrcod") String fstrcod,@RequestParam("flcncod") String flcncod, @RequestBody Gat560 gat560) throws EntityNotFoundException {

        gat560.setBulkpo(bulkpo);
        gat560.setRsvseq(rsvseq);
        gat560.setAcsseq(acsseq);
        gat560.setPonum(ponum);
        gat560.setPoseq(poseq);
        gat560.setFstrcod(fstrcod);
        gat560.setFlcncod(flcncod);

        LOGGER.debug("Gat560 details with id is updated with: {}" , gat560);

        return gat560Service.update(gat560);
    }


    @ApiOperation(value = "Deletes the Gat560 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteGat560(@RequestParam("bulkpo") String bulkpo,@RequestParam("rsvseq") Short rsvseq,@RequestParam("acsseq") Short acsseq,@RequestParam("ponum") String ponum,@RequestParam("poseq") Short poseq,@RequestParam("fstrcod") String fstrcod,@RequestParam("flcncod") String flcncod) throws EntityNotFoundException {

        Gat560Id gat560Id = new Gat560Id();
        gat560Id.setBulkpo(bulkpo);
        gat560Id.setRsvseq(rsvseq);
        gat560Id.setAcsseq(acsseq);
        gat560Id.setPonum(ponum);
        gat560Id.setPoseq(poseq);
        gat560Id.setFstrcod(fstrcod);
        gat560Id.setFlcncod(flcncod);

        LOGGER.debug("Deleting Gat560 with id: {}" , gat560Id);
        Gat560 gat560 = gat560Service.delete(gat560Id);

        return gat560 != null;
    }


    /**
     * @deprecated Use {@link #findGat560s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Gat560 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Gat560> searchGat560sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Gat560s list");
        return gat560Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Gat560 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Gat560> findGat560s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Gat560s list");
        return gat560Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Gat560 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Gat560> filterGat560s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Gat560s list");
        return gat560Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportGat560s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return gat560Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Gat560 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countGat560s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Gat560s");
		return gat560Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getGat560AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return gat560Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Gat560Service instance
	 */
	protected void setGat560Service(Gat560Service service) {
		this.gat560Service = service;
	}

}

