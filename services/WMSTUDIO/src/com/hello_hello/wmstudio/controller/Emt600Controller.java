/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Emt600;
import com.hello_hello.wmstudio.Emt600Id;
import com.hello_hello.wmstudio.service.Emt600Service;


/**
 * Controller object for domain model class Emt600.
 * @see Emt600
 */
@RestController("WMSTUDIO.Emt600Controller")
@Api(value = "Emt600Controller", description = "Exposes APIs to work with Emt600 resource.")
@RequestMapping("/WMSTUDIO/Emt600")
public class Emt600Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Emt600Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Emt600Service")
	private Emt600Service emt600Service;

	@ApiOperation(value = "Creates a new Emt600 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Emt600 createEmt600(@RequestBody Emt600 emt600) {
		LOGGER.debug("Create Emt600 with information: {}" , emt600);

		emt600 = emt600Service.create(emt600);
		LOGGER.debug("Created Emt600 with information: {}" , emt600);

	    return emt600;
	}

@ApiOperation(value = "Returns the Emt600 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Emt600 getEmt600(@RequestParam("techcode") String techcode,@RequestParam("techseq") Short techseq,@RequestParam("techvar") Short techvar) throws EntityNotFoundException {

        Emt600Id emt600Id = new Emt600Id();
        emt600Id.setTechcode(techcode);
        emt600Id.setTechseq(techseq);
        emt600Id.setTechvar(techvar);

        LOGGER.debug("Getting Emt600 with id: {}" , emt600Id);
        Emt600 emt600 = emt600Service.getById(emt600Id);
        LOGGER.debug("Emt600 details with id: {}" , emt600);

        return emt600;
    }



    @ApiOperation(value = "Updates the Emt600 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Emt600 editEmt600(@RequestParam("techcode") String techcode,@RequestParam("techseq") Short techseq,@RequestParam("techvar") Short techvar, @RequestBody Emt600 emt600) throws EntityNotFoundException {

        emt600.setTechcode(techcode);
        emt600.setTechseq(techseq);
        emt600.setTechvar(techvar);

        LOGGER.debug("Emt600 details with id is updated with: {}" , emt600);

        return emt600Service.update(emt600);
    }


    @ApiOperation(value = "Deletes the Emt600 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteEmt600(@RequestParam("techcode") String techcode,@RequestParam("techseq") Short techseq,@RequestParam("techvar") Short techvar) throws EntityNotFoundException {

        Emt600Id emt600Id = new Emt600Id();
        emt600Id.setTechcode(techcode);
        emt600Id.setTechseq(techseq);
        emt600Id.setTechvar(techvar);

        LOGGER.debug("Deleting Emt600 with id: {}" , emt600Id);
        Emt600 emt600 = emt600Service.delete(emt600Id);

        return emt600 != null;
    }


    /**
     * @deprecated Use {@link #findEmt600s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Emt600 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Emt600> searchEmt600sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Emt600s list");
        return emt600Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Emt600 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Emt600> findEmt600s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Emt600s list");
        return emt600Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Emt600 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Emt600> filterEmt600s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Emt600s list");
        return emt600Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportEmt600s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return emt600Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Emt600 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countEmt600s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Emt600s");
		return emt600Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getEmt600AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return emt600Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Emt600Service instance
	 */
	protected void setEmt600Service(Emt600Service service) {
		this.emt600Service = service;
	}

}

