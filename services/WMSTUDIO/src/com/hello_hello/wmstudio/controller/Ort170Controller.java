/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Ort170;
import com.hello_hello.wmstudio.Ort170Id;
import com.hello_hello.wmstudio.service.Ort170Service;


/**
 * Controller object for domain model class Ort170.
 * @see Ort170
 */
@RestController("WMSTUDIO.Ort170Controller")
@Api(value = "Ort170Controller", description = "Exposes APIs to work with Ort170 resource.")
@RequestMapping("/WMSTUDIO/Ort170")
public class Ort170Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ort170Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Ort170Service")
	private Ort170Service ort170Service;

	@ApiOperation(value = "Creates a new Ort170 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort170 createOrt170(@RequestBody Ort170 ort170) {
		LOGGER.debug("Create Ort170 with information: {}" , ort170);

		ort170 = ort170Service.create(ort170);
		LOGGER.debug("Created Ort170 with information: {}" , ort170);

	    return ort170;
	}

@ApiOperation(value = "Returns the Ort170 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort170 getOrt170(@RequestParam("ordnum") String ordnum,@RequestParam("clrseq") Byte clrseq,@RequestParam("gmtseq") String gmtseq) throws EntityNotFoundException {

        Ort170Id ort170Id = new Ort170Id();
        ort170Id.setOrdnum(ordnum);
        ort170Id.setClrseq(clrseq);
        ort170Id.setGmtseq(gmtseq);

        LOGGER.debug("Getting Ort170 with id: {}" , ort170Id);
        Ort170 ort170 = ort170Service.getById(ort170Id);
        LOGGER.debug("Ort170 details with id: {}" , ort170);

        return ort170;
    }



    @ApiOperation(value = "Updates the Ort170 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort170 editOrt170(@RequestParam("ordnum") String ordnum,@RequestParam("clrseq") Byte clrseq,@RequestParam("gmtseq") String gmtseq, @RequestBody Ort170 ort170) throws EntityNotFoundException {

        ort170.setOrdnum(ordnum);
        ort170.setClrseq(clrseq);
        ort170.setGmtseq(gmtseq);

        LOGGER.debug("Ort170 details with id is updated with: {}" , ort170);

        return ort170Service.update(ort170);
    }


    @ApiOperation(value = "Deletes the Ort170 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteOrt170(@RequestParam("ordnum") String ordnum,@RequestParam("clrseq") Byte clrseq,@RequestParam("gmtseq") String gmtseq) throws EntityNotFoundException {

        Ort170Id ort170Id = new Ort170Id();
        ort170Id.setOrdnum(ordnum);
        ort170Id.setClrseq(clrseq);
        ort170Id.setGmtseq(gmtseq);

        LOGGER.debug("Deleting Ort170 with id: {}" , ort170Id);
        Ort170 ort170 = ort170Service.delete(ort170Id);

        return ort170 != null;
    }


    /**
     * @deprecated Use {@link #findOrt170s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Ort170 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort170> searchOrt170sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Ort170s list");
        return ort170Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort170 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort170> findOrt170s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort170s list");
        return ort170Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort170 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort170> filterOrt170s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort170s list");
        return ort170Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportOrt170s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return ort170Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Ort170 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countOrt170s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Ort170s");
		return ort170Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getOrt170AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return ort170Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Ort170Service instance
	 */
	protected void setOrt170Service(Ort170Service service) {
		this.ort170Service = service;
	}

}

