/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Cpt133;
import com.hello_hello.wmstudio.Cpt133Id;
import com.hello_hello.wmstudio.service.Cpt133Service;


/**
 * Controller object for domain model class Cpt133.
 * @see Cpt133
 */
@RestController("WMSTUDIO.Cpt133Controller")
@Api(value = "Cpt133Controller", description = "Exposes APIs to work with Cpt133 resource.")
@RequestMapping("/WMSTUDIO/Cpt133")
public class Cpt133Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Cpt133Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Cpt133Service")
	private Cpt133Service cpt133Service;

	@ApiOperation(value = "Creates a new Cpt133 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Cpt133 createCpt133(@RequestBody Cpt133 cpt133) {
		LOGGER.debug("Create Cpt133 with information: {}" , cpt133);

		cpt133 = cpt133Service.create(cpt133);
		LOGGER.debug("Created Cpt133 with information: {}" , cpt133);

	    return cpt133;
	}

@ApiOperation(value = "Returns the Cpt133 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Cpt133 getCpt133(@RequestParam("dsheetno") String dsheetno,@RequestParam("revno") Byte revno,@RequestParam("modelseq") Byte modelseq,@RequestParam("fabseq") Byte fabseq) throws EntityNotFoundException {

        Cpt133Id cpt133Id = new Cpt133Id();
        cpt133Id.setDsheetno(dsheetno);
        cpt133Id.setRevno(revno);
        cpt133Id.setModelseq(modelseq);
        cpt133Id.setFabseq(fabseq);

        LOGGER.debug("Getting Cpt133 with id: {}" , cpt133Id);
        Cpt133 cpt133 = cpt133Service.getById(cpt133Id);
        LOGGER.debug("Cpt133 details with id: {}" , cpt133);

        return cpt133;
    }



    @ApiOperation(value = "Updates the Cpt133 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Cpt133 editCpt133(@RequestParam("dsheetno") String dsheetno,@RequestParam("revno") Byte revno,@RequestParam("modelseq") Byte modelseq,@RequestParam("fabseq") Byte fabseq, @RequestBody Cpt133 cpt133) throws EntityNotFoundException {

        cpt133.setDsheetno(dsheetno);
        cpt133.setRevno(revno);
        cpt133.setModelseq(modelseq);
        cpt133.setFabseq(fabseq);

        LOGGER.debug("Cpt133 details with id is updated with: {}" , cpt133);

        return cpt133Service.update(cpt133);
    }


    @ApiOperation(value = "Deletes the Cpt133 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteCpt133(@RequestParam("dsheetno") String dsheetno,@RequestParam("revno") Byte revno,@RequestParam("modelseq") Byte modelseq,@RequestParam("fabseq") Byte fabseq) throws EntityNotFoundException {

        Cpt133Id cpt133Id = new Cpt133Id();
        cpt133Id.setDsheetno(dsheetno);
        cpt133Id.setRevno(revno);
        cpt133Id.setModelseq(modelseq);
        cpt133Id.setFabseq(fabseq);

        LOGGER.debug("Deleting Cpt133 with id: {}" , cpt133Id);
        Cpt133 cpt133 = cpt133Service.delete(cpt133Id);

        return cpt133 != null;
    }


    /**
     * @deprecated Use {@link #findCpt133s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Cpt133 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Cpt133> searchCpt133sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Cpt133s list");
        return cpt133Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Cpt133 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Cpt133> findCpt133s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Cpt133s list");
        return cpt133Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Cpt133 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Cpt133> filterCpt133s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Cpt133s list");
        return cpt133Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportCpt133s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return cpt133Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Cpt133 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countCpt133s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Cpt133s");
		return cpt133Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getCpt133AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return cpt133Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Cpt133Service instance
	 */
	protected void setCpt133Service(Cpt133Service service) {
		this.cpt133Service = service;
	}

}

