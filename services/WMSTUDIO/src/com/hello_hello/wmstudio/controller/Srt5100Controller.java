/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Srt5100;
import com.hello_hello.wmstudio.Srt5100Id;
import com.hello_hello.wmstudio.service.Srt5100Service;


/**
 * Controller object for domain model class Srt5100.
 * @see Srt5100
 */
@RestController("WMSTUDIO.Srt5100Controller")
@Api(value = "Srt5100Controller", description = "Exposes APIs to work with Srt5100 resource.")
@RequestMapping("/WMSTUDIO/Srt5100")
public class Srt5100Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Srt5100Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Srt5100Service")
	private Srt5100Service srt5100Service;

	@ApiOperation(value = "Creates a new Srt5100 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Srt5100 createSrt5100(@RequestBody Srt5100 srt5100) {
		LOGGER.debug("Create Srt5100 with information: {}" , srt5100);

		srt5100 = srt5100Service.create(srt5100);
		LOGGER.debug("Created Srt5100 with information: {}" , srt5100);

	    return srt5100;
	}

@ApiOperation(value = "Returns the Srt5100 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Srt5100 getSrt5100(@RequestParam("dsgrequestno") String dsgrequestno,@RequestParam("proposalno") String proposalno) throws EntityNotFoundException {

        Srt5100Id srt5100Id = new Srt5100Id();
        srt5100Id.setDsgrequestno(dsgrequestno);
        srt5100Id.setProposalno(proposalno);

        LOGGER.debug("Getting Srt5100 with id: {}" , srt5100Id);
        Srt5100 srt5100 = srt5100Service.getById(srt5100Id);
        LOGGER.debug("Srt5100 details with id: {}" , srt5100);

        return srt5100;
    }



    @ApiOperation(value = "Updates the Srt5100 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Srt5100 editSrt5100(@RequestParam("dsgrequestno") String dsgrequestno,@RequestParam("proposalno") String proposalno, @RequestBody Srt5100 srt5100) throws EntityNotFoundException {

        srt5100.setDsgrequestno(dsgrequestno);
        srt5100.setProposalno(proposalno);

        LOGGER.debug("Srt5100 details with id is updated with: {}" , srt5100);

        return srt5100Service.update(srt5100);
    }


    @ApiOperation(value = "Deletes the Srt5100 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteSrt5100(@RequestParam("dsgrequestno") String dsgrequestno,@RequestParam("proposalno") String proposalno) throws EntityNotFoundException {

        Srt5100Id srt5100Id = new Srt5100Id();
        srt5100Id.setDsgrequestno(dsgrequestno);
        srt5100Id.setProposalno(proposalno);

        LOGGER.debug("Deleting Srt5100 with id: {}" , srt5100Id);
        Srt5100 srt5100 = srt5100Service.delete(srt5100Id);

        return srt5100 != null;
    }


    /**
     * @deprecated Use {@link #findSrt5100s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Srt5100 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Srt5100> searchSrt5100sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Srt5100s list");
        return srt5100Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Srt5100 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Srt5100> findSrt5100s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Srt5100s list");
        return srt5100Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Srt5100 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Srt5100> filterSrt5100s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Srt5100s list");
        return srt5100Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportSrt5100s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return srt5100Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Srt5100 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countSrt5100s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Srt5100s");
		return srt5100Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getSrt5100AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return srt5100Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Srt5100Service instance
	 */
	protected void setSrt5100Service(Srt5100Service service) {
		this.srt5100Service = service;
	}

}

