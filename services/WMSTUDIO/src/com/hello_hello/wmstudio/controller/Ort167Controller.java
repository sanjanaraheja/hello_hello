/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Ort167;
import com.hello_hello.wmstudio.Ort167Id;
import com.hello_hello.wmstudio.service.Ort167Service;


/**
 * Controller object for domain model class Ort167.
 * @see Ort167
 */
@RestController("WMSTUDIO.Ort167Controller")
@Api(value = "Ort167Controller", description = "Exposes APIs to work with Ort167 resource.")
@RequestMapping("/WMSTUDIO/Ort167")
public class Ort167Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ort167Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Ort167Service")
	private Ort167Service ort167Service;

	@ApiOperation(value = "Creates a new Ort167 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort167 createOrt167(@RequestBody Ort167 ort167) {
		LOGGER.debug("Create Ort167 with information: {}" , ort167);

		ort167 = ort167Service.create(ort167);
		LOGGER.debug("Created Ort167 with information: {}" , ort167);

	    return ort167;
	}

@ApiOperation(value = "Returns the Ort167 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort167 getOrt167(@RequestParam("ordnumRoot") String ordnumRoot,@RequestParam("stycode") String stycode,@RequestParam("revnum") Short revnum,@RequestParam("patfabseq") Byte patfabseq) throws EntityNotFoundException {

        Ort167Id ort167Id = new Ort167Id();
        ort167Id.setOrdnumRoot(ordnumRoot);
        ort167Id.setStycode(stycode);
        ort167Id.setRevnum(revnum);
        ort167Id.setPatfabseq(patfabseq);

        LOGGER.debug("Getting Ort167 with id: {}" , ort167Id);
        Ort167 ort167 = ort167Service.getById(ort167Id);
        LOGGER.debug("Ort167 details with id: {}" , ort167);

        return ort167;
    }



    @ApiOperation(value = "Updates the Ort167 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Ort167 editOrt167(@RequestParam("ordnumRoot") String ordnumRoot,@RequestParam("stycode") String stycode,@RequestParam("revnum") Short revnum,@RequestParam("patfabseq") Byte patfabseq, @RequestBody Ort167 ort167) throws EntityNotFoundException {

        ort167.setOrdnumRoot(ordnumRoot);
        ort167.setStycode(stycode);
        ort167.setRevnum(revnum);
        ort167.setPatfabseq(patfabseq);

        LOGGER.debug("Ort167 details with id is updated with: {}" , ort167);

        return ort167Service.update(ort167);
    }


    @ApiOperation(value = "Deletes the Ort167 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteOrt167(@RequestParam("ordnumRoot") String ordnumRoot,@RequestParam("stycode") String stycode,@RequestParam("revnum") Short revnum,@RequestParam("patfabseq") Byte patfabseq) throws EntityNotFoundException {

        Ort167Id ort167Id = new Ort167Id();
        ort167Id.setOrdnumRoot(ordnumRoot);
        ort167Id.setStycode(stycode);
        ort167Id.setRevnum(revnum);
        ort167Id.setPatfabseq(patfabseq);

        LOGGER.debug("Deleting Ort167 with id: {}" , ort167Id);
        Ort167 ort167 = ort167Service.delete(ort167Id);

        return ort167 != null;
    }


    /**
     * @deprecated Use {@link #findOrt167s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Ort167 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort167> searchOrt167sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Ort167s list");
        return ort167Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort167 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort167> findOrt167s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort167s list");
        return ort167Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Ort167 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Ort167> filterOrt167s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Ort167s list");
        return ort167Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportOrt167s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return ort167Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Ort167 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countOrt167s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Ort167s");
		return ort167Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getOrt167AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return ort167Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Ort167Service instance
	 */
	protected void setOrt167Service(Ort167Service service) {
		this.ort167Service = service;
	}

}

