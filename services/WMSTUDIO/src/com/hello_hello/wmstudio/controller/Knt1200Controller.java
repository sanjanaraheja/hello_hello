/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Knt1200;
import com.hello_hello.wmstudio.service.Knt1200Service;


/**
 * Controller object for domain model class Knt1200.
 * @see Knt1200
 */
@RestController("WMSTUDIO.Knt1200Controller")
@Api(value = "Knt1200Controller", description = "Exposes APIs to work with Knt1200 resource.")
@RequestMapping("/WMSTUDIO/Knt1200")
public class Knt1200Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Knt1200Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Knt1200Service")
	private Knt1200Service knt1200Service;

	@ApiOperation(value = "Creates a new Knt1200 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Knt1200 createKnt1200(@RequestBody Knt1200 knt1200) {
		LOGGER.debug("Create Knt1200 with information: {}" , knt1200);

		knt1200 = knt1200Service.create(knt1200);
		LOGGER.debug("Created Knt1200 with information: {}" , knt1200);

	    return knt1200;
	}

    @ApiOperation(value = "Returns the Knt1200 instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Knt1200 getKnt1200(@PathVariable("id") String id) throws EntityNotFoundException {
        LOGGER.debug("Getting Knt1200 with id: {}" , id);

        Knt1200 foundKnt1200 = knt1200Service.getById(id);
        LOGGER.debug("Knt1200 details with id: {}" , foundKnt1200);

        return foundKnt1200;
    }

    @ApiOperation(value = "Updates the Knt1200 instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Knt1200 editKnt1200(@PathVariable("id") String id, @RequestBody Knt1200 knt1200) throws EntityNotFoundException {
        LOGGER.debug("Editing Knt1200 with id: {}" , knt1200.getStripeid());

        knt1200.setStripeid(id);
        knt1200 = knt1200Service.update(knt1200);
        LOGGER.debug("Knt1200 details with id: {}" , knt1200);

        return knt1200;
    }

    @ApiOperation(value = "Deletes the Knt1200 instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteKnt1200(@PathVariable("id") String id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Knt1200 with id: {}" , id);

        Knt1200 deletedKnt1200 = knt1200Service.delete(id);

        return deletedKnt1200 != null;
    }

    /**
     * @deprecated Use {@link #findKnt1200s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Knt1200 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Knt1200> searchKnt1200sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Knt1200s list");
        return knt1200Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Knt1200 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Knt1200> findKnt1200s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Knt1200s list");
        return knt1200Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Knt1200 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Knt1200> filterKnt1200s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Knt1200s list");
        return knt1200Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportKnt1200s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return knt1200Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Knt1200 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countKnt1200s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Knt1200s");
		return knt1200Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getKnt1200AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return knt1200Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Knt1200Service instance
	 */
	protected void setKnt1200Service(Knt1200Service service) {
		this.knt1200Service = service;
	}

}

