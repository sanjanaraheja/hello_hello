/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.hello_hello.wmstudio.Apt110;
import com.hello_hello.wmstudio.Apt110Id;
import com.hello_hello.wmstudio.service.Apt110Service;


/**
 * Controller object for domain model class Apt110.
 * @see Apt110
 */
@RestController("WMSTUDIO.Apt110Controller")
@Api(value = "Apt110Controller", description = "Exposes APIs to work with Apt110 resource.")
@RequestMapping("/WMSTUDIO/Apt110")
public class Apt110Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Apt110Controller.class);

    @Autowired
	@Qualifier("WMSTUDIO.Apt110Service")
	private Apt110Service apt110Service;

	@ApiOperation(value = "Creates a new Apt110 instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Apt110 createApt110(@RequestBody Apt110 apt110) {
		LOGGER.debug("Create Apt110 with information: {}" , apt110);

		apt110 = apt110Service.create(apt110);
		LOGGER.debug("Created Apt110 with information: {}" , apt110);

	    return apt110;
	}

@ApiOperation(value = "Returns the Apt110 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Apt110 getApt110(@RequestParam("aloprtid") String aloprtid,@RequestParam("alopvar") Short alopvar) throws EntityNotFoundException {

        Apt110Id apt110Id = new Apt110Id();
        apt110Id.setAloprtid(aloprtid);
        apt110Id.setAlopvar(alopvar);

        LOGGER.debug("Getting Apt110 with id: {}" , apt110Id);
        Apt110 apt110 = apt110Service.getById(apt110Id);
        LOGGER.debug("Apt110 details with id: {}" , apt110);

        return apt110;
    }



    @ApiOperation(value = "Updates the Apt110 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Apt110 editApt110(@RequestParam("aloprtid") String aloprtid,@RequestParam("alopvar") Short alopvar, @RequestBody Apt110 apt110) throws EntityNotFoundException {

        apt110.setAloprtid(aloprtid);
        apt110.setAlopvar(alopvar);

        LOGGER.debug("Apt110 details with id is updated with: {}" , apt110);

        return apt110Service.update(apt110);
    }


    @ApiOperation(value = "Deletes the Apt110 instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteApt110(@RequestParam("aloprtid") String aloprtid,@RequestParam("alopvar") Short alopvar) throws EntityNotFoundException {

        Apt110Id apt110Id = new Apt110Id();
        apt110Id.setAloprtid(aloprtid);
        apt110Id.setAlopvar(alopvar);

        LOGGER.debug("Deleting Apt110 with id: {}" , apt110Id);
        Apt110 apt110 = apt110Service.delete(apt110Id);

        return apt110 != null;
    }


    /**
     * @deprecated Use {@link #findApt110s(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Apt110 instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Apt110> searchApt110sByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Apt110s list");
        return apt110Service.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Apt110 instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Apt110> findApt110s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Apt110s list");
        return apt110Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Apt110 instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Apt110> filterApt110s(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Apt110s list");
        return apt110Service.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportApt110s(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return apt110Service.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Apt110 instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countApt110s( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Apt110s");
		return apt110Service.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getApt110AggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return apt110Service.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Apt110Service instance
	 */
	protected void setApt110Service(Apt110Service service) {
		this.apt110Service = service;
	}

}

