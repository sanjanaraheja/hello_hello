/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Ttt305Id implements Serializable {

    private String intrefno;
    private Byte optseq;

    public String getIntrefno() {
        return this.intrefno;
    }

    public void setIntrefno(String intrefno) {
        this.intrefno = intrefno;
    }

    public Byte getOptseq() {
        return this.optseq;
    }

    public void setOptseq(Byte optseq) {
        this.optseq = optseq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ttt305)) return false;
        final Ttt305 ttt305 = (Ttt305) o;
        return Objects.equals(getIntrefno(), ttt305.getIntrefno()) &&
                Objects.equals(getOptseq(), ttt305.getOptseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIntrefno(),
                getOptseq());
    }
}
