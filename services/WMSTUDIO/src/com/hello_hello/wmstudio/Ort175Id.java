/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Ort175Id implements Serializable {

    private String ordnum;
    private Byte clrseq;
    private String gmtseq;
    private Byte fabseq;
    private Byte clrseqf;

    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    public Byte getClrseq() {
        return this.clrseq;
    }

    public void setClrseq(Byte clrseq) {
        this.clrseq = clrseq;
    }

    public String getGmtseq() {
        return this.gmtseq;
    }

    public void setGmtseq(String gmtseq) {
        this.gmtseq = gmtseq;
    }

    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    public Byte getClrseqf() {
        return this.clrseqf;
    }

    public void setClrseqf(Byte clrseqf) {
        this.clrseqf = clrseqf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ort175)) return false;
        final Ort175 ort175 = (Ort175) o;
        return Objects.equals(getOrdnum(), ort175.getOrdnum()) &&
                Objects.equals(getClrseq(), ort175.getClrseq()) &&
                Objects.equals(getGmtseq(), ort175.getGmtseq()) &&
                Objects.equals(getFabseq(), ort175.getFabseq()) &&
                Objects.equals(getClrseqf(), ort175.getClrseqf());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getClrseq(),
                getGmtseq(),
                getFabseq(),
                getClrseqf());
    }
}
