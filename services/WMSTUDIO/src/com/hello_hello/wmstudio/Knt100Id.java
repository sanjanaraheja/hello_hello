/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Knt100Id implements Serializable {

    private String ordnum;
    private Byte fabseq;
    private String kmchcod;
    private Byte seqnum;

    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    public String getKmchcod() {
        return this.kmchcod;
    }

    public void setKmchcod(String kmchcod) {
        this.kmchcod = kmchcod;
    }

    public Byte getSeqnum() {
        return this.seqnum;
    }

    public void setSeqnum(Byte seqnum) {
        this.seqnum = seqnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Knt100)) return false;
        final Knt100 knt100 = (Knt100) o;
        return Objects.equals(getOrdnum(), knt100.getOrdnum()) &&
                Objects.equals(getFabseq(), knt100.getFabseq()) &&
                Objects.equals(getKmchcod(), knt100.getKmchcod()) &&
                Objects.equals(getSeqnum(), knt100.getSeqnum());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getFabseq(),
                getKmchcod(),
                getSeqnum());
    }
}
