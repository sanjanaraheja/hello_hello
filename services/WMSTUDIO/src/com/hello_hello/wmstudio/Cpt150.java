/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Cpt150 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`CPT150`")
@IdClass(Cpt150Id.class)
public class Cpt150 implements Serializable {

    private String dsheetno;
    private Short evnseq;
    private Byte rejcount;
    private Byte repcount;
    private String evncod;
    private String evnsts;
    private String locresp;
    private String deptresp;
    private String teamresp;
    private String preevn;
    private String postevn;
    private LocalDateTime fsttrgdat;
    private String triggerby;
    private Float cpleadtim;
    private LocalDateTime expsdat;
    private LocalDateTime expedat;
    private LocalDateTime actstrdat;
    private LocalDateTime actcmpdat;
    private Short genseq;
    private LocalDateTime defsdat;
    private LocalDateTime defedat;
    private Float cpleadays;
    private String trackevn;
    private String rangefrom;
    private String rangeto;
    private Short ldseq;
    private String animgrp;
    private String remarks;
    private String clrseqgind;
    private String clrseqfind;
    private String fabind;
    private String szeind;
    private String gmtind;
    private String animind;
    private String shwclient;
    private Float clileadtim;
    private String ref1;
    private String usrid;
    private LocalDateTime usrdate;
    private Double tracktime;
    private String notefficient;
    private String manualupd;
    private String pfstab;
    private LocalDateTime inpclidate;
    private LocalDateTime plndate;
    private String plnplayer;
    private String ordnum;
    private String cmpby;
    private Short plnmins;
    private String reason;
    private String stcheck;
    private String yrnind;
    private LocalDateTime ordenquirydate;
    private String tasklevel;
    private String plnstatus;
    private String checkby;
    private LocalDateTime computedactcmpdat;
    private LocalDateTime computedexpsdat;
    private LocalDateTime computedexpedat;
    private String computedtrackevn;
    private Double computedtracktime;
    private Short lateindays;
    private String latestatus;
    private String computedevnsts;
    private LocalDateTime computeddefsdat;
    private LocalDateTime computeddefedat;
    private String archive;
    private String priority;
    private String evntfitby;
    private String prevplnsts;
    private LocalDateTime prevplnstsdate;
    private String plnaso;
    private Short qty;
    private String stycode;
    private String styind;
    private String taskprocess;
    private String checkprocess;
    private String tstremarks;
    private String txtevnseq;
    private String rejectreason;
    private String rejectdept;
    private String reasoncode;
    private String latereasoncode;
    private String latereason;
    private String techcodeind;
    private Short animseq;

    @Id
    @Column(name = "`DSHEETNO`", nullable = false, length = 12)
    public String getDsheetno() {
        return this.dsheetno;
    }

    public void setDsheetno(String dsheetno) {
        this.dsheetno = dsheetno;
    }

    @Id
    @Column(name = "`EVNSEQ`", nullable = false, scale = 0, precision = 5)
    public Short getEvnseq() {
        return this.evnseq;
    }

    public void setEvnseq(Short evnseq) {
        this.evnseq = evnseq;
    }

    @Column(name = "`REJCOUNT`", nullable = true, scale = 0, precision = 2)
    public Byte getRejcount() {
        return this.rejcount;
    }

    public void setRejcount(Byte rejcount) {
        this.rejcount = rejcount;
    }

    @Column(name = "`REPCOUNT`", nullable = true, scale = 0, precision = 2)
    public Byte getRepcount() {
        return this.repcount;
    }

    public void setRepcount(Byte repcount) {
        this.repcount = repcount;
    }

    @Column(name = "`EVNCOD`", nullable = false, length = 8)
    public String getEvncod() {
        return this.evncod;
    }

    public void setEvncod(String evncod) {
        this.evncod = evncod;
    }

    @Column(name = "`EVNSTS`", nullable = true, length = 3)
    public String getEvnsts() {
        return this.evnsts;
    }

    public void setEvnsts(String evnsts) {
        this.evnsts = evnsts;
    }

    @Column(name = "`LOCRESP`", nullable = true, length = 10)
    public String getLocresp() {
        return this.locresp;
    }

    public void setLocresp(String locresp) {
        this.locresp = locresp;
    }

    @Column(name = "`DEPTRESP`", nullable = true, length = 10)
    public String getDeptresp() {
        return this.deptresp;
    }

    public void setDeptresp(String deptresp) {
        this.deptresp = deptresp;
    }

    @Column(name = "`TEAMRESP`", nullable = true, length = 10)
    public String getTeamresp() {
        return this.teamresp;
    }

    public void setTeamresp(String teamresp) {
        this.teamresp = teamresp;
    }

    @Column(name = "`PREEVN`", nullable = true, length = 4000)
    public String getPreevn() {
        return this.preevn;
    }

    public void setPreevn(String preevn) {
        this.preevn = preevn;
    }

    @Column(name = "`POSTEVN`", nullable = true, length = 4000)
    public String getPostevn() {
        return this.postevn;
    }

    public void setPostevn(String postevn) {
        this.postevn = postevn;
    }

    @Column(name = "`FSTTRGDAT`", nullable = true)
    public LocalDateTime getFsttrgdat() {
        return this.fsttrgdat;
    }

    public void setFsttrgdat(LocalDateTime fsttrgdat) {
        this.fsttrgdat = fsttrgdat;
    }

    @Column(name = "`TRIGGERBY`", nullable = true, length = 6)
    public String getTriggerby() {
        return this.triggerby;
    }

    public void setTriggerby(String triggerby) {
        this.triggerby = triggerby;
    }

    @Column(name = "`CPLEADTIM`", nullable = true, scale = 2, precision = 6)
    public Float getCpleadtim() {
        return this.cpleadtim;
    }

    public void setCpleadtim(Float cpleadtim) {
        this.cpleadtim = cpleadtim;
    }

    @Column(name = "`EXPSDAT`", nullable = true)
    public LocalDateTime getExpsdat() {
        return this.expsdat;
    }

    public void setExpsdat(LocalDateTime expsdat) {
        this.expsdat = expsdat;
    }

    @Column(name = "`EXPEDAT`", nullable = true)
    public LocalDateTime getExpedat() {
        return this.expedat;
    }

    public void setExpedat(LocalDateTime expedat) {
        this.expedat = expedat;
    }

    @Column(name = "`ACTSTRDAT`", nullable = true)
    public LocalDateTime getActstrdat() {
        return this.actstrdat;
    }

    public void setActstrdat(LocalDateTime actstrdat) {
        this.actstrdat = actstrdat;
    }

    @Column(name = "`ACTCMPDAT`", nullable = true)
    public LocalDateTime getActcmpdat() {
        return this.actcmpdat;
    }

    public void setActcmpdat(LocalDateTime actcmpdat) {
        this.actcmpdat = actcmpdat;
    }

    @Column(name = "`GENSEQ`", nullable = true, scale = 0, precision = 5)
    public Short getGenseq() {
        return this.genseq;
    }

    public void setGenseq(Short genseq) {
        this.genseq = genseq;
    }

    @Column(name = "`DEFSDAT`", nullable = true)
    public LocalDateTime getDefsdat() {
        return this.defsdat;
    }

    public void setDefsdat(LocalDateTime defsdat) {
        this.defsdat = defsdat;
    }

    @Column(name = "`DEFEDAT`", nullable = true)
    public LocalDateTime getDefedat() {
        return this.defedat;
    }

    public void setDefedat(LocalDateTime defedat) {
        this.defedat = defedat;
    }

    @Column(name = "`CPLEADAYS`", nullable = true, scale = 2, precision = 5)
    public Float getCpleadays() {
        return this.cpleadays;
    }

    public void setCpleadays(Float cpleadays) {
        this.cpleadays = cpleadays;
    }

    @Column(name = "`TRACKEVN`", nullable = true, length = 4000)
    public String getTrackevn() {
        return this.trackevn;
    }

    public void setTrackevn(String trackevn) {
        this.trackevn = trackevn;
    }

    @Column(name = "`RANGEFROM`", nullable = true, length = 5)
    public String getRangefrom() {
        return this.rangefrom;
    }

    public void setRangefrom(String rangefrom) {
        this.rangefrom = rangefrom;
    }

    @Column(name = "`RANGETO`", nullable = true, length = 5)
    public String getRangeto() {
        return this.rangeto;
    }

    public void setRangeto(String rangeto) {
        this.rangeto = rangeto;
    }

    @Column(name = "`LDSEQ`", nullable = true, scale = 0, precision = 5)
    public Short getLdseq() {
        return this.ldseq;
    }

    public void setLdseq(Short ldseq) {
        this.ldseq = ldseq;
    }

    @Column(name = "`ANIMGRP`", nullable = true, length = 3)
    public String getAnimgrp() {
        return this.animgrp;
    }

    public void setAnimgrp(String animgrp) {
        this.animgrp = animgrp;
    }

    @Column(name = "`REMARKS`", nullable = true, length = 4000)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "`CLRSEQGIND`", nullable = true, length = 1)
    public String getClrseqgind() {
        return this.clrseqgind;
    }

    public void setClrseqgind(String clrseqgind) {
        this.clrseqgind = clrseqgind;
    }

    @Column(name = "`CLRSEQFIND`", nullable = true, length = 1)
    public String getClrseqfind() {
        return this.clrseqfind;
    }

    public void setClrseqfind(String clrseqfind) {
        this.clrseqfind = clrseqfind;
    }

    @Column(name = "`FABIND`", nullable = true, length = 1)
    public String getFabind() {
        return this.fabind;
    }

    public void setFabind(String fabind) {
        this.fabind = fabind;
    }

    @Column(name = "`SZEIND`", nullable = true, length = 1)
    public String getSzeind() {
        return this.szeind;
    }

    public void setSzeind(String szeind) {
        this.szeind = szeind;
    }

    @Column(name = "`GMTIND`", nullable = true, length = 1)
    public String getGmtind() {
        return this.gmtind;
    }

    public void setGmtind(String gmtind) {
        this.gmtind = gmtind;
    }

    @Column(name = "`ANIMIND`", nullable = true, length = 1)
    public String getAnimind() {
        return this.animind;
    }

    public void setAnimind(String animind) {
        this.animind = animind;
    }

    @Column(name = "`SHWCLIENT`", nullable = true, length = 1)
    public String getShwclient() {
        return this.shwclient;
    }

    public void setShwclient(String shwclient) {
        this.shwclient = shwclient;
    }

    @Column(name = "`CLILEADTIM`", nullable = true, scale = 2, precision = 6)
    public Float getClileadtim() {
        return this.clileadtim;
    }

    public void setClileadtim(Float clileadtim) {
        this.clileadtim = clileadtim;
    }

    @Column(name = "`REF1`", nullable = true, length = 30)
    public String getRef1() {
        return this.ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    @Column(name = "`USRID`", nullable = true, length = 6)
    public String getUsrid() {
        return this.usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    @Column(name = "`USRDATE`", nullable = true)
    public LocalDateTime getUsrdate() {
        return this.usrdate;
    }

    public void setUsrdate(LocalDateTime usrdate) {
        this.usrdate = usrdate;
    }

    @Column(name = "`TRACKTIME`", nullable = true, scale = 4, precision = 10)
    public Double getTracktime() {
        return this.tracktime;
    }

    public void setTracktime(Double tracktime) {
        this.tracktime = tracktime;
    }

    @Column(name = "`NOTEFFICIENT`", nullable = true, length = 1)
    public String getNotefficient() {
        return this.notefficient;
    }

    public void setNotefficient(String notefficient) {
        this.notefficient = notefficient;
    }

    @Column(name = "`MANUALUPD`", nullable = true, length = 1)
    public String getManualupd() {
        return this.manualupd;
    }

    public void setManualupd(String manualupd) {
        this.manualupd = manualupd;
    }

    @Column(name = "`PFSTAB`", nullable = true, length = 30)
    public String getPfstab() {
        return this.pfstab;
    }

    public void setPfstab(String pfstab) {
        this.pfstab = pfstab;
    }

    @Column(name = "`INPCLIDATE`", nullable = true)
    public LocalDateTime getInpclidate() {
        return this.inpclidate;
    }

    public void setInpclidate(LocalDateTime inpclidate) {
        this.inpclidate = inpclidate;
    }

    @Column(name = "`PLNDATE`", nullable = true)
    public LocalDateTime getPlndate() {
        return this.plndate;
    }

    public void setPlndate(LocalDateTime plndate) {
        this.plndate = plndate;
    }

    @Column(name = "`PLNPLAYER`", nullable = true, length = 50)
    public String getPlnplayer() {
        return this.plnplayer;
    }

    public void setPlnplayer(String plnplayer) {
        this.plnplayer = plnplayer;
    }

    @Column(name = "`ORDNUM`", nullable = true, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Column(name = "`CMPBY`", nullable = true, length = 6)
    public String getCmpby() {
        return this.cmpby;
    }

    public void setCmpby(String cmpby) {
        this.cmpby = cmpby;
    }

    @Column(name = "`PLNMINS`", nullable = true, scale = 0, precision = 5)
    public Short getPlnmins() {
        return this.plnmins;
    }

    public void setPlnmins(Short plnmins) {
        this.plnmins = plnmins;
    }

    @Column(name = "`REASON`", nullable = true, length = 100)
    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Column(name = "`STCHECK`", nullable = true, length = 1)
    public String getStcheck() {
        return this.stcheck;
    }

    public void setStcheck(String stcheck) {
        this.stcheck = stcheck;
    }

    @Column(name = "`YRNIND`", nullable = true, length = 1)
    public String getYrnind() {
        return this.yrnind;
    }

    public void setYrnind(String yrnind) {
        this.yrnind = yrnind;
    }

    @Column(name = "`ORDENQUIRYDATE`", nullable = true)
    public LocalDateTime getOrdenquirydate() {
        return this.ordenquirydate;
    }

    public void setOrdenquirydate(LocalDateTime ordenquirydate) {
        this.ordenquirydate = ordenquirydate;
    }

    @Column(name = "`TASKLEVEL`", nullable = true, length = 8)
    public String getTasklevel() {
        return this.tasklevel;
    }

    public void setTasklevel(String tasklevel) {
        this.tasklevel = tasklevel;
    }

    @Column(name = "`PLNSTATUS`", nullable = true, length = 9)
    public String getPlnstatus() {
        return this.plnstatus;
    }

    public void setPlnstatus(String plnstatus) {
        this.plnstatus = plnstatus;
    }

    @Column(name = "`CHECKBY`", nullable = true, length = 6)
    public String getCheckby() {
        return this.checkby;
    }

    public void setCheckby(String checkby) {
        this.checkby = checkby;
    }

    @Column(name = "`COMPUTEDACTCMPDAT`", nullable = true)
    public LocalDateTime getComputedactcmpdat() {
        return this.computedactcmpdat;
    }

    public void setComputedactcmpdat(LocalDateTime computedactcmpdat) {
        this.computedactcmpdat = computedactcmpdat;
    }

    @Column(name = "`COMPUTEDEXPSDAT`", nullable = true)
    public LocalDateTime getComputedexpsdat() {
        return this.computedexpsdat;
    }

    public void setComputedexpsdat(LocalDateTime computedexpsdat) {
        this.computedexpsdat = computedexpsdat;
    }

    @Column(name = "`COMPUTEDEXPEDAT`", nullable = true)
    public LocalDateTime getComputedexpedat() {
        return this.computedexpedat;
    }

    public void setComputedexpedat(LocalDateTime computedexpedat) {
        this.computedexpedat = computedexpedat;
    }

    @Column(name = "`COMPUTEDTRACKEVN`", nullable = true, length = 4000)
    public String getComputedtrackevn() {
        return this.computedtrackevn;
    }

    public void setComputedtrackevn(String computedtrackevn) {
        this.computedtrackevn = computedtrackevn;
    }

    @Column(name = "`COMPUTEDTRACKTIME`", nullable = true, scale = 2, precision = 10)
    public Double getComputedtracktime() {
        return this.computedtracktime;
    }

    public void setComputedtracktime(Double computedtracktime) {
        this.computedtracktime = computedtracktime;
    }

    @Column(name = "`LATEINDAYS`", nullable = true, scale = 0, precision = 5)
    public Short getLateindays() {
        return this.lateindays;
    }

    public void setLateindays(Short lateindays) {
        this.lateindays = lateindays;
    }

    @Column(name = "`LATESTATUS`", nullable = true, length = 10)
    public String getLatestatus() {
        return this.latestatus;
    }

    public void setLatestatus(String latestatus) {
        this.latestatus = latestatus;
    }

    @Column(name = "`COMPUTEDEVNSTS`", nullable = true, length = 3)
    public String getComputedevnsts() {
        return this.computedevnsts;
    }

    public void setComputedevnsts(String computedevnsts) {
        this.computedevnsts = computedevnsts;
    }

    @Column(name = "`COMPUTEDDEFSDAT`", nullable = true)
    public LocalDateTime getComputeddefsdat() {
        return this.computeddefsdat;
    }

    public void setComputeddefsdat(LocalDateTime computeddefsdat) {
        this.computeddefsdat = computeddefsdat;
    }

    @Column(name = "`COMPUTEDDEFEDAT`", nullable = true)
    public LocalDateTime getComputeddefedat() {
        return this.computeddefedat;
    }

    public void setComputeddefedat(LocalDateTime computeddefedat) {
        this.computeddefedat = computeddefedat;
    }

    @Column(name = "`ARCHIVE`", nullable = true, length = 1)
    public String getArchive() {
        return this.archive;
    }

    public void setArchive(String archive) {
        this.archive = archive;
    }

    @Column(name = "`PRIORITY`", nullable = true, length = 2)
    public String getPriority() {
        return this.priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Column(name = "`EVNTFITBY`", nullable = true, length = 6)
    public String getEvntfitby() {
        return this.evntfitby;
    }

    public void setEvntfitby(String evntfitby) {
        this.evntfitby = evntfitby;
    }

    @Column(name = "`PREVPLNSTS`", nullable = true, length = 9)
    public String getPrevplnsts() {
        return this.prevplnsts;
    }

    public void setPrevplnsts(String prevplnsts) {
        this.prevplnsts = prevplnsts;
    }

    @Column(name = "`PREVPLNSTSDATE`", nullable = true)
    public LocalDateTime getPrevplnstsdate() {
        return this.prevplnstsdate;
    }

    public void setPrevplnstsdate(LocalDateTime prevplnstsdate) {
        this.prevplnstsdate = prevplnstsdate;
    }

    @Column(name = "`PLNASO`", nullable = true, length = 50)
    public String getPlnaso() {
        return this.plnaso;
    }

    public void setPlnaso(String plnaso) {
        this.plnaso = plnaso;
    }

    @Column(name = "`QTY`", nullable = true, scale = 0, precision = 3)
    public Short getQty() {
        return this.qty;
    }

    public void setQty(Short qty) {
        this.qty = qty;
    }

    @Column(name = "`STYCODE`", nullable = true, length = 2)
    public String getStycode() {
        return this.stycode;
    }

    public void setStycode(String stycode) {
        this.stycode = stycode;
    }

    @Column(name = "`STYIND`", nullable = true, length = 1)
    public String getStyind() {
        return this.styind;
    }

    public void setStyind(String styind) {
        this.styind = styind;
    }

    @Column(name = "`TASKPROCESS`", nullable = true, length = 3)
    public String getTaskprocess() {
        return this.taskprocess;
    }

    public void setTaskprocess(String taskprocess) {
        this.taskprocess = taskprocess;
    }

    @Column(name = "`CHECKPROCESS`", nullable = true, length = 3)
    public String getCheckprocess() {
        return this.checkprocess;
    }

    public void setCheckprocess(String checkprocess) {
        this.checkprocess = checkprocess;
    }

    @Column(name = "`TSTREMARKS`", nullable = true, length = 4000)
    public String getTstremarks() {
        return this.tstremarks;
    }

    public void setTstremarks(String tstremarks) {
        this.tstremarks = tstremarks;
    }

    @Column(name = "`TXTEVNSEQ`", nullable = true, length = 8)
    public String getTxtevnseq() {
        return this.txtevnseq;
    }

    public void setTxtevnseq(String txtevnseq) {
        this.txtevnseq = txtevnseq;
    }

    @Column(name = "`REJECTREASON`", nullable = true, length = 100)
    public String getRejectreason() {
        return this.rejectreason;
    }

    public void setRejectreason(String rejectreason) {
        this.rejectreason = rejectreason;
    }

    @Column(name = "`REJECTDEPT`", nullable = true, length = 20)
    public String getRejectdept() {
        return this.rejectdept;
    }

    public void setRejectdept(String rejectdept) {
        this.rejectdept = rejectdept;
    }

    @Column(name = "`REASONCODE`", nullable = true, length = 3)
    public String getReasoncode() {
        return this.reasoncode;
    }

    public void setReasoncode(String reasoncode) {
        this.reasoncode = reasoncode;
    }

    @Column(name = "`LATEREASONCODE`", nullable = true, length = 3)
    public String getLatereasoncode() {
        return this.latereasoncode;
    }

    public void setLatereasoncode(String latereasoncode) {
        this.latereasoncode = latereasoncode;
    }

    @Column(name = "`LATEREASON`", nullable = true, length = 100)
    public String getLatereason() {
        return this.latereason;
    }

    public void setLatereason(String latereason) {
        this.latereason = latereason;
    }

    @Column(name = "`TECHCODEIND`", nullable = true, length = 1)
    public String getTechcodeind() {
        return this.techcodeind;
    }

    public void setTechcodeind(String techcodeind) {
        this.techcodeind = techcodeind;
    }

    @Column(name = "`ANIMSEQ`", nullable = true, scale = 0, precision = 5)
    public Short getAnimseq() {
        return this.animseq;
    }

    public void setAnimseq(Short animseq) {
        this.animseq = animseq;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cpt150)) return false;
        final Cpt150 cpt150 = (Cpt150) o;
        return Objects.equals(getDsheetno(), cpt150.getDsheetno()) &&
                Objects.equals(getEvnseq(), cpt150.getEvnseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDsheetno(),
                getEvnseq());
    }
}

