/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Ttt300 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`TTT300`")
public class Ttt300 implements Serializable {

    private String intrefno;
    private String refno;
    private String skuno;
    private String ordnum;
    private Byte fabseq;
    private String refdyejob;
    private String fabtyp;
    private String fabsubcat;
    private String calstn;
    private String str;
    private Short bsffabwgt;
    private Short finfabwgt;
    private Double kgcost;
    private String aloprtid;
    private Short alopvar;
    private String stripeid;
    private Float cpi;
    private Float wpi;
    private String fabcareid;
    private String inside;
    private String outside;
    private String spfinish;
    private String enzyme;
    private String bdsze;
    private LocalDateTime fdReldat;
    private String fdRelid;
    private LocalDateTime fnReldat;
    private String fnRelid;
    private LocalDateTime knReldat;
    private String knRelid;
    private LocalDateTime ccReldat;
    private String ccRelid;
    private String clientSts;
    private LocalDateTime clientModdat;
    private String clientModid;
    private String reportnum;
    private LocalDateTime clientStsdat;
    private Short tdwgt;
    private Float tdwid;
    private Short intrefnoVer;
    private Float nwasteAop;
    private String intrefnoVerModid;
    private LocalDateTime intrefnoVerModdat;
    private Float tightness;
    private String selvreq;
    private Float selvwdth;
    private String heatsetreq;
    private String cutstr;
    private Float residualwdth;
    private String opencompactreq;
    private String finishingtempnum;
    private Byte skewangle;
    private Short lstcpyno;
    private Float cutborderwdth;
    private String animonfab;
    private String markertyp;
    private String thermobdInd;
    private String dccode;
    private String fabdcodelist;
    private String ldecoroute;
    private String sdecoroute;
    private String cmbfabreq;
    private String srcprgid;

    @Id
    @Column(name = "`INTREFNO`", nullable = false, length = 10)
    public String getIntrefno() {
        return this.intrefno;
    }

    public void setIntrefno(String intrefno) {
        this.intrefno = intrefno;
    }

    @Column(name = "`REFNO`", nullable = true, length = 13)
    public String getRefno() {
        return this.refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    @Column(name = "`SKUNO`", nullable = true, length = 20)
    public String getSkuno() {
        return this.skuno;
    }

    public void setSkuno(String skuno) {
        this.skuno = skuno;
    }

    @Column(name = "`ORDNUM`", nullable = true, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Column(name = "`FABSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    @Column(name = "`REFDYEJOB`", nullable = true, length = 9)
    public String getRefdyejob() {
        return this.refdyejob;
    }

    public void setRefdyejob(String refdyejob) {
        this.refdyejob = refdyejob;
    }

    @Column(name = "`FABTYP`", nullable = true, length = 7)
    public String getFabtyp() {
        return this.fabtyp;
    }

    public void setFabtyp(String fabtyp) {
        this.fabtyp = fabtyp;
    }

    @Column(name = "`FABSUBCAT`", nullable = true, length = 5)
    public String getFabsubcat() {
        return this.fabsubcat;
    }

    public void setFabsubcat(String fabsubcat) {
        this.fabsubcat = fabsubcat;
    }

    @Column(name = "`CALSTN`", nullable = true, length = 1)
    public String getCalstn() {
        return this.calstn;
    }

    public void setCalstn(String calstn) {
        this.calstn = calstn;
    }

    @Column(name = "`STR`", nullable = true, length = 1)
    public String getStr() {
        return this.str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Column(name = "`BSFFABWGT`", nullable = true, scale = 0, precision = 3)
    public Short getBsffabwgt() {
        return this.bsffabwgt;
    }

    public void setBsffabwgt(Short bsffabwgt) {
        this.bsffabwgt = bsffabwgt;
    }

    @Column(name = "`FINFABWGT`", nullable = true, scale = 0, precision = 3)
    public Short getFinfabwgt() {
        return this.finfabwgt;
    }

    public void setFinfabwgt(Short finfabwgt) {
        this.finfabwgt = finfabwgt;
    }

    @Column(name = "`KGCOST`", nullable = true, scale = 2, precision = 8)
    public Double getKgcost() {
        return this.kgcost;
    }

    public void setKgcost(Double kgcost) {
        this.kgcost = kgcost;
    }

    @Column(name = "`ALOPRTID`", nullable = true, length = 15)
    public String getAloprtid() {
        return this.aloprtid;
    }

    public void setAloprtid(String aloprtid) {
        this.aloprtid = aloprtid;
    }

    @Column(name = "`ALOPVAR`", nullable = true, scale = 0, precision = 3)
    public Short getAlopvar() {
        return this.alopvar;
    }

    public void setAlopvar(Short alopvar) {
        this.alopvar = alopvar;
    }

    @Column(name = "`STRIPEID`", nullable = true, length = 15)
    public String getStripeid() {
        return this.stripeid;
    }

    public void setStripeid(String stripeid) {
        this.stripeid = stripeid;
    }

    @Column(name = "`CPI`", nullable = true, scale = 2, precision = 6)
    public Float getCpi() {
        return this.cpi;
    }

    public void setCpi(Float cpi) {
        this.cpi = cpi;
    }

    @Column(name = "`WPI`", nullable = true, scale = 2, precision = 6)
    public Float getWpi() {
        return this.wpi;
    }

    public void setWpi(Float wpi) {
        this.wpi = wpi;
    }

    @Column(name = "`FABCAREID`", nullable = true, length = 9)
    public String getFabcareid() {
        return this.fabcareid;
    }

    public void setFabcareid(String fabcareid) {
        this.fabcareid = fabcareid;
    }

    @Column(name = "`INSIDE`", nullable = true, length = 2)
    public String getInside() {
        return this.inside;
    }

    public void setInside(String inside) {
        this.inside = inside;
    }

    @Column(name = "`OUTSIDE`", nullable = true, length = 2)
    public String getOutside() {
        return this.outside;
    }

    public void setOutside(String outside) {
        this.outside = outside;
    }

    @Column(name = "`SPFINISH`", nullable = true, length = 2)
    public String getSpfinish() {
        return this.spfinish;
    }

    public void setSpfinish(String spfinish) {
        this.spfinish = spfinish;
    }

    @Column(name = "`ENZYME`", nullable = true, length = 2)
    public String getEnzyme() {
        return this.enzyme;
    }

    public void setEnzyme(String enzyme) {
        this.enzyme = enzyme;
    }

    @Column(name = "`BDSZE`", nullable = true, length = 1)
    public String getBdsze() {
        return this.bdsze;
    }

    public void setBdsze(String bdsze) {
        this.bdsze = bdsze;
    }

    @Column(name = "`FD_RELDAT`", nullable = true)
    public LocalDateTime getFdReldat() {
        return this.fdReldat;
    }

    public void setFdReldat(LocalDateTime fdReldat) {
        this.fdReldat = fdReldat;
    }

    @Column(name = "`FD_RELID`", nullable = true, length = 6)
    public String getFdRelid() {
        return this.fdRelid;
    }

    public void setFdRelid(String fdRelid) {
        this.fdRelid = fdRelid;
    }

    @Column(name = "`FN_RELDAT`", nullable = true)
    public LocalDateTime getFnReldat() {
        return this.fnReldat;
    }

    public void setFnReldat(LocalDateTime fnReldat) {
        this.fnReldat = fnReldat;
    }

    @Column(name = "`FN_RELID`", nullable = true, length = 6)
    public String getFnRelid() {
        return this.fnRelid;
    }

    public void setFnRelid(String fnRelid) {
        this.fnRelid = fnRelid;
    }

    @Column(name = "`KN_RELDAT`", nullable = true)
    public LocalDateTime getKnReldat() {
        return this.knReldat;
    }

    public void setKnReldat(LocalDateTime knReldat) {
        this.knReldat = knReldat;
    }

    @Column(name = "`KN_RELID`", nullable = true, length = 6)
    public String getKnRelid() {
        return this.knRelid;
    }

    public void setKnRelid(String knRelid) {
        this.knRelid = knRelid;
    }

    @Column(name = "`CC_RELDAT`", nullable = true)
    public LocalDateTime getCcReldat() {
        return this.ccReldat;
    }

    public void setCcReldat(LocalDateTime ccReldat) {
        this.ccReldat = ccReldat;
    }

    @Column(name = "`CC_RELID`", nullable = true, length = 6)
    public String getCcRelid() {
        return this.ccRelid;
    }

    public void setCcRelid(String ccRelid) {
        this.ccRelid = ccRelid;
    }

    @Column(name = "`CLIENT_STS`", nullable = true, length = 3)
    public String getClientSts() {
        return this.clientSts;
    }

    public void setClientSts(String clientSts) {
        this.clientSts = clientSts;
    }

    @Column(name = "`CLIENT_MODDAT`", nullable = true)
    public LocalDateTime getClientModdat() {
        return this.clientModdat;
    }

    public void setClientModdat(LocalDateTime clientModdat) {
        this.clientModdat = clientModdat;
    }

    @Column(name = "`CLIENT_MODID`", nullable = true, length = 6)
    public String getClientModid() {
        return this.clientModid;
    }

    public void setClientModid(String clientModid) {
        this.clientModid = clientModid;
    }

    @Column(name = "`REPORTNUM`", nullable = true, length = 8)
    public String getReportnum() {
        return this.reportnum;
    }

    public void setReportnum(String reportnum) {
        this.reportnum = reportnum;
    }

    @Column(name = "`CLIENT_STSDAT`", nullable = true)
    public LocalDateTime getClientStsdat() {
        return this.clientStsdat;
    }

    public void setClientStsdat(LocalDateTime clientStsdat) {
        this.clientStsdat = clientStsdat;
    }

    @Column(name = "`TDWGT`", nullable = true, scale = 0, precision = 4)
    public Short getTdwgt() {
        return this.tdwgt;
    }

    public void setTdwgt(Short tdwgt) {
        this.tdwgt = tdwgt;
    }

    @Column(name = "`TDWID`", nullable = true, scale = 1, precision = 4)
    public Float getTdwid() {
        return this.tdwid;
    }

    public void setTdwid(Float tdwid) {
        this.tdwid = tdwid;
    }

    @Column(name = "`INTREFNO_VER`", nullable = true, scale = 0, precision = 3)
    public Short getIntrefnoVer() {
        return this.intrefnoVer;
    }

    public void setIntrefnoVer(Short intrefnoVer) {
        this.intrefnoVer = intrefnoVer;
    }

    @Column(name = "`NWASTE_AOP`", nullable = true, scale = 2, precision = 6)
    public Float getNwasteAop() {
        return this.nwasteAop;
    }

    public void setNwasteAop(Float nwasteAop) {
        this.nwasteAop = nwasteAop;
    }

    @Column(name = "`INTREFNO_VER_MODID`", nullable = true, length = 6)
    public String getIntrefnoVerModid() {
        return this.intrefnoVerModid;
    }

    public void setIntrefnoVerModid(String intrefnoVerModid) {
        this.intrefnoVerModid = intrefnoVerModid;
    }

    @Column(name = "`INTREFNO_VER_MODDAT`", nullable = true)
    public LocalDateTime getIntrefnoVerModdat() {
        return this.intrefnoVerModdat;
    }

    public void setIntrefnoVerModdat(LocalDateTime intrefnoVerModdat) {
        this.intrefnoVerModdat = intrefnoVerModdat;
    }

    @Column(name = "`TIGHTNESS`", nullable = true, scale = 1, precision = 4)
    public Float getTightness() {
        return this.tightness;
    }

    public void setTightness(Float tightness) {
        this.tightness = tightness;
    }

    @Column(name = "`SELVREQ`", nullable = true, length = 1)
    public String getSelvreq() {
        return this.selvreq;
    }

    public void setSelvreq(String selvreq) {
        this.selvreq = selvreq;
    }

    @Column(name = "`SELVWDTH`", nullable = true, scale = 1, precision = 5)
    public Float getSelvwdth() {
        return this.selvwdth;
    }

    public void setSelvwdth(Float selvwdth) {
        this.selvwdth = selvwdth;
    }

    @Column(name = "`HEATSETREQ`", nullable = true, length = 1)
    public String getHeatsetreq() {
        return this.heatsetreq;
    }

    public void setHeatsetreq(String heatsetreq) {
        this.heatsetreq = heatsetreq;
    }

    @Column(name = "`CUTSTR`", nullable = true, length = 1)
    public String getCutstr() {
        return this.cutstr;
    }

    public void setCutstr(String cutstr) {
        this.cutstr = cutstr;
    }

    @Column(name = "`RESIDUALWDTH`", nullable = true, scale = 1, precision = 5)
    public Float getResidualwdth() {
        return this.residualwdth;
    }

    public void setResidualwdth(Float residualwdth) {
        this.residualwdth = residualwdth;
    }

    @Column(name = "`OPENCOMPACTREQ`", nullable = true, length = 1)
    public String getOpencompactreq() {
        return this.opencompactreq;
    }

    public void setOpencompactreq(String opencompactreq) {
        this.opencompactreq = opencompactreq;
    }

    @Column(name = "`FINISHINGTEMPNUM`", nullable = true, length = 11)
    public String getFinishingtempnum() {
        return this.finishingtempnum;
    }

    public void setFinishingtempnum(String finishingtempnum) {
        this.finishingtempnum = finishingtempnum;
    }

    @Column(name = "`SKEWANGLE`", nullable = true, scale = 0, precision = 2)
    public Byte getSkewangle() {
        return this.skewangle;
    }

    public void setSkewangle(Byte skewangle) {
        this.skewangle = skewangle;
    }

    @Column(name = "`LSTCPYNO`", nullable = true, scale = 0, precision = 3)
    public Short getLstcpyno() {
        return this.lstcpyno;
    }

    public void setLstcpyno(Short lstcpyno) {
        this.lstcpyno = lstcpyno;
    }

    @Column(name = "`CUTBORDERWDTH`", nullable = true, scale = 1, precision = 5)
    public Float getCutborderwdth() {
        return this.cutborderwdth;
    }

    public void setCutborderwdth(Float cutborderwdth) {
        this.cutborderwdth = cutborderwdth;
    }

    @Column(name = "`ANIMONFAB`", nullable = true, length = 2)
    public String getAnimonfab() {
        return this.animonfab;
    }

    public void setAnimonfab(String animonfab) {
        this.animonfab = animonfab;
    }

    @Column(name = "`MARKERTYP`", nullable = true, length = 2)
    public String getMarkertyp() {
        return this.markertyp;
    }

    public void setMarkertyp(String markertyp) {
        this.markertyp = markertyp;
    }

    @Column(name = "`THERMOBD_IND`", nullable = true, length = 1)
    public String getThermobdInd() {
        return this.thermobdInd;
    }

    public void setThermobdInd(String thermobdInd) {
        this.thermobdInd = thermobdInd;
    }

    @Column(name = "`DCCODE`", nullable = true, length = 15)
    public String getDccode() {
        return this.dccode;
    }

    public void setDccode(String dccode) {
        this.dccode = dccode;
    }

    @Column(name = "`FABDCODELIST`", nullable = true, length = 200)
    public String getFabdcodelist() {
        return this.fabdcodelist;
    }

    public void setFabdcodelist(String fabdcodelist) {
        this.fabdcodelist = fabdcodelist;
    }

    @Column(name = "`LDECOROUTE`", nullable = true, length = 200)
    public String getLdecoroute() {
        return this.ldecoroute;
    }

    public void setLdecoroute(String ldecoroute) {
        this.ldecoroute = ldecoroute;
    }

    @Column(name = "`SDECOROUTE`", nullable = true, length = 150)
    public String getSdecoroute() {
        return this.sdecoroute;
    }

    public void setSdecoroute(String sdecoroute) {
        this.sdecoroute = sdecoroute;
    }

    @Column(name = "`CMBFABREQ`", nullable = true, length = 1)
    public String getCmbfabreq() {
        return this.cmbfabreq;
    }

    public void setCmbfabreq(String cmbfabreq) {
        this.cmbfabreq = cmbfabreq;
    }

    @Column(name = "`SRCPRGID`", nullable = true, length = 15)
    public String getSrcprgid() {
        return this.srcprgid;
    }

    public void setSrcprgid(String srcprgid) {
        this.srcprgid = srcprgid;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ttt300)) return false;
        final Ttt300 ttt300 = (Ttt300) o;
        return Objects.equals(getIntrefno(), ttt300.getIntrefno());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIntrefno());
    }
}

