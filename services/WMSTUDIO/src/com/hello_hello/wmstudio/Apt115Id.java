/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Apt115Id implements Serializable {

    private String aloprtid;
    private Short alopvar;
    private Byte clrseq;

    public String getAloprtid() {
        return this.aloprtid;
    }

    public void setAloprtid(String aloprtid) {
        this.aloprtid = aloprtid;
    }

    public Short getAlopvar() {
        return this.alopvar;
    }

    public void setAlopvar(Short alopvar) {
        this.alopvar = alopvar;
    }

    public Byte getClrseq() {
        return this.clrseq;
    }

    public void setClrseq(Byte clrseq) {
        this.clrseq = clrseq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Apt115)) return false;
        final Apt115 apt115 = (Apt115) o;
        return Objects.equals(getAloprtid(), apt115.getAloprtid()) &&
                Objects.equals(getAlopvar(), apt115.getAlopvar()) &&
                Objects.equals(getClrseq(), apt115.getClrseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAloprtid(),
                getAlopvar(),
                getClrseq());
    }
}
