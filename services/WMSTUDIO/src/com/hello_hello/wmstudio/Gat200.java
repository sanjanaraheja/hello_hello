/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Gat200 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`GAT200`")
@IdClass(Gat200Id.class)
public class Gat200 implements Serializable {

    private String ordnum;
    private Short acsseq;
    private Byte brkseq;
    private String bulkpo;
    private String prodcode;
    private String breakdown;
    private String puruom;
    private Double unitCapacity;
    private Double planqty;
    private Double sysqty;
    private Double netqty;
    private Double addqty;
    private Double revqty;
    private Double purqty;
    private Float purrto;
    private String lcncod;
    private String strcod;
    private String struom;
    private LocalDateTime expdeldat;
    private String supcod;
    private String ctycod;
    private String ccycod;
    private Double ccyrate;
    private Double price;
    private LocalDateTime podate;
    private String ponum;
    private String status;
    private String apvflag;
    private String reasoncode;
    private String remarks;
    private String recalcreq;
    private String unsplit;
    private Byte revnum;
    private String recalcdes;
    private String reference;
    private String splitind;
    private String freightTerm;
    private Double freightVal;
    private Byte precision;
    private String usrid;
    private LocalDateTime usrdate;
    private String purrem;
    private String colrefLab;
    private String detailrefLab;
    private LocalDateTime mexpdeldat;
    private String sourcing;
    private String supInvoice;
    private Double actPrice;
    private Double forPrice;
    private String accRemark;
    private String regenid;
    private LocalDateTime regendat;
    private String desSupref1;
    private String desSupref2;
    private String purrelease;
    private LocalDateTime purreldate;
    private String skuno;
    private String deldn;
    private String reltostore;
    private String flcncod;
    private String fstrcod;
    private String reltocmti;
    private String forSupcod;
    private LocalDateTime cmtideldate;
    private String sysparam;
    private LocalDateTime inputdate;
    private String inputby;
    private String cmtigrp;
    private String shpmod;
    private String consignee;
    private String addind;
    private String cmpflag;
    private String purrelid;
    private String reltocmtiid;
    private LocalDateTime reltocmtidate;
    private String issuetomerch;
    private String hideacs;
    private LocalDateTime expokdat;
    private String fponum;
    private Short facsseq;
    private String filmRequired;
    private Short roundup;
    private String surplusCheck;
    private String surplusChkrem;

    @Id
    @Column(name = "`ORDNUM`", nullable = false, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Id
    @Column(name = "`ACSSEQ`", nullable = false, scale = 0, precision = 3)
    public Short getAcsseq() {
        return this.acsseq;
    }

    public void setAcsseq(Short acsseq) {
        this.acsseq = acsseq;
    }

    @Id
    @Column(name = "`BRKSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getBrkseq() {
        return this.brkseq;
    }

    public void setBrkseq(Byte brkseq) {
        this.brkseq = brkseq;
    }

    @Id
    @Column(name = "`BULKPO`", nullable = false, length = 15)
    public String getBulkpo() {
        return this.bulkpo;
    }

    public void setBulkpo(String bulkpo) {
        this.bulkpo = bulkpo;
    }

    @Column(name = "`PRODCODE`", nullable = true, length = 100)
    public String getProdcode() {
        return this.prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    @Column(name = "`BREAKDOWN`", nullable = true, length = 3)
    public String getBreakdown() {
        return this.breakdown;
    }

    public void setBreakdown(String breakdown) {
        this.breakdown = breakdown;
    }

    @Column(name = "`PURUOM`", nullable = true, length = 3)
    public String getPuruom() {
        return this.puruom;
    }

    public void setPuruom(String puruom) {
        this.puruom = puruom;
    }

    @Column(name = "`UNIT_CAPACITY`", nullable = true, scale = 4, precision = 10)
    public Double getUnitCapacity() {
        return this.unitCapacity;
    }

    public void setUnitCapacity(Double unitCapacity) {
        this.unitCapacity = unitCapacity;
    }

    @Column(name = "`PLANQTY`", nullable = true, scale = 2, precision = 12)
    public Double getPlanqty() {
        return this.planqty;
    }

    public void setPlanqty(Double planqty) {
        this.planqty = planqty;
    }

    @Column(name = "`SYSQTY`", nullable = true, scale = 2, precision = 12)
    public Double getSysqty() {
        return this.sysqty;
    }

    public void setSysqty(Double sysqty) {
        this.sysqty = sysqty;
    }

    @Column(name = "`NETQTY`", nullable = true, scale = 4, precision = 13)
    public Double getNetqty() {
        return this.netqty;
    }

    public void setNetqty(Double netqty) {
        this.netqty = netqty;
    }

    @Column(name = "`ADDQTY`", nullable = true, scale = 2, precision = 12)
    public Double getAddqty() {
        return this.addqty;
    }

    public void setAddqty(Double addqty) {
        this.addqty = addqty;
    }

    @Column(name = "`REVQTY`", nullable = true, scale = 2, precision = 12)
    public Double getRevqty() {
        return this.revqty;
    }

    public void setRevqty(Double revqty) {
        this.revqty = revqty;
    }

    @Column(name = "`PURQTY`", nullable = true, scale = 2, precision = 12)
    public Double getPurqty() {
        return this.purqty;
    }

    public void setPurqty(Double purqty) {
        this.purqty = purqty;
    }

    @Column(name = "`PURRTO`", nullable = true, scale = 3, precision = 4)
    public Float getPurrto() {
        return this.purrto;
    }

    public void setPurrto(Float purrto) {
        this.purrto = purrto;
    }

    @Column(name = "`LCNCOD`", nullable = true, length = 3)
    public String getLcncod() {
        return this.lcncod;
    }

    public void setLcncod(String lcncod) {
        this.lcncod = lcncod;
    }

    @Column(name = "`STRCOD`", nullable = true, length = 6)
    public String getStrcod() {
        return this.strcod;
    }

    public void setStrcod(String strcod) {
        this.strcod = strcod;
    }

    @Column(name = "`STRUOM`", nullable = true, length = 3)
    public String getStruom() {
        return this.struom;
    }

    public void setStruom(String struom) {
        this.struom = struom;
    }

    @Column(name = "`EXPDELDAT`", nullable = true)
    public LocalDateTime getExpdeldat() {
        return this.expdeldat;
    }

    public void setExpdeldat(LocalDateTime expdeldat) {
        this.expdeldat = expdeldat;
    }

    @Column(name = "`SUPCOD`", nullable = true, length = 10)
    public String getSupcod() {
        return this.supcod;
    }

    public void setSupcod(String supcod) {
        this.supcod = supcod;
    }

    @Column(name = "`CTYCOD`", nullable = true, length = 3)
    public String getCtycod() {
        return this.ctycod;
    }

    public void setCtycod(String ctycod) {
        this.ctycod = ctycod;
    }

    @Column(name = "`CCYCOD`", nullable = true, length = 3)
    public String getCcycod() {
        return this.ccycod;
    }

    public void setCcycod(String ccycod) {
        this.ccycod = ccycod;
    }

    @Column(name = "`CCYRATE`", nullable = true, scale = 2, precision = 8)
    public Double getCcyrate() {
        return this.ccyrate;
    }

    public void setCcyrate(Double ccyrate) {
        this.ccyrate = ccyrate;
    }

    @Column(name = "`PRICE`", nullable = true, scale = 4, precision = 8)
    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Column(name = "`PODATE`", nullable = true)
    public LocalDateTime getPodate() {
        return this.podate;
    }

    public void setPodate(LocalDateTime podate) {
        this.podate = podate;
    }

    @Column(name = "`PONUM`", nullable = true, length = 10)
    public String getPonum() {
        return this.ponum;
    }

    public void setPonum(String ponum) {
        this.ponum = ponum;
    }

    @Column(name = "`STATUS`", nullable = true, length = 3)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "`APVFLAG`", nullable = true, length = 1)
    public String getApvflag() {
        return this.apvflag;
    }

    public void setApvflag(String apvflag) {
        this.apvflag = apvflag;
    }

    @Column(name = "`REASONCODE`", nullable = true, length = 20)
    public String getReasoncode() {
        return this.reasoncode;
    }

    public void setReasoncode(String reasoncode) {
        this.reasoncode = reasoncode;
    }

    @Column(name = "`REMARKS`", nullable = true, length = 100)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "`RECALCREQ`", nullable = true, length = 1)
    public String getRecalcreq() {
        return this.recalcreq;
    }

    public void setRecalcreq(String recalcreq) {
        this.recalcreq = recalcreq;
    }

    @Column(name = "`UNSPLIT`", nullable = true, length = 1)
    public String getUnsplit() {
        return this.unsplit;
    }

    public void setUnsplit(String unsplit) {
        this.unsplit = unsplit;
    }

    @Column(name = "`REVNUM`", nullable = true, scale = 0, precision = 2)
    public Byte getRevnum() {
        return this.revnum;
    }

    public void setRevnum(Byte revnum) {
        this.revnum = revnum;
    }

    @Column(name = "`RECALCDES`", nullable = true, length = 1)
    public String getRecalcdes() {
        return this.recalcdes;
    }

    public void setRecalcdes(String recalcdes) {
        this.recalcdes = recalcdes;
    }

    @Column(name = "`REFERENCE`", nullable = true, length = 15)
    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Column(name = "`SPLITIND`", nullable = true, length = 1)
    public String getSplitind() {
        return this.splitind;
    }

    public void setSplitind(String splitind) {
        this.splitind = splitind;
    }

    @Column(name = "`FREIGHT_TERM`", nullable = true, length = 3)
    public String getFreightTerm() {
        return this.freightTerm;
    }

    public void setFreightTerm(String freightTerm) {
        this.freightTerm = freightTerm;
    }

    @Column(name = "`FREIGHT_VAL`", nullable = true, scale = 5, precision = 8)
    public Double getFreightVal() {
        return this.freightVal;
    }

    public void setFreightVal(Double freightVal) {
        this.freightVal = freightVal;
    }

    @Column(name = "`PRECISION`", nullable = true, scale = 0, precision = 1)
    public Byte getPrecision() {
        return this.precision;
    }

    public void setPrecision(Byte precision) {
        this.precision = precision;
    }

    @Column(name = "`USRID`", nullable = true, length = 6)
    public String getUsrid() {
        return this.usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    @Column(name = "`USRDATE`", nullable = true)
    public LocalDateTime getUsrdate() {
        return this.usrdate;
    }

    public void setUsrdate(LocalDateTime usrdate) {
        this.usrdate = usrdate;
    }

    @Column(name = "`PURREM`", nullable = true, length = 200)
    public String getPurrem() {
        return this.purrem;
    }

    public void setPurrem(String purrem) {
        this.purrem = purrem;
    }

    @Column(name = "`COLREF_LAB`", nullable = true, length = 280)
    public String getColrefLab() {
        return this.colrefLab;
    }

    public void setColrefLab(String colrefLab) {
        this.colrefLab = colrefLab;
    }

    @Column(name = "`DETAILREF_LAB`", nullable = true, length = 280)
    public String getDetailrefLab() {
        return this.detailrefLab;
    }

    public void setDetailrefLab(String detailrefLab) {
        this.detailrefLab = detailrefLab;
    }

    @Column(name = "`MEXPDELDAT`", nullable = true)
    public LocalDateTime getMexpdeldat() {
        return this.mexpdeldat;
    }

    public void setMexpdeldat(LocalDateTime mexpdeldat) {
        this.mexpdeldat = mexpdeldat;
    }

    @Column(name = "`SOURCING`", nullable = true, length = 3)
    public String getSourcing() {
        return this.sourcing;
    }

    public void setSourcing(String sourcing) {
        this.sourcing = sourcing;
    }

    @Column(name = "`SUP_INVOICE`", nullable = true, length = 10)
    public String getSupInvoice() {
        return this.supInvoice;
    }

    public void setSupInvoice(String supInvoice) {
        this.supInvoice = supInvoice;
    }

    @Column(name = "`ACT_PRICE`", nullable = true, scale = 4, precision = 8)
    public Double getActPrice() {
        return this.actPrice;
    }

    public void setActPrice(Double actPrice) {
        this.actPrice = actPrice;
    }

    @Column(name = "`FOR_PRICE`", nullable = true, scale = 4, precision = 8)
    public Double getForPrice() {
        return this.forPrice;
    }

    public void setForPrice(Double forPrice) {
        this.forPrice = forPrice;
    }

    @Column(name = "`ACC_REMARK`", nullable = true, length = 30)
    public String getAccRemark() {
        return this.accRemark;
    }

    public void setAccRemark(String accRemark) {
        this.accRemark = accRemark;
    }

    @Column(name = "`REGENID`", nullable = true, length = 6)
    public String getRegenid() {
        return this.regenid;
    }

    public void setRegenid(String regenid) {
        this.regenid = regenid;
    }

    @Column(name = "`REGENDAT`", nullable = true)
    public LocalDateTime getRegendat() {
        return this.regendat;
    }

    public void setRegendat(LocalDateTime regendat) {
        this.regendat = regendat;
    }

    @Column(name = "`DES_SUPREF1`", nullable = true, length = 150)
    public String getDesSupref1() {
        return this.desSupref1;
    }

    public void setDesSupref1(String desSupref1) {
        this.desSupref1 = desSupref1;
    }

    @Column(name = "`DES_SUPREF2`", nullable = true, length = 150)
    public String getDesSupref2() {
        return this.desSupref2;
    }

    public void setDesSupref2(String desSupref2) {
        this.desSupref2 = desSupref2;
    }

    @Column(name = "`PURRELEASE`", nullable = true, length = 1)
    public String getPurrelease() {
        return this.purrelease;
    }

    public void setPurrelease(String purrelease) {
        this.purrelease = purrelease;
    }

    @Column(name = "`PURRELDATE`", nullable = true)
    public LocalDateTime getPurreldate() {
        return this.purreldate;
    }

    public void setPurreldate(LocalDateTime purreldate) {
        this.purreldate = purreldate;
    }

    @Column(name = "`SKUNO`", nullable = true, length = 20)
    public String getSkuno() {
        return this.skuno;
    }

    public void setSkuno(String skuno) {
        this.skuno = skuno;
    }

    @Column(name = "`DELDN`", nullable = true, length = 10)
    public String getDeldn() {
        return this.deldn;
    }

    public void setDeldn(String deldn) {
        this.deldn = deldn;
    }

    @Column(name = "`RELTOSTORE`", nullable = true, length = 1)
    public String getReltostore() {
        return this.reltostore;
    }

    public void setReltostore(String reltostore) {
        this.reltostore = reltostore;
    }

    @Column(name = "`FLCNCOD`", nullable = true, length = 6)
    public String getFlcncod() {
        return this.flcncod;
    }

    public void setFlcncod(String flcncod) {
        this.flcncod = flcncod;
    }

    @Column(name = "`FSTRCOD`", nullable = true, length = 6)
    public String getFstrcod() {
        return this.fstrcod;
    }

    public void setFstrcod(String fstrcod) {
        this.fstrcod = fstrcod;
    }

    @Column(name = "`RELTOCMTI`", nullable = true, length = 1)
    public String getReltocmti() {
        return this.reltocmti;
    }

    public void setReltocmti(String reltocmti) {
        this.reltocmti = reltocmti;
    }

    @Column(name = "`FOR_SUPCOD`", nullable = true, length = 10)
    public String getForSupcod() {
        return this.forSupcod;
    }

    public void setForSupcod(String forSupcod) {
        this.forSupcod = forSupcod;
    }

    @Column(name = "`CMTIDELDATE`", nullable = true)
    public LocalDateTime getCmtideldate() {
        return this.cmtideldate;
    }

    public void setCmtideldate(LocalDateTime cmtideldate) {
        this.cmtideldate = cmtideldate;
    }

    @Column(name = "`SYSPARAM`", nullable = true, length = 3)
    public String getSysparam() {
        return this.sysparam;
    }

    public void setSysparam(String sysparam) {
        this.sysparam = sysparam;
    }

    @Column(name = "`INPUTDATE`", nullable = true)
    public LocalDateTime getInputdate() {
        return this.inputdate;
    }

    public void setInputdate(LocalDateTime inputdate) {
        this.inputdate = inputdate;
    }

    @Column(name = "`INPUTBY`", nullable = true, length = 6)
    public String getInputby() {
        return this.inputby;
    }

    public void setInputby(String inputby) {
        this.inputby = inputby;
    }

    @Column(name = "`CMTIGRP`", nullable = true, length = 4)
    public String getCmtigrp() {
        return this.cmtigrp;
    }

    public void setCmtigrp(String cmtigrp) {
        this.cmtigrp = cmtigrp;
    }

    @Column(name = "`SHPMOD`", nullable = true, length = 2)
    public String getShpmod() {
        return this.shpmod;
    }

    public void setShpmod(String shpmod) {
        this.shpmod = shpmod;
    }

    @Column(name = "`CONSIGNEE`", nullable = true, length = 20)
    public String getConsignee() {
        return this.consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    @Column(name = "`ADDIND`", nullable = true, length = 1)
    public String getAddind() {
        return this.addind;
    }

    public void setAddind(String addind) {
        this.addind = addind;
    }

    @Column(name = "`CMPFLAG`", nullable = true, length = 1)
    public String getCmpflag() {
        return this.cmpflag;
    }

    public void setCmpflag(String cmpflag) {
        this.cmpflag = cmpflag;
    }

    @Column(name = "`PURRELID`", nullable = true, length = 6)
    public String getPurrelid() {
        return this.purrelid;
    }

    public void setPurrelid(String purrelid) {
        this.purrelid = purrelid;
    }

    @Column(name = "`RELTOCMTIID`", nullable = true, length = 6)
    public String getReltocmtiid() {
        return this.reltocmtiid;
    }

    public void setReltocmtiid(String reltocmtiid) {
        this.reltocmtiid = reltocmtiid;
    }

    @Column(name = "`RELTOCMTIDATE`", nullable = true)
    public LocalDateTime getReltocmtidate() {
        return this.reltocmtidate;
    }

    public void setReltocmtidate(LocalDateTime reltocmtidate) {
        this.reltocmtidate = reltocmtidate;
    }

    @Column(name = "`ISSUETOMERCH`", nullable = true, length = 1)
    public String getIssuetomerch() {
        return this.issuetomerch;
    }

    public void setIssuetomerch(String issuetomerch) {
        this.issuetomerch = issuetomerch;
    }

    @Column(name = "`HIDEACS`", nullable = true, length = 1)
    public String getHideacs() {
        return this.hideacs;
    }

    public void setHideacs(String hideacs) {
        this.hideacs = hideacs;
    }

    @Column(name = "`EXPOKDAT`", nullable = true)
    public LocalDateTime getExpokdat() {
        return this.expokdat;
    }

    public void setExpokdat(LocalDateTime expokdat) {
        this.expokdat = expokdat;
    }

    @Column(name = "`FPONUM`", nullable = true, length = 10)
    public String getFponum() {
        return this.fponum;
    }

    public void setFponum(String fponum) {
        this.fponum = fponum;
    }

    @Column(name = "`FACSSEQ`", nullable = true, scale = 0, precision = 3)
    public Short getFacsseq() {
        return this.facsseq;
    }

    public void setFacsseq(Short facsseq) {
        this.facsseq = facsseq;
    }

    @Column(name = "`FILM_REQUIRED`", nullable = true, length = 1)
    public String getFilmRequired() {
        return this.filmRequired;
    }

    public void setFilmRequired(String filmRequired) {
        this.filmRequired = filmRequired;
    }

    @Column(name = "`ROUNDUP`", nullable = true, scale = 0, precision = 4)
    public Short getRoundup() {
        return this.roundup;
    }

    public void setRoundup(Short roundup) {
        this.roundup = roundup;
    }

    @Column(name = "`SURPLUS_CHECK`", nullable = true, length = 1)
    public String getSurplusCheck() {
        return this.surplusCheck;
    }

    public void setSurplusCheck(String surplusCheck) {
        this.surplusCheck = surplusCheck;
    }

    @Column(name = "`SURPLUS_CHKREM`", nullable = true, length = 100)
    public String getSurplusChkrem() {
        return this.surplusChkrem;
    }

    public void setSurplusChkrem(String surplusChkrem) {
        this.surplusChkrem = surplusChkrem;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gat200)) return false;
        final Gat200 gat200 = (Gat200) o;
        return Objects.equals(getOrdnum(), gat200.getOrdnum()) &&
                Objects.equals(getAcsseq(), gat200.getAcsseq()) &&
                Objects.equals(getBrkseq(), gat200.getBrkseq()) &&
                Objects.equals(getBulkpo(), gat200.getBulkpo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getAcsseq(),
                getBrkseq(),
                getBulkpo());
    }
}

