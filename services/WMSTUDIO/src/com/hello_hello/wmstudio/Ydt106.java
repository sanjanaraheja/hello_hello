/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Ydt106 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`YDT106`")
@IdClass(Ydt106Id.class)
public class Ydt106 implements Serializable {

    private String mnyrnclr;
    private String mnclrref;
    private String lcncod;
    private String yrncnt;
    private String yrncps;
    private String yrncdes;
    private String yrnqlty;
    private String yrnsubqltya;
    private String yrnsubqltyb;
    private String yrnffcod;
    private String yrnori;
    private String status;
    private String usrid;
    private LocalDateTime usrdate;
    private String remarks;

    @Id
    @Column(name = "`MNYRNCLR`", nullable = false, length = 2)
    public String getMnyrnclr() {
        return this.mnyrnclr;
    }

    public void setMnyrnclr(String mnyrnclr) {
        this.mnyrnclr = mnyrnclr;
    }

    @Id
    @Column(name = "`MNCLRREF`", nullable = false, length = 15)
    public String getMnclrref() {
        return this.mnclrref;
    }

    public void setMnclrref(String mnclrref) {
        this.mnclrref = mnclrref;
    }

    @Column(name = "`LCNCOD`", nullable = true, length = 6)
    public String getLcncod() {
        return this.lcncod;
    }

    public void setLcncod(String lcncod) {
        this.lcncod = lcncod;
    }

    @Column(name = "`YRNCNT`", nullable = true, length = 16)
    public String getYrncnt() {
        return this.yrncnt;
    }

    public void setYrncnt(String yrncnt) {
        this.yrncnt = yrncnt;
    }

    @Column(name = "`YRNCPS`", nullable = true, length = 26)
    public String getYrncps() {
        return this.yrncps;
    }

    public void setYrncps(String yrncps) {
        this.yrncps = yrncps;
    }

    @Column(name = "`YRNCDES`", nullable = true, length = 25)
    public String getYrncdes() {
        return this.yrncdes;
    }

    public void setYrncdes(String yrncdes) {
        this.yrncdes = yrncdes;
    }

    @Column(name = "`YRNQLTY`", nullable = true, length = 7)
    public String getYrnqlty() {
        return this.yrnqlty;
    }

    public void setYrnqlty(String yrnqlty) {
        this.yrnqlty = yrnqlty;
    }

    @Column(name = "`YRNSUBQLTYA`", nullable = true, length = 7)
    public String getYrnsubqltya() {
        return this.yrnsubqltya;
    }

    public void setYrnsubqltya(String yrnsubqltya) {
        this.yrnsubqltya = yrnsubqltya;
    }

    @Column(name = "`YRNSUBQLTYB`", nullable = true, length = 7)
    public String getYrnsubqltyb() {
        return this.yrnsubqltyb;
    }

    public void setYrnsubqltyb(String yrnsubqltyb) {
        this.yrnsubqltyb = yrnsubqltyb;
    }

    @Column(name = "`YRNFFCOD`", nullable = true, length = 7)
    public String getYrnffcod() {
        return this.yrnffcod;
    }

    public void setYrnffcod(String yrnffcod) {
        this.yrnffcod = yrnffcod;
    }

    @Column(name = "`YRNORI`", nullable = true, length = 7)
    public String getYrnori() {
        return this.yrnori;
    }

    public void setYrnori(String yrnori) {
        this.yrnori = yrnori;
    }

    @Column(name = "`STATUS`", nullable = true, length = 3)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "`USRID`", nullable = true, length = 6)
    public String getUsrid() {
        return this.usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    @Column(name = "`USRDATE`", nullable = true)
    public LocalDateTime getUsrdate() {
        return this.usrdate;
    }

    public void setUsrdate(LocalDateTime usrdate) {
        this.usrdate = usrdate;
    }

    @Column(name = "`REMARKS`", nullable = true, length = 200)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ydt106)) return false;
        final Ydt106 ydt106 = (Ydt106) o;
        return Objects.equals(getMnyrnclr(), ydt106.getMnyrnclr()) &&
                Objects.equals(getMnclrref(), ydt106.getMnclrref());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMnyrnclr(),
                getMnclrref());
    }
}

