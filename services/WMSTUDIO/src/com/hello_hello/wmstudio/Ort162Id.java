/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Ort162Id implements Serializable {

    private String ordnum;
    private Byte fabseq;
    private Byte clrseq;
    private String surpluscat;
    private Byte surplusno;

    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    public Byte getClrseq() {
        return this.clrseq;
    }

    public void setClrseq(Byte clrseq) {
        this.clrseq = clrseq;
    }

    public String getSurpluscat() {
        return this.surpluscat;
    }

    public void setSurpluscat(String surpluscat) {
        this.surpluscat = surpluscat;
    }

    public Byte getSurplusno() {
        return this.surplusno;
    }

    public void setSurplusno(Byte surplusno) {
        this.surplusno = surplusno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ort162)) return false;
        final Ort162 ort162 = (Ort162) o;
        return Objects.equals(getOrdnum(), ort162.getOrdnum()) &&
                Objects.equals(getFabseq(), ort162.getFabseq()) &&
                Objects.equals(getClrseq(), ort162.getClrseq()) &&
                Objects.equals(getSurpluscat(), ort162.getSurpluscat()) &&
                Objects.equals(getSurplusno(), ort162.getSurplusno());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getFabseq(),
                getClrseq(),
                getSurpluscat(),
                getSurplusno());
    }
}
