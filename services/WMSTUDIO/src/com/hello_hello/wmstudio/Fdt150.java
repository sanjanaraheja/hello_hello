/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Fdt150 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`FDT150`")
@IdClass(Fdt150Id.class)
public class Fdt150 implements Serializable {

    private String dyejob;
    private Byte fabseq;
    private Double amtreq;
    private Double wgtreq;
    private Double wgtcmp;
    private Double amtrej;
    private LocalDateTime prtdat;
    private Integer delivin;
    private Integer delivout;
    private Double kgsrecv;
    private Double kgsrej;
    private LocalDateTime indat;
    private LocalDateTime outdat;
    private LocalDateTime deldat;
    private String ordnum;
    private Byte clrseq;
    private Double wgtdel;
    private Double wgtdye;
    private Short numbales;
    private Byte numpal;
    private Short balpwgt;
    private Short yrnid;
    private String cutsts;
    private String ddelnum;
    private String indelnum;
    private LocalDateTime knstrdat;
    private LocalDateTime knenddat;
    private String sdyejob;
    private Byte sfabseq;
    private Double cuamtreq;
    private Double cukgsreq;
    private Double cuamtcq;
    private Double cuamtconf;
    private Double cuamtmeas;
    private String ofabscansts;
    private String kntofab;
    private Byte numbalesfn;
    private Byte kntfabseq;
    private String fdyests;
    private String fnxtscanproc;
    private String fnxtscanscn;
    private String lnkdyejob;
    private Byte lnkfabseq;
    private Double amtrecv;
    private Byte alcseq;
    private String wgtdyetfSdyejob;
    private Double wgtdyetfOut;
    private Byte wgtdyetfSfabseq;
    private Double wgtdyetfIn;
    private String actrack;
    private String destrack;
    private String wmsprocessed;
    private Double amtreqaddIn;
    private String kncqDyereqtyp;
    private String haltkntSts;
    private LocalDateTime haltkntStsdat;
    private String haltkntBy;
    private String haltaopSts;
    private LocalDateTime haltaopStsdat;
    private String haltaopBy;
    private String haltkntProcid;
    private String haltaopProcid;
    private Double expsurpWgt;
    private Double declaredsurpWgt;
    private String fdyescn;
    private String cuinstructionfilename;
    private String fskcondDfncat;
    private LocalDateTime fskcondDfncatdat;
    private String fskcondDfncatby;
    private String fskcondDfncatprgid;
    private String htplanlcncod;
    private Double curelaxedlen;

    @Id
    @Column(name = "`DYEJOB`", nullable = false, length = 9)
    public String getDyejob() {
        return this.dyejob;
    }

    public void setDyejob(String dyejob) {
        this.dyejob = dyejob;
    }

    @Id
    @Column(name = "`FABSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    @Column(name = "`AMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getAmtreq() {
        return this.amtreq;
    }

    public void setAmtreq(Double amtreq) {
        this.amtreq = amtreq;
    }

    @Column(name = "`WGTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getWgtreq() {
        return this.wgtreq;
    }

    public void setWgtreq(Double wgtreq) {
        this.wgtreq = wgtreq;
    }

    @Column(name = "`WGTCMP`", nullable = true, scale = 2, precision = 8)
    public Double getWgtcmp() {
        return this.wgtcmp;
    }

    public void setWgtcmp(Double wgtcmp) {
        this.wgtcmp = wgtcmp;
    }

    @Column(name = "`AMTREJ`", nullable = true, scale = 2, precision = 8)
    public Double getAmtrej() {
        return this.amtrej;
    }

    public void setAmtrej(Double amtrej) {
        this.amtrej = amtrej;
    }

    @Column(name = "`PRTDAT`", nullable = true)
    public LocalDateTime getPrtdat() {
        return this.prtdat;
    }

    public void setPrtdat(LocalDateTime prtdat) {
        this.prtdat = prtdat;
    }

    @Column(name = "`DELIVIN`", nullable = true, scale = 0, precision = 6)
    public Integer getDelivin() {
        return this.delivin;
    }

    public void setDelivin(Integer delivin) {
        this.delivin = delivin;
    }

    @Column(name = "`DELIVOUT`", nullable = true, scale = 0, precision = 6)
    public Integer getDelivout() {
        return this.delivout;
    }

    public void setDelivout(Integer delivout) {
        this.delivout = delivout;
    }

    @Column(name = "`KGSRECV`", nullable = true, scale = 2, precision = 8)
    public Double getKgsrecv() {
        return this.kgsrecv;
    }

    public void setKgsrecv(Double kgsrecv) {
        this.kgsrecv = kgsrecv;
    }

    @Column(name = "`KGSREJ`", nullable = true, scale = 2, precision = 8)
    public Double getKgsrej() {
        return this.kgsrej;
    }

    public void setKgsrej(Double kgsrej) {
        this.kgsrej = kgsrej;
    }

    @Column(name = "`INDAT`", nullable = true)
    public LocalDateTime getIndat() {
        return this.indat;
    }

    public void setIndat(LocalDateTime indat) {
        this.indat = indat;
    }

    @Column(name = "`OUTDAT`", nullable = true)
    public LocalDateTime getOutdat() {
        return this.outdat;
    }

    public void setOutdat(LocalDateTime outdat) {
        this.outdat = outdat;
    }

    @Column(name = "`DELDAT`", nullable = true)
    public LocalDateTime getDeldat() {
        return this.deldat;
    }

    public void setDeldat(LocalDateTime deldat) {
        this.deldat = deldat;
    }

    @Column(name = "`ORDNUM`", nullable = true, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Column(name = "`CLRSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getClrseq() {
        return this.clrseq;
    }

    public void setClrseq(Byte clrseq) {
        this.clrseq = clrseq;
    }

    @Column(name = "`WGTDEL`", nullable = true, scale = 2, precision = 8)
    public Double getWgtdel() {
        return this.wgtdel;
    }

    public void setWgtdel(Double wgtdel) {
        this.wgtdel = wgtdel;
    }

    @Column(name = "`WGTDYE`", nullable = true, scale = 2, precision = 8)
    public Double getWgtdye() {
        return this.wgtdye;
    }

    public void setWgtdye(Double wgtdye) {
        this.wgtdye = wgtdye;
    }

    @Column(name = "`NUMBALES`", nullable = true, scale = 0, precision = 3)
    public Short getNumbales() {
        return this.numbales;
    }

    public void setNumbales(Short numbales) {
        this.numbales = numbales;
    }

    @Column(name = "`NUMPAL`", nullable = true, scale = 0, precision = 1)
    public Byte getNumpal() {
        return this.numpal;
    }

    public void setNumpal(Byte numpal) {
        this.numpal = numpal;
    }

    @Column(name = "`BALPWGT`", nullable = true, scale = 0, precision = 3)
    public Short getBalpwgt() {
        return this.balpwgt;
    }

    public void setBalpwgt(Short balpwgt) {
        this.balpwgt = balpwgt;
    }

    @Column(name = "`YRNID`", nullable = true, scale = 0, precision = 3)
    public Short getYrnid() {
        return this.yrnid;
    }

    public void setYrnid(Short yrnid) {
        this.yrnid = yrnid;
    }

    @Column(name = "`CUTSTS`", nullable = true, length = 1)
    public String getCutsts() {
        return this.cutsts;
    }

    public void setCutsts(String cutsts) {
        this.cutsts = cutsts;
    }

    @Column(name = "`DDELNUM`", nullable = true, length = 8)
    public String getDdelnum() {
        return this.ddelnum;
    }

    public void setDdelnum(String ddelnum) {
        this.ddelnum = ddelnum;
    }

    @Column(name = "`INDELNUM`", nullable = true, length = 8)
    public String getIndelnum() {
        return this.indelnum;
    }

    public void setIndelnum(String indelnum) {
        this.indelnum = indelnum;
    }

    @Column(name = "`KNSTRDAT`", nullable = true)
    public LocalDateTime getKnstrdat() {
        return this.knstrdat;
    }

    public void setKnstrdat(LocalDateTime knstrdat) {
        this.knstrdat = knstrdat;
    }

    @Column(name = "`KNENDDAT`", nullable = true)
    public LocalDateTime getKnenddat() {
        return this.knenddat;
    }

    public void setKnenddat(LocalDateTime knenddat) {
        this.knenddat = knenddat;
    }

    @Column(name = "`SDYEJOB`", nullable = true, length = 9)
    public String getSdyejob() {
        return this.sdyejob;
    }

    public void setSdyejob(String sdyejob) {
        this.sdyejob = sdyejob;
    }

    @Column(name = "`SFABSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getSfabseq() {
        return this.sfabseq;
    }

    public void setSfabseq(Byte sfabseq) {
        this.sfabseq = sfabseq;
    }

    @Column(name = "`CUAMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getCuamtreq() {
        return this.cuamtreq;
    }

    public void setCuamtreq(Double cuamtreq) {
        this.cuamtreq = cuamtreq;
    }

    @Column(name = "`CUKGSREQ`", nullable = true, scale = 2, precision = 8)
    public Double getCukgsreq() {
        return this.cukgsreq;
    }

    public void setCukgsreq(Double cukgsreq) {
        this.cukgsreq = cukgsreq;
    }

    @Column(name = "`CUAMTCQ`", nullable = true, scale = 2, precision = 8)
    public Double getCuamtcq() {
        return this.cuamtcq;
    }

    public void setCuamtcq(Double cuamtcq) {
        this.cuamtcq = cuamtcq;
    }

    @Column(name = "`CUAMTCONF`", nullable = true, scale = 2, precision = 8)
    public Double getCuamtconf() {
        return this.cuamtconf;
    }

    public void setCuamtconf(Double cuamtconf) {
        this.cuamtconf = cuamtconf;
    }

    @Column(name = "`CUAMTMEAS`", nullable = true, scale = 2, precision = 8)
    public Double getCuamtmeas() {
        return this.cuamtmeas;
    }

    public void setCuamtmeas(Double cuamtmeas) {
        this.cuamtmeas = cuamtmeas;
    }

    @Column(name = "`OFABSCANSTS`", nullable = true, length = 1)
    public String getOfabscansts() {
        return this.ofabscansts;
    }

    public void setOfabscansts(String ofabscansts) {
        this.ofabscansts = ofabscansts;
    }

    @Column(name = "`KNTOFAB`", nullable = true, length = 1)
    public String getKntofab() {
        return this.kntofab;
    }

    public void setKntofab(String kntofab) {
        this.kntofab = kntofab;
    }

    @Column(name = "`NUMBALESFN`", nullable = true, scale = 0, precision = 2)
    public Byte getNumbalesfn() {
        return this.numbalesfn;
    }

    public void setNumbalesfn(Byte numbalesfn) {
        this.numbalesfn = numbalesfn;
    }

    @Column(name = "`KNTFABSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getKntfabseq() {
        return this.kntfabseq;
    }

    public void setKntfabseq(Byte kntfabseq) {
        this.kntfabseq = kntfabseq;
    }

    @Column(name = "`FDYESTS`", nullable = true, length = 4)
    public String getFdyests() {
        return this.fdyests;
    }

    public void setFdyests(String fdyests) {
        this.fdyests = fdyests;
    }

    @Column(name = "`FNXTSCANPROC`", nullable = true, length = 4)
    public String getFnxtscanproc() {
        return this.fnxtscanproc;
    }

    public void setFnxtscanproc(String fnxtscanproc) {
        this.fnxtscanproc = fnxtscanproc;
    }

    @Column(name = "`FNXTSCANSCN`", nullable = true, length = 4)
    public String getFnxtscanscn() {
        return this.fnxtscanscn;
    }

    public void setFnxtscanscn(String fnxtscanscn) {
        this.fnxtscanscn = fnxtscanscn;
    }

    @Column(name = "`LNKDYEJOB`", nullable = true, length = 9)
    public String getLnkdyejob() {
        return this.lnkdyejob;
    }

    public void setLnkdyejob(String lnkdyejob) {
        this.lnkdyejob = lnkdyejob;
    }

    @Column(name = "`LNKFABSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getLnkfabseq() {
        return this.lnkfabseq;
    }

    public void setLnkfabseq(Byte lnkfabseq) {
        this.lnkfabseq = lnkfabseq;
    }

    @Column(name = "`AMTRECV`", nullable = true, scale = 2, precision = 8)
    public Double getAmtrecv() {
        return this.amtrecv;
    }

    public void setAmtrecv(Double amtrecv) {
        this.amtrecv = amtrecv;
    }

    @Column(name = "`ALCSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getAlcseq() {
        return this.alcseq;
    }

    public void setAlcseq(Byte alcseq) {
        this.alcseq = alcseq;
    }

    @Column(name = "`WGTDYETF_SDYEJOB`", nullable = true, length = 9)
    public String getWgtdyetfSdyejob() {
        return this.wgtdyetfSdyejob;
    }

    public void setWgtdyetfSdyejob(String wgtdyetfSdyejob) {
        this.wgtdyetfSdyejob = wgtdyetfSdyejob;
    }

    @Column(name = "`WGTDYETF_OUT`", nullable = true, scale = 2, precision = 8)
    public Double getWgtdyetfOut() {
        return this.wgtdyetfOut;
    }

    public void setWgtdyetfOut(Double wgtdyetfOut) {
        this.wgtdyetfOut = wgtdyetfOut;
    }

    @Column(name = "`WGTDYETF_SFABSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getWgtdyetfSfabseq() {
        return this.wgtdyetfSfabseq;
    }

    public void setWgtdyetfSfabseq(Byte wgtdyetfSfabseq) {
        this.wgtdyetfSfabseq = wgtdyetfSfabseq;
    }

    @Column(name = "`WGTDYETF_IN`", nullable = true, scale = 2, precision = 8)
    public Double getWgtdyetfIn() {
        return this.wgtdyetfIn;
    }

    public void setWgtdyetfIn(Double wgtdyetfIn) {
        this.wgtdyetfIn = wgtdyetfIn;
    }

    @Column(name = "`ACTRACK`", nullable = true, length = 6)
    public String getActrack() {
        return this.actrack;
    }

    public void setActrack(String actrack) {
        this.actrack = actrack;
    }

    @Column(name = "`DESTRACK`", nullable = true, length = 6)
    public String getDestrack() {
        return this.destrack;
    }

    public void setDestrack(String destrack) {
        this.destrack = destrack;
    }

    @Column(name = "`WMSPROCESSED`", nullable = true, length = 1)
    public String getWmsprocessed() {
        return this.wmsprocessed;
    }

    public void setWmsprocessed(String wmsprocessed) {
        this.wmsprocessed = wmsprocessed;
    }

    @Column(name = "`AMTREQADD_IN`", nullable = true, scale = 2, precision = 8)
    public Double getAmtreqaddIn() {
        return this.amtreqaddIn;
    }

    public void setAmtreqaddIn(Double amtreqaddIn) {
        this.amtreqaddIn = amtreqaddIn;
    }

    @Column(name = "`KNCQ_DYEREQTYP`", nullable = true, length = 2)
    public String getKncqDyereqtyp() {
        return this.kncqDyereqtyp;
    }

    public void setKncqDyereqtyp(String kncqDyereqtyp) {
        this.kncqDyereqtyp = kncqDyereqtyp;
    }

    @Column(name = "`HALTKNT_STS`", nullable = true, length = 4)
    public String getHaltkntSts() {
        return this.haltkntSts;
    }

    public void setHaltkntSts(String haltkntSts) {
        this.haltkntSts = haltkntSts;
    }

    @Column(name = "`HALTKNT_STSDAT`", nullable = true)
    public LocalDateTime getHaltkntStsdat() {
        return this.haltkntStsdat;
    }

    public void setHaltkntStsdat(LocalDateTime haltkntStsdat) {
        this.haltkntStsdat = haltkntStsdat;
    }

    @Column(name = "`HALTKNT_BY`", nullable = true, length = 6)
    public String getHaltkntBy() {
        return this.haltkntBy;
    }

    public void setHaltkntBy(String haltkntBy) {
        this.haltkntBy = haltkntBy;
    }

    @Column(name = "`HALTAOP_STS`", nullable = true, length = 4)
    public String getHaltaopSts() {
        return this.haltaopSts;
    }

    public void setHaltaopSts(String haltaopSts) {
        this.haltaopSts = haltaopSts;
    }

    @Column(name = "`HALTAOP_STSDAT`", nullable = true)
    public LocalDateTime getHaltaopStsdat() {
        return this.haltaopStsdat;
    }

    public void setHaltaopStsdat(LocalDateTime haltaopStsdat) {
        this.haltaopStsdat = haltaopStsdat;
    }

    @Column(name = "`HALTAOP_BY`", nullable = true, length = 6)
    public String getHaltaopBy() {
        return this.haltaopBy;
    }

    public void setHaltaopBy(String haltaopBy) {
        this.haltaopBy = haltaopBy;
    }

    @Column(name = "`HALTKNT_PROCID`", nullable = true, length = 11)
    public String getHaltkntProcid() {
        return this.haltkntProcid;
    }

    public void setHaltkntProcid(String haltkntProcid) {
        this.haltkntProcid = haltkntProcid;
    }

    @Column(name = "`HALTAOP_PROCID`", nullable = true, length = 11)
    public String getHaltaopProcid() {
        return this.haltaopProcid;
    }

    public void setHaltaopProcid(String haltaopProcid) {
        this.haltaopProcid = haltaopProcid;
    }

    @Column(name = "`EXPSURP_WGT`", nullable = true, scale = 2, precision = 8)
    public Double getExpsurpWgt() {
        return this.expsurpWgt;
    }

    public void setExpsurpWgt(Double expsurpWgt) {
        this.expsurpWgt = expsurpWgt;
    }

    @Column(name = "`DECLAREDSURP_WGT`", nullable = true, scale = 2, precision = 8)
    public Double getDeclaredsurpWgt() {
        return this.declaredsurpWgt;
    }

    public void setDeclaredsurpWgt(Double declaredsurpWgt) {
        this.declaredsurpWgt = declaredsurpWgt;
    }

    @Column(name = "`FDYESCN`", nullable = true, length = 2)
    public String getFdyescn() {
        return this.fdyescn;
    }

    public void setFdyescn(String fdyescn) {
        this.fdyescn = fdyescn;
    }

    @Column(name = "`CUINSTRUCTIONFILENAME`", nullable = true, length = 9)
    public String getCuinstructionfilename() {
        return this.cuinstructionfilename;
    }

    public void setCuinstructionfilename(String cuinstructionfilename) {
        this.cuinstructionfilename = cuinstructionfilename;
    }

    @Column(name = "`FSKCOND_DFNCAT`", nullable = true, length = 7)
    public String getFskcondDfncat() {
        return this.fskcondDfncat;
    }

    public void setFskcondDfncat(String fskcondDfncat) {
        this.fskcondDfncat = fskcondDfncat;
    }

    @Column(name = "`FSKCOND_DFNCATDAT`", nullable = true)
    public LocalDateTime getFskcondDfncatdat() {
        return this.fskcondDfncatdat;
    }

    public void setFskcondDfncatdat(LocalDateTime fskcondDfncatdat) {
        this.fskcondDfncatdat = fskcondDfncatdat;
    }

    @Column(name = "`FSKCOND_DFNCATBY`", nullable = true, length = 6)
    public String getFskcondDfncatby() {
        return this.fskcondDfncatby;
    }

    public void setFskcondDfncatby(String fskcondDfncatby) {
        this.fskcondDfncatby = fskcondDfncatby;
    }

    @Column(name = "`FSKCOND_DFNCATPRGID`", nullable = true, length = 16)
    public String getFskcondDfncatprgid() {
        return this.fskcondDfncatprgid;
    }

    public void setFskcondDfncatprgid(String fskcondDfncatprgid) {
        this.fskcondDfncatprgid = fskcondDfncatprgid;
    }

    @Column(name = "`HTPLANLCNCOD`", nullable = true, length = 6)
    public String getHtplanlcncod() {
        return this.htplanlcncod;
    }

    public void setHtplanlcncod(String htplanlcncod) {
        this.htplanlcncod = htplanlcncod;
    }

    @Column(name = "`CURELAXEDLEN`", nullable = true, scale = 2, precision = 8)
    public Double getCurelaxedlen() {
        return this.curelaxedlen;
    }

    public void setCurelaxedlen(Double curelaxedlen) {
        this.curelaxedlen = curelaxedlen;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fdt150)) return false;
        final Fdt150 fdt150 = (Fdt150) o;
        return Objects.equals(getDyejob(), fdt150.getDyejob()) &&
                Objects.equals(getFabseq(), fdt150.getFabseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDyejob(),
                getFabseq());
    }
}

