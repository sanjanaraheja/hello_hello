/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Ort127;
import com.hello_hello.wmstudio.Ort127Id;


/**
 * ServiceImpl object for domain model class Ort127.
 *
 * @see Ort127
 */
@Service("WMSTUDIO.Ort127Service")
@Validated
public class Ort127ServiceImpl implements Ort127Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ort127ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Ort127Dao")
    private WMGenericDao<Ort127, Ort127Id> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Ort127, Ort127Id> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Ort127 create(Ort127 ort127) {
        LOGGER.debug("Creating a new Ort127 with information: {}", ort127);

        Ort127 ort127Created = this.wmGenericDao.create(ort127);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(ort127Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Ort127 getById(Ort127Id ort127Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Ort127 by id: {}", ort127Id);
        return this.wmGenericDao.findById(ort127Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Ort127 findById(Ort127Id ort127Id) {
        LOGGER.debug("Finding Ort127 by id: {}", ort127Id);
        try {
            return this.wmGenericDao.findById(ort127Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Ort127 found with id: {}", ort127Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Ort127 update(Ort127 ort127) throws EntityNotFoundException {
        LOGGER.debug("Updating Ort127 with information: {}", ort127);

        this.wmGenericDao.update(ort127);
        this.wmGenericDao.refresh(ort127);

        return ort127;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Ort127 delete(Ort127Id ort127Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Ort127 with id: {}", ort127Id);
        Ort127 deleted = this.wmGenericDao.findById(ort127Id);
        if (deleted == null) {
            LOGGER.debug("No Ort127 found with id: {}", ort127Id);
            throw new EntityNotFoundException(String.valueOf(ort127Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Ort127 ort127) {
        LOGGER.debug("Deleting Ort127 with {}", ort127);
        this.wmGenericDao.delete(ort127);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Ort127> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Ort127s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Ort127> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Ort127s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Ort127 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

