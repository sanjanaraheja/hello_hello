/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Cpt133;
import com.hello_hello.wmstudio.Cpt133Id;


/**
 * ServiceImpl object for domain model class Cpt133.
 *
 * @see Cpt133
 */
@Service("WMSTUDIO.Cpt133Service")
@Validated
public class Cpt133ServiceImpl implements Cpt133Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Cpt133ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Cpt133Dao")
    private WMGenericDao<Cpt133, Cpt133Id> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Cpt133, Cpt133Id> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Cpt133 create(Cpt133 cpt133) {
        LOGGER.debug("Creating a new Cpt133 with information: {}", cpt133);

        Cpt133 cpt133Created = this.wmGenericDao.create(cpt133);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(cpt133Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Cpt133 getById(Cpt133Id cpt133Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Cpt133 by id: {}", cpt133Id);
        return this.wmGenericDao.findById(cpt133Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Cpt133 findById(Cpt133Id cpt133Id) {
        LOGGER.debug("Finding Cpt133 by id: {}", cpt133Id);
        try {
            return this.wmGenericDao.findById(cpt133Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Cpt133 found with id: {}", cpt133Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Cpt133 update(Cpt133 cpt133) throws EntityNotFoundException {
        LOGGER.debug("Updating Cpt133 with information: {}", cpt133);

        this.wmGenericDao.update(cpt133);
        this.wmGenericDao.refresh(cpt133);

        return cpt133;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Cpt133 delete(Cpt133Id cpt133Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Cpt133 with id: {}", cpt133Id);
        Cpt133 deleted = this.wmGenericDao.findById(cpt133Id);
        if (deleted == null) {
            LOGGER.debug("No Cpt133 found with id: {}", cpt133Id);
            throw new EntityNotFoundException(String.valueOf(cpt133Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Cpt133 cpt133) {
        LOGGER.debug("Deleting Cpt133 with {}", cpt133);
        this.wmGenericDao.delete(cpt133);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Cpt133> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Cpt133s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Cpt133> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Cpt133s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Cpt133 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

