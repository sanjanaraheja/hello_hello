/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Ttt301;
import com.hello_hello.wmstudio.Ttt301Id;

/**
 * Service object for domain model class {@link Ttt301}.
 */
public interface Ttt301Service {

    /**
     * Creates a new Ttt301. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Ttt301 if any.
     *
     * @param ttt301 Details of the Ttt301 to be created; value cannot be null.
     * @return The newly created Ttt301.
     */
	Ttt301 create(@Valid Ttt301 ttt301);


	/**
	 * Returns Ttt301 by given id if exists.
	 *
	 * @param ttt301Id The id of the Ttt301 to get; value cannot be null.
	 * @return Ttt301 associated with the given ttt301Id.
     * @throws EntityNotFoundException If no Ttt301 is found.
	 */
	Ttt301 getById(Ttt301Id ttt301Id) throws EntityNotFoundException;

    /**
	 * Find and return the Ttt301 by given id if exists, returns null otherwise.
	 *
	 * @param ttt301Id The id of the Ttt301 to get; value cannot be null.
	 * @return Ttt301 associated with the given ttt301Id.
	 */
	Ttt301 findById(Ttt301Id ttt301Id);


	/**
	 * Updates the details of an existing Ttt301. It replaces all fields of the existing Ttt301 with the given ttt301.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Ttt301 if any.
     *
	 * @param ttt301 The details of the Ttt301 to be updated; value cannot be null.
	 * @return The updated Ttt301.
	 * @throws EntityNotFoundException if no Ttt301 is found with given input.
	 */
	Ttt301 update(@Valid Ttt301 ttt301) throws EntityNotFoundException;

    /**
	 * Deletes an existing Ttt301 with the given id.
	 *
	 * @param ttt301Id The id of the Ttt301 to be deleted; value cannot be null.
	 * @return The deleted Ttt301.
	 * @throws EntityNotFoundException if no Ttt301 found with the given id.
	 */
	Ttt301 delete(Ttt301Id ttt301Id) throws EntityNotFoundException;

    /**
	 * Deletes an existing Ttt301 with the given object.
	 *
	 * @param ttt301 The instance of the Ttt301 to be deleted; value cannot be null.
	 */
	void delete(Ttt301 ttt301);

	/**
	 * Find all Ttt301s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Ttt301s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Ttt301> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Ttt301s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Ttt301s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Ttt301> findAll(String query, Pageable pageable);

    /**
	 * Exports all Ttt301s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Ttt301s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Ttt301.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

