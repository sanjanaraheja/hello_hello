/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Ttt300;

/**
 * Service object for domain model class {@link Ttt300}.
 */
public interface Ttt300Service {

    /**
     * Creates a new Ttt300. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Ttt300 if any.
     *
     * @param ttt300 Details of the Ttt300 to be created; value cannot be null.
     * @return The newly created Ttt300.
     */
	Ttt300 create(@Valid Ttt300 ttt300);


	/**
	 * Returns Ttt300 by given id if exists.
	 *
	 * @param ttt300Id The id of the Ttt300 to get; value cannot be null.
	 * @return Ttt300 associated with the given ttt300Id.
     * @throws EntityNotFoundException If no Ttt300 is found.
	 */
	Ttt300 getById(String ttt300Id) throws EntityNotFoundException;

    /**
	 * Find and return the Ttt300 by given id if exists, returns null otherwise.
	 *
	 * @param ttt300Id The id of the Ttt300 to get; value cannot be null.
	 * @return Ttt300 associated with the given ttt300Id.
	 */
	Ttt300 findById(String ttt300Id);


	/**
	 * Updates the details of an existing Ttt300. It replaces all fields of the existing Ttt300 with the given ttt300.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Ttt300 if any.
     *
	 * @param ttt300 The details of the Ttt300 to be updated; value cannot be null.
	 * @return The updated Ttt300.
	 * @throws EntityNotFoundException if no Ttt300 is found with given input.
	 */
	Ttt300 update(@Valid Ttt300 ttt300) throws EntityNotFoundException;

    /**
	 * Deletes an existing Ttt300 with the given id.
	 *
	 * @param ttt300Id The id of the Ttt300 to be deleted; value cannot be null.
	 * @return The deleted Ttt300.
	 * @throws EntityNotFoundException if no Ttt300 found with the given id.
	 */
	Ttt300 delete(String ttt300Id) throws EntityNotFoundException;

    /**
	 * Deletes an existing Ttt300 with the given object.
	 *
	 * @param ttt300 The instance of the Ttt300 to be deleted; value cannot be null.
	 */
	void delete(Ttt300 ttt300);

	/**
	 * Find all Ttt300s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Ttt300s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Ttt300> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Ttt300s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Ttt300s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Ttt300> findAll(String query, Pageable pageable);

    /**
	 * Exports all Ttt300s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Ttt300s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Ttt300.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

