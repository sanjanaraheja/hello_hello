/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Rft170;


/**
 * ServiceImpl object for domain model class Rft170.
 *
 * @see Rft170
 */
@Service("WMSTUDIO.Rft170Service")
@Validated
public class Rft170ServiceImpl implements Rft170Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Rft170ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Rft170Dao")
    private WMGenericDao<Rft170, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Rft170, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Rft170 create(Rft170 rft170) {
        LOGGER.debug("Creating a new Rft170 with information: {}", rft170);

        Rft170 rft170Created = this.wmGenericDao.create(rft170);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(rft170Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Rft170 getById(String rft170Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Rft170 by id: {}", rft170Id);
        return this.wmGenericDao.findById(rft170Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Rft170 findById(String rft170Id) {
        LOGGER.debug("Finding Rft170 by id: {}", rft170Id);
        try {
            return this.wmGenericDao.findById(rft170Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Rft170 found with id: {}", rft170Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Rft170 update(Rft170 rft170) throws EntityNotFoundException {
        LOGGER.debug("Updating Rft170 with information: {}", rft170);

        this.wmGenericDao.update(rft170);
        this.wmGenericDao.refresh(rft170);

        return rft170;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Rft170 delete(String rft170Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Rft170 with id: {}", rft170Id);
        Rft170 deleted = this.wmGenericDao.findById(rft170Id);
        if (deleted == null) {
            LOGGER.debug("No Rft170 found with id: {}", rft170Id);
            throw new EntityNotFoundException(String.valueOf(rft170Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Rft170 rft170) {
        LOGGER.debug("Deleting Rft170 with {}", rft170);
        this.wmGenericDao.delete(rft170);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Rft170> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Rft170s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Rft170> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Rft170s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Rft170 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

