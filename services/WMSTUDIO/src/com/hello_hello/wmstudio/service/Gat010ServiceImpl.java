/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Gat010;


/**
 * ServiceImpl object for domain model class Gat010.
 *
 * @see Gat010
 */
@Service("WMSTUDIO.Gat010Service")
@Validated
public class Gat010ServiceImpl implements Gat010Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Gat010ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Gat010Dao")
    private WMGenericDao<Gat010, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Gat010, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Gat010 create(Gat010 gat010) {
        LOGGER.debug("Creating a new Gat010 with information: {}", gat010);

        Gat010 gat010Created = this.wmGenericDao.create(gat010);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(gat010Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat010 getById(String gat010Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Gat010 by id: {}", gat010Id);
        return this.wmGenericDao.findById(gat010Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat010 findById(String gat010Id) {
        LOGGER.debug("Finding Gat010 by id: {}", gat010Id);
        try {
            return this.wmGenericDao.findById(gat010Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Gat010 found with id: {}", gat010Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat010 update(Gat010 gat010) throws EntityNotFoundException {
        LOGGER.debug("Updating Gat010 with information: {}", gat010);

        this.wmGenericDao.update(gat010);
        this.wmGenericDao.refresh(gat010);

        return gat010;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Gat010 delete(String gat010Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Gat010 with id: {}", gat010Id);
        Gat010 deleted = this.wmGenericDao.findById(gat010Id);
        if (deleted == null) {
            LOGGER.debug("No Gat010 found with id: {}", gat010Id);
            throw new EntityNotFoundException(String.valueOf(gat010Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Gat010 gat010) {
        LOGGER.debug("Deleting Gat010 with {}", gat010);
        this.wmGenericDao.delete(gat010);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Gat010> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Gat010s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Gat010> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Gat010s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Gat010 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

