/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Ort141;
import com.hello_hello.wmstudio.Ort141Id;

/**
 * Service object for domain model class {@link Ort141}.
 */
public interface Ort141Service {

    /**
     * Creates a new Ort141. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Ort141 if any.
     *
     * @param ort141 Details of the Ort141 to be created; value cannot be null.
     * @return The newly created Ort141.
     */
	Ort141 create(@Valid Ort141 ort141);


	/**
	 * Returns Ort141 by given id if exists.
	 *
	 * @param ort141Id The id of the Ort141 to get; value cannot be null.
	 * @return Ort141 associated with the given ort141Id.
     * @throws EntityNotFoundException If no Ort141 is found.
	 */
	Ort141 getById(Ort141Id ort141Id) throws EntityNotFoundException;

    /**
	 * Find and return the Ort141 by given id if exists, returns null otherwise.
	 *
	 * @param ort141Id The id of the Ort141 to get; value cannot be null.
	 * @return Ort141 associated with the given ort141Id.
	 */
	Ort141 findById(Ort141Id ort141Id);


	/**
	 * Updates the details of an existing Ort141. It replaces all fields of the existing Ort141 with the given ort141.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Ort141 if any.
     *
	 * @param ort141 The details of the Ort141 to be updated; value cannot be null.
	 * @return The updated Ort141.
	 * @throws EntityNotFoundException if no Ort141 is found with given input.
	 */
	Ort141 update(@Valid Ort141 ort141) throws EntityNotFoundException;

    /**
	 * Deletes an existing Ort141 with the given id.
	 *
	 * @param ort141Id The id of the Ort141 to be deleted; value cannot be null.
	 * @return The deleted Ort141.
	 * @throws EntityNotFoundException if no Ort141 found with the given id.
	 */
	Ort141 delete(Ort141Id ort141Id) throws EntityNotFoundException;

    /**
	 * Deletes an existing Ort141 with the given object.
	 *
	 * @param ort141 The instance of the Ort141 to be deleted; value cannot be null.
	 */
	void delete(Ort141 ort141);

	/**
	 * Find all Ort141s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Ort141s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Ort141> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Ort141s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Ort141s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Ort141> findAll(String query, Pageable pageable);

    /**
	 * Exports all Ort141s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Ort141s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Ort141.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

