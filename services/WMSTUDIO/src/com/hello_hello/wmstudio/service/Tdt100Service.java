/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Tdt100;
import com.hello_hello.wmstudio.Tdt100Id;

/**
 * Service object for domain model class {@link Tdt100}.
 */
public interface Tdt100Service {

    /**
     * Creates a new Tdt100. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Tdt100 if any.
     *
     * @param tdt100 Details of the Tdt100 to be created; value cannot be null.
     * @return The newly created Tdt100.
     */
	Tdt100 create(@Valid Tdt100 tdt100);


	/**
	 * Returns Tdt100 by given id if exists.
	 *
	 * @param tdt100Id The id of the Tdt100 to get; value cannot be null.
	 * @return Tdt100 associated with the given tdt100Id.
     * @throws EntityNotFoundException If no Tdt100 is found.
	 */
	Tdt100 getById(Tdt100Id tdt100Id) throws EntityNotFoundException;

    /**
	 * Find and return the Tdt100 by given id if exists, returns null otherwise.
	 *
	 * @param tdt100Id The id of the Tdt100 to get; value cannot be null.
	 * @return Tdt100 associated with the given tdt100Id.
	 */
	Tdt100 findById(Tdt100Id tdt100Id);


	/**
	 * Updates the details of an existing Tdt100. It replaces all fields of the existing Tdt100 with the given tdt100.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Tdt100 if any.
     *
	 * @param tdt100 The details of the Tdt100 to be updated; value cannot be null.
	 * @return The updated Tdt100.
	 * @throws EntityNotFoundException if no Tdt100 is found with given input.
	 */
	Tdt100 update(@Valid Tdt100 tdt100) throws EntityNotFoundException;

    /**
	 * Deletes an existing Tdt100 with the given id.
	 *
	 * @param tdt100Id The id of the Tdt100 to be deleted; value cannot be null.
	 * @return The deleted Tdt100.
	 * @throws EntityNotFoundException if no Tdt100 found with the given id.
	 */
	Tdt100 delete(Tdt100Id tdt100Id) throws EntityNotFoundException;

    /**
	 * Deletes an existing Tdt100 with the given object.
	 *
	 * @param tdt100 The instance of the Tdt100 to be deleted; value cannot be null.
	 */
	void delete(Tdt100 tdt100);

	/**
	 * Find all Tdt100s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Tdt100s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Tdt100> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Tdt100s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Tdt100s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Tdt100> findAll(String query, Pageable pageable);

    /**
	 * Exports all Tdt100s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Tdt100s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Tdt100.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

