/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Apt100;


/**
 * ServiceImpl object for domain model class Apt100.
 *
 * @see Apt100
 */
@Service("WMSTUDIO.Apt100Service")
@Validated
public class Apt100ServiceImpl implements Apt100Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Apt100ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Apt100Dao")
    private WMGenericDao<Apt100, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Apt100, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Apt100 create(Apt100 apt100) {
        LOGGER.debug("Creating a new Apt100 with information: {}", apt100);

        Apt100 apt100Created = this.wmGenericDao.create(apt100);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(apt100Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Apt100 getById(String apt100Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Apt100 by id: {}", apt100Id);
        return this.wmGenericDao.findById(apt100Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Apt100 findById(String apt100Id) {
        LOGGER.debug("Finding Apt100 by id: {}", apt100Id);
        try {
            return this.wmGenericDao.findById(apt100Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Apt100 found with id: {}", apt100Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Apt100 update(Apt100 apt100) throws EntityNotFoundException {
        LOGGER.debug("Updating Apt100 with information: {}", apt100);

        this.wmGenericDao.update(apt100);
        this.wmGenericDao.refresh(apt100);

        return apt100;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Apt100 delete(String apt100Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Apt100 with id: {}", apt100Id);
        Apt100 deleted = this.wmGenericDao.findById(apt100Id);
        if (deleted == null) {
            LOGGER.debug("No Apt100 found with id: {}", apt100Id);
            throw new EntityNotFoundException(String.valueOf(apt100Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Apt100 apt100) {
        LOGGER.debug("Deleting Apt100 with {}", apt100);
        this.wmGenericDao.delete(apt100);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Apt100> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Apt100s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Apt100> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Apt100s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Apt100 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

