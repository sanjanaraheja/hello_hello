/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Gat100;
import com.hello_hello.wmstudio.Gat100Id;


/**
 * ServiceImpl object for domain model class Gat100.
 *
 * @see Gat100
 */
@Service("WMSTUDIO.Gat100Service")
@Validated
public class Gat100ServiceImpl implements Gat100Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Gat100ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Gat100Dao")
    private WMGenericDao<Gat100, Gat100Id> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Gat100, Gat100Id> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Gat100 create(Gat100 gat100) {
        LOGGER.debug("Creating a new Gat100 with information: {}", gat100);

        Gat100 gat100Created = this.wmGenericDao.create(gat100);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(gat100Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat100 getById(Gat100Id gat100Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Gat100 by id: {}", gat100Id);
        return this.wmGenericDao.findById(gat100Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat100 findById(Gat100Id gat100Id) {
        LOGGER.debug("Finding Gat100 by id: {}", gat100Id);
        try {
            return this.wmGenericDao.findById(gat100Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Gat100 found with id: {}", gat100Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat100 update(Gat100 gat100) throws EntityNotFoundException {
        LOGGER.debug("Updating Gat100 with information: {}", gat100);

        this.wmGenericDao.update(gat100);
        this.wmGenericDao.refresh(gat100);

        return gat100;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Gat100 delete(Gat100Id gat100Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Gat100 with id: {}", gat100Id);
        Gat100 deleted = this.wmGenericDao.findById(gat100Id);
        if (deleted == null) {
            LOGGER.debug("No Gat100 found with id: {}", gat100Id);
            throw new EntityNotFoundException(String.valueOf(gat100Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Gat100 gat100) {
        LOGGER.debug("Deleting Gat100 with {}", gat100);
        this.wmGenericDao.delete(gat100);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Gat100> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Gat100s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Gat100> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Gat100s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Gat100 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

