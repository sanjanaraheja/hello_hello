/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Rft170;

/**
 * Service object for domain model class {@link Rft170}.
 */
public interface Rft170Service {

    /**
     * Creates a new Rft170. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Rft170 if any.
     *
     * @param rft170 Details of the Rft170 to be created; value cannot be null.
     * @return The newly created Rft170.
     */
	Rft170 create(@Valid Rft170 rft170);


	/**
	 * Returns Rft170 by given id if exists.
	 *
	 * @param rft170Id The id of the Rft170 to get; value cannot be null.
	 * @return Rft170 associated with the given rft170Id.
     * @throws EntityNotFoundException If no Rft170 is found.
	 */
	Rft170 getById(String rft170Id) throws EntityNotFoundException;

    /**
	 * Find and return the Rft170 by given id if exists, returns null otherwise.
	 *
	 * @param rft170Id The id of the Rft170 to get; value cannot be null.
	 * @return Rft170 associated with the given rft170Id.
	 */
	Rft170 findById(String rft170Id);


	/**
	 * Updates the details of an existing Rft170. It replaces all fields of the existing Rft170 with the given rft170.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Rft170 if any.
     *
	 * @param rft170 The details of the Rft170 to be updated; value cannot be null.
	 * @return The updated Rft170.
	 * @throws EntityNotFoundException if no Rft170 is found with given input.
	 */
	Rft170 update(@Valid Rft170 rft170) throws EntityNotFoundException;

    /**
	 * Deletes an existing Rft170 with the given id.
	 *
	 * @param rft170Id The id of the Rft170 to be deleted; value cannot be null.
	 * @return The deleted Rft170.
	 * @throws EntityNotFoundException if no Rft170 found with the given id.
	 */
	Rft170 delete(String rft170Id) throws EntityNotFoundException;

    /**
	 * Deletes an existing Rft170 with the given object.
	 *
	 * @param rft170 The instance of the Rft170 to be deleted; value cannot be null.
	 */
	void delete(Rft170 rft170);

	/**
	 * Find all Rft170s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Rft170s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Rft170> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Rft170s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Rft170s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Rft170> findAll(String query, Pageable pageable);

    /**
	 * Exports all Rft170s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Rft170s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Rft170.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

