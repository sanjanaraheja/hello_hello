/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Apt150;
import com.hello_hello.wmstudio.Apt150Id;

/**
 * Service object for domain model class {@link Apt150}.
 */
public interface Apt150Service {

    /**
     * Creates a new Apt150. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Apt150 if any.
     *
     * @param apt150 Details of the Apt150 to be created; value cannot be null.
     * @return The newly created Apt150.
     */
	Apt150 create(@Valid Apt150 apt150);


	/**
	 * Returns Apt150 by given id if exists.
	 *
	 * @param apt150Id The id of the Apt150 to get; value cannot be null.
	 * @return Apt150 associated with the given apt150Id.
     * @throws EntityNotFoundException If no Apt150 is found.
	 */
	Apt150 getById(Apt150Id apt150Id) throws EntityNotFoundException;

    /**
	 * Find and return the Apt150 by given id if exists, returns null otherwise.
	 *
	 * @param apt150Id The id of the Apt150 to get; value cannot be null.
	 * @return Apt150 associated with the given apt150Id.
	 */
	Apt150 findById(Apt150Id apt150Id);


	/**
	 * Updates the details of an existing Apt150. It replaces all fields of the existing Apt150 with the given apt150.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Apt150 if any.
     *
	 * @param apt150 The details of the Apt150 to be updated; value cannot be null.
	 * @return The updated Apt150.
	 * @throws EntityNotFoundException if no Apt150 is found with given input.
	 */
	Apt150 update(@Valid Apt150 apt150) throws EntityNotFoundException;

    /**
	 * Deletes an existing Apt150 with the given id.
	 *
	 * @param apt150Id The id of the Apt150 to be deleted; value cannot be null.
	 * @return The deleted Apt150.
	 * @throws EntityNotFoundException if no Apt150 found with the given id.
	 */
	Apt150 delete(Apt150Id apt150Id) throws EntityNotFoundException;

    /**
	 * Deletes an existing Apt150 with the given object.
	 *
	 * @param apt150 The instance of the Apt150 to be deleted; value cannot be null.
	 */
	void delete(Apt150 apt150);

	/**
	 * Find all Apt150s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Apt150s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Apt150> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Apt150s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Apt150s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Apt150> findAll(String query, Pageable pageable);

    /**
	 * Exports all Apt150s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Apt150s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Apt150.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

