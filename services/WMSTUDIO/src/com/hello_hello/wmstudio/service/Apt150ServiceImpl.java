/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Apt150;
import com.hello_hello.wmstudio.Apt150Id;


/**
 * ServiceImpl object for domain model class Apt150.
 *
 * @see Apt150
 */
@Service("WMSTUDIO.Apt150Service")
@Validated
public class Apt150ServiceImpl implements Apt150Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Apt150ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Apt150Dao")
    private WMGenericDao<Apt150, Apt150Id> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Apt150, Apt150Id> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Apt150 create(Apt150 apt150) {
        LOGGER.debug("Creating a new Apt150 with information: {}", apt150);

        Apt150 apt150Created = this.wmGenericDao.create(apt150);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(apt150Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Apt150 getById(Apt150Id apt150Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Apt150 by id: {}", apt150Id);
        return this.wmGenericDao.findById(apt150Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Apt150 findById(Apt150Id apt150Id) {
        LOGGER.debug("Finding Apt150 by id: {}", apt150Id);
        try {
            return this.wmGenericDao.findById(apt150Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Apt150 found with id: {}", apt150Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Apt150 update(Apt150 apt150) throws EntityNotFoundException {
        LOGGER.debug("Updating Apt150 with information: {}", apt150);

        this.wmGenericDao.update(apt150);
        this.wmGenericDao.refresh(apt150);

        return apt150;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Apt150 delete(Apt150Id apt150Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Apt150 with id: {}", apt150Id);
        Apt150 deleted = this.wmGenericDao.findById(apt150Id);
        if (deleted == null) {
            LOGGER.debug("No Apt150 found with id: {}", apt150Id);
            throw new EntityNotFoundException(String.valueOf(apt150Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Apt150 apt150) {
        LOGGER.debug("Deleting Apt150 with {}", apt150);
        this.wmGenericDao.delete(apt150);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Apt150> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Apt150s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Apt150> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Apt150s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Apt150 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

