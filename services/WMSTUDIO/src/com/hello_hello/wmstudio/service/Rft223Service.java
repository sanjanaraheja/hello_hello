/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Rft223;
import com.hello_hello.wmstudio.Rft223Id;

/**
 * Service object for domain model class {@link Rft223}.
 */
public interface Rft223Service {

    /**
     * Creates a new Rft223. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Rft223 if any.
     *
     * @param rft223 Details of the Rft223 to be created; value cannot be null.
     * @return The newly created Rft223.
     */
	Rft223 create(@Valid Rft223 rft223);


	/**
	 * Returns Rft223 by given id if exists.
	 *
	 * @param rft223Id The id of the Rft223 to get; value cannot be null.
	 * @return Rft223 associated with the given rft223Id.
     * @throws EntityNotFoundException If no Rft223 is found.
	 */
	Rft223 getById(Rft223Id rft223Id) throws EntityNotFoundException;

    /**
	 * Find and return the Rft223 by given id if exists, returns null otherwise.
	 *
	 * @param rft223Id The id of the Rft223 to get; value cannot be null.
	 * @return Rft223 associated with the given rft223Id.
	 */
	Rft223 findById(Rft223Id rft223Id);


	/**
	 * Updates the details of an existing Rft223. It replaces all fields of the existing Rft223 with the given rft223.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Rft223 if any.
     *
	 * @param rft223 The details of the Rft223 to be updated; value cannot be null.
	 * @return The updated Rft223.
	 * @throws EntityNotFoundException if no Rft223 is found with given input.
	 */
	Rft223 update(@Valid Rft223 rft223) throws EntityNotFoundException;

    /**
	 * Deletes an existing Rft223 with the given id.
	 *
	 * @param rft223Id The id of the Rft223 to be deleted; value cannot be null.
	 * @return The deleted Rft223.
	 * @throws EntityNotFoundException if no Rft223 found with the given id.
	 */
	Rft223 delete(Rft223Id rft223Id) throws EntityNotFoundException;

    /**
	 * Deletes an existing Rft223 with the given object.
	 *
	 * @param rft223 The instance of the Rft223 to be deleted; value cannot be null.
	 */
	void delete(Rft223 rft223);

	/**
	 * Find all Rft223s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Rft223s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Rft223> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Rft223s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Rft223s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Rft223> findAll(String query, Pageable pageable);

    /**
	 * Exports all Rft223s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Rft223s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Rft223.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

