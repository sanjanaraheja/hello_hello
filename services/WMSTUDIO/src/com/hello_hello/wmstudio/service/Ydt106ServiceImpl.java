/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Ydt106;
import com.hello_hello.wmstudio.Ydt106Id;


/**
 * ServiceImpl object for domain model class Ydt106.
 *
 * @see Ydt106
 */
@Service("WMSTUDIO.Ydt106Service")
@Validated
public class Ydt106ServiceImpl implements Ydt106Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ydt106ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Ydt106Dao")
    private WMGenericDao<Ydt106, Ydt106Id> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Ydt106, Ydt106Id> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Ydt106 create(Ydt106 ydt106) {
        LOGGER.debug("Creating a new Ydt106 with information: {}", ydt106);

        Ydt106 ydt106Created = this.wmGenericDao.create(ydt106);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(ydt106Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Ydt106 getById(Ydt106Id ydt106Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Ydt106 by id: {}", ydt106Id);
        return this.wmGenericDao.findById(ydt106Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Ydt106 findById(Ydt106Id ydt106Id) {
        LOGGER.debug("Finding Ydt106 by id: {}", ydt106Id);
        try {
            return this.wmGenericDao.findById(ydt106Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Ydt106 found with id: {}", ydt106Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Ydt106 update(Ydt106 ydt106) throws EntityNotFoundException {
        LOGGER.debug("Updating Ydt106 with information: {}", ydt106);

        this.wmGenericDao.update(ydt106);
        this.wmGenericDao.refresh(ydt106);

        return ydt106;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Ydt106 delete(Ydt106Id ydt106Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Ydt106 with id: {}", ydt106Id);
        Ydt106 deleted = this.wmGenericDao.findById(ydt106Id);
        if (deleted == null) {
            LOGGER.debug("No Ydt106 found with id: {}", ydt106Id);
            throw new EntityNotFoundException(String.valueOf(ydt106Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Ydt106 ydt106) {
        LOGGER.debug("Deleting Ydt106 with {}", ydt106);
        this.wmGenericDao.delete(ydt106);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Ydt106> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Ydt106s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Ydt106> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Ydt106s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Ydt106 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

