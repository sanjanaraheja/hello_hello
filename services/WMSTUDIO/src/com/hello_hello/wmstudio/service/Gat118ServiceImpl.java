/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hello_hello.wmstudio.Gat118;
import com.hello_hello.wmstudio.Gat118Id;


/**
 * ServiceImpl object for domain model class Gat118.
 *
 * @see Gat118
 */
@Service("WMSTUDIO.Gat118Service")
@Validated
public class Gat118ServiceImpl implements Gat118Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Gat118ServiceImpl.class);


    @Autowired
    @Qualifier("WMSTUDIO.Gat118Dao")
    private WMGenericDao<Gat118, Gat118Id> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Gat118, Gat118Id> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
    @Override
	public Gat118 create(Gat118 gat118) {
        LOGGER.debug("Creating a new Gat118 with information: {}", gat118);

        Gat118 gat118Created = this.wmGenericDao.create(gat118);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(gat118Created);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat118 getById(Gat118Id gat118Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Gat118 by id: {}", gat118Id);
        return this.wmGenericDao.findById(gat118Id);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat118 findById(Gat118Id gat118Id) {
        LOGGER.debug("Finding Gat118 by id: {}", gat118Id);
        try {
            return this.wmGenericDao.findById(gat118Id);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Gat118 found with id: {}", gat118Id, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "WMSTUDIOTransactionManager")
	@Override
	public Gat118 update(Gat118 gat118) throws EntityNotFoundException {
        LOGGER.debug("Updating Gat118 with information: {}", gat118);

        this.wmGenericDao.update(gat118);
        this.wmGenericDao.refresh(gat118);

        return gat118;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public Gat118 delete(Gat118Id gat118Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Gat118 with id: {}", gat118Id);
        Gat118 deleted = this.wmGenericDao.findById(gat118Id);
        if (deleted == null) {
            LOGGER.debug("No Gat118 found with id: {}", gat118Id);
            throw new EntityNotFoundException(String.valueOf(gat118Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WMSTUDIOTransactionManager")
	@Override
	public void delete(Gat118 gat118) {
        LOGGER.debug("Deleting Gat118 with {}", gat118);
        this.wmGenericDao.delete(gat118);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public Page<Gat118> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Gat118s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Page<Gat118> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Gat118s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WMSTUDIO for table Gat118 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WMSTUDIOTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

