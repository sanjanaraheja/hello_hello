/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Ort200 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`ORT200`")
@IdClass(Ort200Id.class)
public class Ort200 implements Serializable {

    private String ordnum;
    private String pckcod;
    private String gmtseq;
    private Byte clrseq;
    private String animgp;
    private byte pckunt;
    private Byte finPckunt;

    @Id
    @Column(name = "`ORDNUM`", nullable = false, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Id
    @Column(name = "`PCKCOD`", nullable = false, length = 2)
    public String getPckcod() {
        return this.pckcod;
    }

    public void setPckcod(String pckcod) {
        this.pckcod = pckcod;
    }

    @Id
    @Column(name = "`GMTSEQ`", nullable = false, length = 2)
    public String getGmtseq() {
        return this.gmtseq;
    }

    public void setGmtseq(String gmtseq) {
        this.gmtseq = gmtseq;
    }

    @Id
    @Column(name = "`CLRSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getClrseq() {
        return this.clrseq;
    }

    public void setClrseq(Byte clrseq) {
        this.clrseq = clrseq;
    }

    @Id
    @Column(name = "`ANIMGP`", nullable = false, length = 3)
    public String getAnimgp() {
        return this.animgp;
    }

    public void setAnimgp(String animgp) {
        this.animgp = animgp;
    }

    @Column(name = "`PCKUNT`", nullable = false, scale = 0, precision = 2)
    public byte getPckunt() {
        return this.pckunt;
    }

    public void setPckunt(byte pckunt) {
        this.pckunt = pckunt;
    }

    @Column(name = "`FIN_PCKUNT`", nullable = true, scale = 0, precision = 2)
    public Byte getFinPckunt() {
        return this.finPckunt;
    }

    public void setFinPckunt(Byte finPckunt) {
        this.finPckunt = finPckunt;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ort200)) return false;
        final Ort200 ort200 = (Ort200) o;
        return Objects.equals(getOrdnum(), ort200.getOrdnum()) &&
                Objects.equals(getPckcod(), ort200.getPckcod()) &&
                Objects.equals(getGmtseq(), ort200.getGmtseq()) &&
                Objects.equals(getClrseq(), ort200.getClrseq()) &&
                Objects.equals(getAnimgp(), ort200.getAnimgp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getPckcod(),
                getGmtseq(),
                getClrseq(),
                getAnimgp());
    }
}

