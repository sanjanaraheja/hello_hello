/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Gat118 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`GAT118`")
@IdClass(Gat118Id.class)
public class Gat118 implements Serializable {

    private String ordnum;
    private Short acsseq;
    private String gmtseq;
    private Byte consseq;
    private Byte pddszeseq;
    private Byte szeseq;
    private Byte fabseq;
    private String acsdim;
    private Float nopce;
    private Double amtreq;
    private String usrid;
    private LocalDateTime usrdate;

    @Id
    @Column(name = "`ORDNUM`", nullable = false, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Id
    @Column(name = "`ACSSEQ`", nullable = false, scale = 0, precision = 3)
    public Short getAcsseq() {
        return this.acsseq;
    }

    public void setAcsseq(Short acsseq) {
        this.acsseq = acsseq;
    }

    @Id
    @Column(name = "`GMTSEQ`", nullable = false, length = 2)
    public String getGmtseq() {
        return this.gmtseq;
    }

    public void setGmtseq(String gmtseq) {
        this.gmtseq = gmtseq;
    }

    @Id
    @Column(name = "`CONSSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getConsseq() {
        return this.consseq;
    }

    public void setConsseq(Byte consseq) {
        this.consseq = consseq;
    }

    @Id
    @Column(name = "`PDDSZESEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getPddszeseq() {
        return this.pddszeseq;
    }

    public void setPddszeseq(Byte pddszeseq) {
        this.pddszeseq = pddszeseq;
    }

    @Column(name = "`SZESEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getSzeseq() {
        return this.szeseq;
    }

    public void setSzeseq(Byte szeseq) {
        this.szeseq = szeseq;
    }

    @Column(name = "`FABSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    @Column(name = "`ACSDIM`", nullable = true, length = 45)
    public String getAcsdim() {
        return this.acsdim;
    }

    public void setAcsdim(String acsdim) {
        this.acsdim = acsdim;
    }

    @Column(name = "`NOPCE`", nullable = true, scale = 1, precision = 2)
    public Float getNopce() {
        return this.nopce;
    }

    public void setNopce(Float nopce) {
        this.nopce = nopce;
    }

    @Column(name = "`AMTREQ`", nullable = true, scale = 5, precision = 10)
    public Double getAmtreq() {
        return this.amtreq;
    }

    public void setAmtreq(Double amtreq) {
        this.amtreq = amtreq;
    }

    @Column(name = "`USRID`", nullable = true, length = 6)
    public String getUsrid() {
        return this.usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    @Column(name = "`USRDATE`", nullable = true)
    public LocalDateTime getUsrdate() {
        return this.usrdate;
    }

    public void setUsrdate(LocalDateTime usrdate) {
        this.usrdate = usrdate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gat118)) return false;
        final Gat118 gat118 = (Gat118) o;
        return Objects.equals(getOrdnum(), gat118.getOrdnum()) &&
                Objects.equals(getAcsseq(), gat118.getAcsseq()) &&
                Objects.equals(getGmtseq(), gat118.getGmtseq()) &&
                Objects.equals(getConsseq(), gat118.getConsseq()) &&
                Objects.equals(getPddszeseq(), gat118.getPddszeseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getAcsseq(),
                getGmtseq(),
                getConsseq(),
                getPddszeseq());
    }
}

