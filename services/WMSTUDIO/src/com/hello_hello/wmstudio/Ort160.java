/*Copyright (c) 2016-2017 Pramati.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hello_hello.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Ort160 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`ORT160`")
@IdClass(Ort160Id.class)
public class Ort160 implements Serializable {

    private String ordnum;
    private Byte clrseq;
    private Byte fabseq;
    private Double fabrto;
    private Double amtreq;
    private Double wgtreq;
    private Float pceperkg;
    private Double lenreq;
    private Byte optseq;
    private Float fbamtreq;
    private Float psamtreq;
    private Integer pcsreq;
    private String usrid;
    private LocalDateTime usrdate;
    private Float pcspersurp;
    private Short pcsindsurp;
    private Double uexcrto;
    private Double uincrto;
    private Double kexcrto;
    private Double kincrto;
    private Float nwaste;
    private Double cuamtreq;
    private Double cukgsreq;
    private String recalcreq;
    private Double dystkamt;
    private Double dyamtreq;
    private Double dykgsreq;
    private Double olduexcrto;
    private Double olduincrto;
    private String recalcpcs;
    private String recalcrto;
    private Byte blkhangers;
    private Double lstUexcrto;
    private Double lstUincrto;
    private Double lstKexcrto;
    private Double lstKincrto;
    private Byte lstOptseq;
    private String miswasteflag;
    private Float genwaste;
    private Double calcamtreq;
    private Double diffAmtreq;
    private String marcode;
    private Double nopcs;
    private String lstMarcode;
    private Double lstNopcs;
    private Double marlgth;
    private Double lstMarlgth;
    private String allind;
    private String ttrefno;
    private String ttrefReqind;
    private String ttrefReqby;
    private LocalDateTime ttrefReqdat;
    private Double amtreqAftkds;
    private Float genwasteAop;
    private Float markerwid;
    private Float markerefficiency;
    private String skewremoved;
    private Float lstMarkerwid;
    private Float lstMarkerefficiency;
    private String lstSkewremoved;
    private Float genwasteSelvwdth;
    private String genwasteHeatsetreq;
    private String genwasteOldkdsNotemplate;
    private String dsgreqProposaloption;
    private LocalDateTime dsgreqModdat;
    private String dsgreqModby;
    private LocalDateTime edyedat;
    private String edyeremarks;
    private String markereffReacode;
    private String markereffRemarks;
    private String lstMarkereffReacode;
    private String lstMarkereffRemarks;
    private String inirtoDiffReacode;
    private String inirtoDiffRemarks;
    private Float genwasteFringe;
    private String updmrkprogid;
    private String recalcmrk;
    private String lstChangeId;
    private String genwasteSkumatrixidSubseq;
    private Float genwasteSku;
    private String genwasteStddyeWasteid;
    private Double sowasteDyeamt;
    private String sowasteAmtuom;
    private Double amtreqFmavg;
    private Double cuamtreqFmavg;
    private Double cukgsreqFmavg;
    private Double bdwasteCuamtreq;
    private Double dyeamtreqFmavg;
    private Double dyekgsreqFmavg;
    private Double bdwasteMaxamtreq;
    private Double bdwasteSumamtreqmainfab;
    private Double bdwasteAmtreqwithoutbd;
    private Integer pcsaftsurp;
    private Float genwasteSelvpc;
    private Float genwasteInistdaop;
    private Float genwasteStdaop;
    private Float genwasteBrush;
    private Float genwasteStddyefn;
    private Float genwasteLshearing;
    private Float genwasteHshearing;
    private String genwasteBrushreq;
    private Float genwasteInitotal;
    private Float genwasteIntfmw;
    private String genwasteIntfmwScnrid;
    private Double genwasteIntfmwInidyamtreq;
    private Double genwasteIntfmwInidykgreq;
    private Double genwasteIntfmwInikntamt;
    private Double genwasteIntfmwIninwaste;
    private Float genwasteIntfmwNumbatch;
    private Double genwasteIntfmwDyamtrmv;
    private Float genwasteSuede;
    private String genwasteCalcmethod;
    private String amtreqCalcmethod;
    private Float bakbefM1Genwaste;
    private Float bakbefM1Nwaste;
    private Short pcsrequiredadj;
    private Ort140 ort140;
    private Ort120 ort120;

    @Id
    @Column(name = "`ORDNUM`", nullable = false, length = 12)
    public String getOrdnum() {
        return this.ordnum;
    }

    public void setOrdnum(String ordnum) {
        this.ordnum = ordnum;
    }

    @Id
    @Column(name = "`CLRSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getClrseq() {
        return this.clrseq;
    }

    public void setClrseq(Byte clrseq) {
        this.clrseq = clrseq;
    }

    @Id
    @Column(name = "`FABSEQ`", nullable = false, scale = 0, precision = 2)
    public Byte getFabseq() {
        return this.fabseq;
    }

    public void setFabseq(Byte fabseq) {
        this.fabseq = fabseq;
    }

    @Column(name = "`FABRTO`", nullable = true, scale = 4, precision = 10)
    public Double getFabrto() {
        return this.fabrto;
    }

    public void setFabrto(Double fabrto) {
        this.fabrto = fabrto;
    }

    @Column(name = "`AMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getAmtreq() {
        return this.amtreq;
    }

    public void setAmtreq(Double amtreq) {
        this.amtreq = amtreq;
    }

    @Column(name = "`WGTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getWgtreq() {
        return this.wgtreq;
    }

    public void setWgtreq(Double wgtreq) {
        this.wgtreq = wgtreq;
    }

    @Column(name = "`PCEPERKG`", nullable = true, scale = 2, precision = 6)
    public Float getPceperkg() {
        return this.pceperkg;
    }

    public void setPceperkg(Float pceperkg) {
        this.pceperkg = pceperkg;
    }

    @Column(name = "`LENREQ`", nullable = true, scale = 2, precision = 8)
    public Double getLenreq() {
        return this.lenreq;
    }

    public void setLenreq(Double lenreq) {
        this.lenreq = lenreq;
    }

    @Column(name = "`OPTSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getOptseq() {
        return this.optseq;
    }

    public void setOptseq(Byte optseq) {
        this.optseq = optseq;
    }

    @Column(name = "`FBAMTREQ`", nullable = true, scale = 2, precision = 4)
    public Float getFbamtreq() {
        return this.fbamtreq;
    }

    public void setFbamtreq(Float fbamtreq) {
        this.fbamtreq = fbamtreq;
    }

    @Column(name = "`PSAMTREQ`", nullable = true, scale = 2, precision = 4)
    public Float getPsamtreq() {
        return this.psamtreq;
    }

    public void setPsamtreq(Float psamtreq) {
        this.psamtreq = psamtreq;
    }

    @Column(name = "`PCSREQ`", nullable = true, scale = 0, precision = 6)
    public Integer getPcsreq() {
        return this.pcsreq;
    }

    public void setPcsreq(Integer pcsreq) {
        this.pcsreq = pcsreq;
    }

    @Column(name = "`USRID`", nullable = true, length = 6)
    public String getUsrid() {
        return this.usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    @Column(name = "`USRDATE`", nullable = true)
    public LocalDateTime getUsrdate() {
        return this.usrdate;
    }

    public void setUsrdate(LocalDateTime usrdate) {
        this.usrdate = usrdate;
    }

    @Column(name = "`PCSPERSURP`", nullable = true, scale = 2, precision = 6)
    public Float getPcspersurp() {
        return this.pcspersurp;
    }

    public void setPcspersurp(Float pcspersurp) {
        this.pcspersurp = pcspersurp;
    }

    @Column(name = "`PCSINDSURP`", nullable = true, scale = 0, precision = 5)
    public Short getPcsindsurp() {
        return this.pcsindsurp;
    }

    public void setPcsindsurp(Short pcsindsurp) {
        this.pcsindsurp = pcsindsurp;
    }

    @Column(name = "`UEXCRTO`", nullable = true, scale = 4, precision = 8)
    public Double getUexcrto() {
        return this.uexcrto;
    }

    public void setUexcrto(Double uexcrto) {
        this.uexcrto = uexcrto;
    }

    @Column(name = "`UINCRTO`", nullable = true, scale = 4, precision = 8)
    public Double getUincrto() {
        return this.uincrto;
    }

    public void setUincrto(Double uincrto) {
        this.uincrto = uincrto;
    }

    @Column(name = "`KEXCRTO`", nullable = true, scale = 4, precision = 10)
    public Double getKexcrto() {
        return this.kexcrto;
    }

    public void setKexcrto(Double kexcrto) {
        this.kexcrto = kexcrto;
    }

    @Column(name = "`KINCRTO`", nullable = true, scale = 4, precision = 10)
    public Double getKincrto() {
        return this.kincrto;
    }

    public void setKincrto(Double kincrto) {
        this.kincrto = kincrto;
    }

    @Column(name = "`NWASTE`", nullable = true, scale = 2, precision = 6)
    public Float getNwaste() {
        return this.nwaste;
    }

    public void setNwaste(Float nwaste) {
        this.nwaste = nwaste;
    }

    @Column(name = "`CUAMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getCuamtreq() {
        return this.cuamtreq;
    }

    public void setCuamtreq(Double cuamtreq) {
        this.cuamtreq = cuamtreq;
    }

    @Column(name = "`CUKGSREQ`", nullable = true, scale = 2, precision = 8)
    public Double getCukgsreq() {
        return this.cukgsreq;
    }

    public void setCukgsreq(Double cukgsreq) {
        this.cukgsreq = cukgsreq;
    }

    @Column(name = "`RECALCREQ`", nullable = true, length = 1)
    public String getRecalcreq() {
        return this.recalcreq;
    }

    public void setRecalcreq(String recalcreq) {
        this.recalcreq = recalcreq;
    }

    @Column(name = "`DYSTKAMT`", nullable = true, scale = 2, precision = 8)
    public Double getDystkamt() {
        return this.dystkamt;
    }

    public void setDystkamt(Double dystkamt) {
        this.dystkamt = dystkamt;
    }

    @Column(name = "`DYAMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getDyamtreq() {
        return this.dyamtreq;
    }

    public void setDyamtreq(Double dyamtreq) {
        this.dyamtreq = dyamtreq;
    }

    @Column(name = "`DYKGSREQ`", nullable = true, scale = 2, precision = 8)
    public Double getDykgsreq() {
        return this.dykgsreq;
    }

    public void setDykgsreq(Double dykgsreq) {
        this.dykgsreq = dykgsreq;
    }

    @Column(name = "`OLDUEXCRTO`", nullable = true, scale = 4, precision = 8)
    public Double getOlduexcrto() {
        return this.olduexcrto;
    }

    public void setOlduexcrto(Double olduexcrto) {
        this.olduexcrto = olduexcrto;
    }

    @Column(name = "`OLDUINCRTO`", nullable = true, scale = 4, precision = 8)
    public Double getOlduincrto() {
        return this.olduincrto;
    }

    public void setOlduincrto(Double olduincrto) {
        this.olduincrto = olduincrto;
    }

    @Column(name = "`RECALCPCS`", nullable = true, length = 1)
    public String getRecalcpcs() {
        return this.recalcpcs;
    }

    public void setRecalcpcs(String recalcpcs) {
        this.recalcpcs = recalcpcs;
    }

    @Column(name = "`RECALCRTO`", nullable = true, length = 1)
    public String getRecalcrto() {
        return this.recalcrto;
    }

    public void setRecalcrto(String recalcrto) {
        this.recalcrto = recalcrto;
    }

    @Column(name = "`BLKHANGERS`", nullable = true, scale = 0, precision = 2)
    public Byte getBlkhangers() {
        return this.blkhangers;
    }

    public void setBlkhangers(Byte blkhangers) {
        this.blkhangers = blkhangers;
    }

    @Column(name = "`LST_UEXCRTO`", nullable = true, scale = 4, precision = 8)
    public Double getLstUexcrto() {
        return this.lstUexcrto;
    }

    public void setLstUexcrto(Double lstUexcrto) {
        this.lstUexcrto = lstUexcrto;
    }

    @Column(name = "`LST_UINCRTO`", nullable = true, scale = 4, precision = 8)
    public Double getLstUincrto() {
        return this.lstUincrto;
    }

    public void setLstUincrto(Double lstUincrto) {
        this.lstUincrto = lstUincrto;
    }

    @Column(name = "`LST_KEXCRTO`", nullable = true, scale = 4, precision = 10)
    public Double getLstKexcrto() {
        return this.lstKexcrto;
    }

    public void setLstKexcrto(Double lstKexcrto) {
        this.lstKexcrto = lstKexcrto;
    }

    @Column(name = "`LST_KINCRTO`", nullable = true, scale = 4, precision = 10)
    public Double getLstKincrto() {
        return this.lstKincrto;
    }

    public void setLstKincrto(Double lstKincrto) {
        this.lstKincrto = lstKincrto;
    }

    @Column(name = "`LST_OPTSEQ`", nullable = true, scale = 0, precision = 2)
    public Byte getLstOptseq() {
        return this.lstOptseq;
    }

    public void setLstOptseq(Byte lstOptseq) {
        this.lstOptseq = lstOptseq;
    }

    @Column(name = "`MISWASTEFLAG`", nullable = true, length = 1)
    public String getMiswasteflag() {
        return this.miswasteflag;
    }

    public void setMiswasteflag(String miswasteflag) {
        this.miswasteflag = miswasteflag;
    }

    @Column(name = "`GENWASTE`", nullable = true, scale = 2, precision = 6)
    public Float getGenwaste() {
        return this.genwaste;
    }

    public void setGenwaste(Float genwaste) {
        this.genwaste = genwaste;
    }

    @Column(name = "`CALCAMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getCalcamtreq() {
        return this.calcamtreq;
    }

    public void setCalcamtreq(Double calcamtreq) {
        this.calcamtreq = calcamtreq;
    }

    @Column(name = "`DIFF_AMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getDiffAmtreq() {
        return this.diffAmtreq;
    }

    public void setDiffAmtreq(Double diffAmtreq) {
        this.diffAmtreq = diffAmtreq;
    }

    @Column(name = "`MARCODE`", nullable = true, length = 100)
    public String getMarcode() {
        return this.marcode;
    }

    public void setMarcode(String marcode) {
        this.marcode = marcode;
    }

    @Column(name = "`NOPCS`", nullable = true, scale = 2, precision = 8)
    public Double getNopcs() {
        return this.nopcs;
    }

    public void setNopcs(Double nopcs) {
        this.nopcs = nopcs;
    }

    @Column(name = "`LST_MARCODE`", nullable = true, length = 100)
    public String getLstMarcode() {
        return this.lstMarcode;
    }

    public void setLstMarcode(String lstMarcode) {
        this.lstMarcode = lstMarcode;
    }

    @Column(name = "`LST_NOPCS`", nullable = true, scale = 2, precision = 8)
    public Double getLstNopcs() {
        return this.lstNopcs;
    }

    public void setLstNopcs(Double lstNopcs) {
        this.lstNopcs = lstNopcs;
    }

    @Column(name = "`MARLGTH`", nullable = true, scale = 4, precision = 10)
    public Double getMarlgth() {
        return this.marlgth;
    }

    public void setMarlgth(Double marlgth) {
        this.marlgth = marlgth;
    }

    @Column(name = "`LST_MARLGTH`", nullable = true, scale = 4, precision = 10)
    public Double getLstMarlgth() {
        return this.lstMarlgth;
    }

    public void setLstMarlgth(Double lstMarlgth) {
        this.lstMarlgth = lstMarlgth;
    }

    @Column(name = "`ALLIND`", nullable = true, length = 1)
    public String getAllind() {
        return this.allind;
    }

    public void setAllind(String allind) {
        this.allind = allind;
    }

    @Column(name = "`TTREFNO`", nullable = true, length = 13)
    public String getTtrefno() {
        return this.ttrefno;
    }

    public void setTtrefno(String ttrefno) {
        this.ttrefno = ttrefno;
    }

    @Column(name = "`TTREF_REQIND`", nullable = true, length = 1)
    public String getTtrefReqind() {
        return this.ttrefReqind;
    }

    public void setTtrefReqind(String ttrefReqind) {
        this.ttrefReqind = ttrefReqind;
    }

    @Column(name = "`TTREF_REQBY`", nullable = true, length = 6)
    public String getTtrefReqby() {
        return this.ttrefReqby;
    }

    public void setTtrefReqby(String ttrefReqby) {
        this.ttrefReqby = ttrefReqby;
    }

    @Column(name = "`TTREF_REQDAT`", nullable = true)
    public LocalDateTime getTtrefReqdat() {
        return this.ttrefReqdat;
    }

    public void setTtrefReqdat(LocalDateTime ttrefReqdat) {
        this.ttrefReqdat = ttrefReqdat;
    }

    @Column(name = "`AMTREQ_AFTKDS`", nullable = true, scale = 2, precision = 8)
    public Double getAmtreqAftkds() {
        return this.amtreqAftkds;
    }

    public void setAmtreqAftkds(Double amtreqAftkds) {
        this.amtreqAftkds = amtreqAftkds;
    }

    @Column(name = "`GENWASTE_AOP`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteAop() {
        return this.genwasteAop;
    }

    public void setGenwasteAop(Float genwasteAop) {
        this.genwasteAop = genwasteAop;
    }

    @Column(name = "`MARKERWID`", nullable = true, scale = 1, precision = 4)
    public Float getMarkerwid() {
        return this.markerwid;
    }

    public void setMarkerwid(Float markerwid) {
        this.markerwid = markerwid;
    }

    @Column(name = "`MARKEREFFICIENCY`", nullable = true, scale = 2, precision = 5)
    public Float getMarkerefficiency() {
        return this.markerefficiency;
    }

    public void setMarkerefficiency(Float markerefficiency) {
        this.markerefficiency = markerefficiency;
    }

    @Column(name = "`SKEWREMOVED`", nullable = true, length = 1)
    public String getSkewremoved() {
        return this.skewremoved;
    }

    public void setSkewremoved(String skewremoved) {
        this.skewremoved = skewremoved;
    }

    @Column(name = "`LST_MARKERWID`", nullable = true, scale = 1, precision = 4)
    public Float getLstMarkerwid() {
        return this.lstMarkerwid;
    }

    public void setLstMarkerwid(Float lstMarkerwid) {
        this.lstMarkerwid = lstMarkerwid;
    }

    @Column(name = "`LST_MARKEREFFICIENCY`", nullable = true, scale = 2, precision = 5)
    public Float getLstMarkerefficiency() {
        return this.lstMarkerefficiency;
    }

    public void setLstMarkerefficiency(Float lstMarkerefficiency) {
        this.lstMarkerefficiency = lstMarkerefficiency;
    }

    @Column(name = "`LST_SKEWREMOVED`", nullable = true, length = 1)
    public String getLstSkewremoved() {
        return this.lstSkewremoved;
    }

    public void setLstSkewremoved(String lstSkewremoved) {
        this.lstSkewremoved = lstSkewremoved;
    }

    @Column(name = "`GENWASTE_SELVWDTH`", nullable = true, scale = 1, precision = 5)
    public Float getGenwasteSelvwdth() {
        return this.genwasteSelvwdth;
    }

    public void setGenwasteSelvwdth(Float genwasteSelvwdth) {
        this.genwasteSelvwdth = genwasteSelvwdth;
    }

    @Column(name = "`GENWASTE_HEATSETREQ`", nullable = true, length = 1)
    public String getGenwasteHeatsetreq() {
        return this.genwasteHeatsetreq;
    }

    public void setGenwasteHeatsetreq(String genwasteHeatsetreq) {
        this.genwasteHeatsetreq = genwasteHeatsetreq;
    }

    @Column(name = "`GENWASTE_OLDKDS_NOTEMPLATE`", nullable = true, length = 1)
    public String getGenwasteOldkdsNotemplate() {
        return this.genwasteOldkdsNotemplate;
    }

    public void setGenwasteOldkdsNotemplate(String genwasteOldkdsNotemplate) {
        this.genwasteOldkdsNotemplate = genwasteOldkdsNotemplate;
    }

    @Column(name = "`DSGREQ_PROPOSALOPTION`", nullable = true, length = 15)
    public String getDsgreqProposaloption() {
        return this.dsgreqProposaloption;
    }

    public void setDsgreqProposaloption(String dsgreqProposaloption) {
        this.dsgreqProposaloption = dsgreqProposaloption;
    }

    @Column(name = "`DSGREQ_MODDAT`", nullable = true)
    public LocalDateTime getDsgreqModdat() {
        return this.dsgreqModdat;
    }

    public void setDsgreqModdat(LocalDateTime dsgreqModdat) {
        this.dsgreqModdat = dsgreqModdat;
    }

    @Column(name = "`DSGREQ_MODBY`", nullable = true, length = 6)
    public String getDsgreqModby() {
        return this.dsgreqModby;
    }

    public void setDsgreqModby(String dsgreqModby) {
        this.dsgreqModby = dsgreqModby;
    }

    @Column(name = "`EDYEDAT`", nullable = true)
    public LocalDateTime getEdyedat() {
        return this.edyedat;
    }

    public void setEdyedat(LocalDateTime edyedat) {
        this.edyedat = edyedat;
    }

    @Column(name = "`EDYEREMARKS`", nullable = true, length = 4000)
    public String getEdyeremarks() {
        return this.edyeremarks;
    }

    public void setEdyeremarks(String edyeremarks) {
        this.edyeremarks = edyeremarks;
    }

    @Column(name = "`MARKEREFF_REACODE`", nullable = true, length = 6)
    public String getMarkereffReacode() {
        return this.markereffReacode;
    }

    public void setMarkereffReacode(String markereffReacode) {
        this.markereffReacode = markereffReacode;
    }

    @Column(name = "`MARKEREFF_REMARKS`", nullable = true, length = 50)
    public String getMarkereffRemarks() {
        return this.markereffRemarks;
    }

    public void setMarkereffRemarks(String markereffRemarks) {
        this.markereffRemarks = markereffRemarks;
    }

    @Column(name = "`LST_MARKEREFF_REACODE`", nullable = true, length = 6)
    public String getLstMarkereffReacode() {
        return this.lstMarkereffReacode;
    }

    public void setLstMarkereffReacode(String lstMarkereffReacode) {
        this.lstMarkereffReacode = lstMarkereffReacode;
    }

    @Column(name = "`LST_MARKEREFF_REMARKS`", nullable = true, length = 50)
    public String getLstMarkereffRemarks() {
        return this.lstMarkereffRemarks;
    }

    public void setLstMarkereffRemarks(String lstMarkereffRemarks) {
        this.lstMarkereffRemarks = lstMarkereffRemarks;
    }

    @Column(name = "`INIRTO_DIFF_REACODE`", nullable = true, length = 6)
    public String getInirtoDiffReacode() {
        return this.inirtoDiffReacode;
    }

    public void setInirtoDiffReacode(String inirtoDiffReacode) {
        this.inirtoDiffReacode = inirtoDiffReacode;
    }

    @Column(name = "`INIRTO_DIFF_REMARKS`", nullable = true, length = 100)
    public String getInirtoDiffRemarks() {
        return this.inirtoDiffRemarks;
    }

    public void setInirtoDiffRemarks(String inirtoDiffRemarks) {
        this.inirtoDiffRemarks = inirtoDiffRemarks;
    }

    @Column(name = "`GENWASTE_FRINGE`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteFringe() {
        return this.genwasteFringe;
    }

    public void setGenwasteFringe(Float genwasteFringe) {
        this.genwasteFringe = genwasteFringe;
    }

    @Column(name = "`UPDMRKPROGID`", nullable = true, length = 9)
    public String getUpdmrkprogid() {
        return this.updmrkprogid;
    }

    public void setUpdmrkprogid(String updmrkprogid) {
        this.updmrkprogid = updmrkprogid;
    }

    @Column(name = "`RECALCMRK`", nullable = true, length = 1)
    public String getRecalcmrk() {
        return this.recalcmrk;
    }

    public void setRecalcmrk(String recalcmrk) {
        this.recalcmrk = recalcmrk;
    }

    @Column(name = "`LST_CHANGE_ID`", nullable = true, length = 10)
    public String getLstChangeId() {
        return this.lstChangeId;
    }

    public void setLstChangeId(String lstChangeId) {
        this.lstChangeId = lstChangeId;
    }

    @Column(name = "`GENWASTE_SKUMATRIXID_SUBSEQ`", nullable = true, length = 20)
    public String getGenwasteSkumatrixidSubseq() {
        return this.genwasteSkumatrixidSubseq;
    }

    public void setGenwasteSkumatrixidSubseq(String genwasteSkumatrixidSubseq) {
        this.genwasteSkumatrixidSubseq = genwasteSkumatrixidSubseq;
    }

    @Column(name = "`GENWASTE_SKU`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteSku() {
        return this.genwasteSku;
    }

    public void setGenwasteSku(Float genwasteSku) {
        this.genwasteSku = genwasteSku;
    }

    @Column(name = "`GENWASTE_STDDYE_WASTEID`", nullable = true, length = 8)
    public String getGenwasteStddyeWasteid() {
        return this.genwasteStddyeWasteid;
    }

    public void setGenwasteStddyeWasteid(String genwasteStddyeWasteid) {
        this.genwasteStddyeWasteid = genwasteStddyeWasteid;
    }

    @Column(name = "`SOWASTE_DYEAMT`", nullable = true, scale = 2, precision = 8)
    public Double getSowasteDyeamt() {
        return this.sowasteDyeamt;
    }

    public void setSowasteDyeamt(Double sowasteDyeamt) {
        this.sowasteDyeamt = sowasteDyeamt;
    }

    @Column(name = "`SOWASTE_AMTUOM`", nullable = true, length = 3)
    public String getSowasteAmtuom() {
        return this.sowasteAmtuom;
    }

    public void setSowasteAmtuom(String sowasteAmtuom) {
        this.sowasteAmtuom = sowasteAmtuom;
    }

    @Column(name = "`AMTREQ_FMAVG`", nullable = true, scale = 2, precision = 8)
    public Double getAmtreqFmavg() {
        return this.amtreqFmavg;
    }

    public void setAmtreqFmavg(Double amtreqFmavg) {
        this.amtreqFmavg = amtreqFmavg;
    }

    @Column(name = "`CUAMTREQ_FMAVG`", nullable = true, scale = 2, precision = 8)
    public Double getCuamtreqFmavg() {
        return this.cuamtreqFmavg;
    }

    public void setCuamtreqFmavg(Double cuamtreqFmavg) {
        this.cuamtreqFmavg = cuamtreqFmavg;
    }

    @Column(name = "`CUKGSREQ_FMAVG`", nullable = true, scale = 2, precision = 8)
    public Double getCukgsreqFmavg() {
        return this.cukgsreqFmavg;
    }

    public void setCukgsreqFmavg(Double cukgsreqFmavg) {
        this.cukgsreqFmavg = cukgsreqFmavg;
    }

    @Column(name = "`BDWASTE_CUAMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getBdwasteCuamtreq() {
        return this.bdwasteCuamtreq;
    }

    public void setBdwasteCuamtreq(Double bdwasteCuamtreq) {
        this.bdwasteCuamtreq = bdwasteCuamtreq;
    }

    @Column(name = "`DYEAMTREQ_FMAVG`", nullable = true, scale = 2, precision = 8)
    public Double getDyeamtreqFmavg() {
        return this.dyeamtreqFmavg;
    }

    public void setDyeamtreqFmavg(Double dyeamtreqFmavg) {
        this.dyeamtreqFmavg = dyeamtreqFmavg;
    }

    @Column(name = "`DYEKGSREQ_FMAVG`", nullable = true, scale = 2, precision = 8)
    public Double getDyekgsreqFmavg() {
        return this.dyekgsreqFmavg;
    }

    public void setDyekgsreqFmavg(Double dyekgsreqFmavg) {
        this.dyekgsreqFmavg = dyekgsreqFmavg;
    }

    @Column(name = "`BDWASTE_MAXAMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getBdwasteMaxamtreq() {
        return this.bdwasteMaxamtreq;
    }

    public void setBdwasteMaxamtreq(Double bdwasteMaxamtreq) {
        this.bdwasteMaxamtreq = bdwasteMaxamtreq;
    }

    @Column(name = "`BDWASTE_SUMAMTREQMAINFAB`", nullable = true, scale = 2, precision = 8)
    public Double getBdwasteSumamtreqmainfab() {
        return this.bdwasteSumamtreqmainfab;
    }

    public void setBdwasteSumamtreqmainfab(Double bdwasteSumamtreqmainfab) {
        this.bdwasteSumamtreqmainfab = bdwasteSumamtreqmainfab;
    }

    @Column(name = "`BDWASTE_AMTREQWITHOUTBD`", nullable = true, scale = 2, precision = 8)
    public Double getBdwasteAmtreqwithoutbd() {
        return this.bdwasteAmtreqwithoutbd;
    }

    public void setBdwasteAmtreqwithoutbd(Double bdwasteAmtreqwithoutbd) {
        this.bdwasteAmtreqwithoutbd = bdwasteAmtreqwithoutbd;
    }

    @Column(name = "`PCSAFTSURP`", nullable = true, scale = 0, precision = 7)
    public Integer getPcsaftsurp() {
        return this.pcsaftsurp;
    }

    public void setPcsaftsurp(Integer pcsaftsurp) {
        this.pcsaftsurp = pcsaftsurp;
    }

    @Column(name = "`GENWASTE_SELVPC`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteSelvpc() {
        return this.genwasteSelvpc;
    }

    public void setGenwasteSelvpc(Float genwasteSelvpc) {
        this.genwasteSelvpc = genwasteSelvpc;
    }

    @Column(name = "`GENWASTE_INISTDAOP`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteInistdaop() {
        return this.genwasteInistdaop;
    }

    public void setGenwasteInistdaop(Float genwasteInistdaop) {
        this.genwasteInistdaop = genwasteInistdaop;
    }

    @Column(name = "`GENWASTE_STDAOP`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteStdaop() {
        return this.genwasteStdaop;
    }

    public void setGenwasteStdaop(Float genwasteStdaop) {
        this.genwasteStdaop = genwasteStdaop;
    }

    @Column(name = "`GENWASTE_BRUSH`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteBrush() {
        return this.genwasteBrush;
    }

    public void setGenwasteBrush(Float genwasteBrush) {
        this.genwasteBrush = genwasteBrush;
    }

    @Column(name = "`GENWASTE_STDDYEFN`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteStddyefn() {
        return this.genwasteStddyefn;
    }

    public void setGenwasteStddyefn(Float genwasteStddyefn) {
        this.genwasteStddyefn = genwasteStddyefn;
    }

    @Column(name = "`GENWASTE_LSHEARING`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteLshearing() {
        return this.genwasteLshearing;
    }

    public void setGenwasteLshearing(Float genwasteLshearing) {
        this.genwasteLshearing = genwasteLshearing;
    }

    @Column(name = "`GENWASTE_HSHEARING`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteHshearing() {
        return this.genwasteHshearing;
    }

    public void setGenwasteHshearing(Float genwasteHshearing) {
        this.genwasteHshearing = genwasteHshearing;
    }

    @Column(name = "`GENWASTE_BRUSHREQ`", nullable = true, length = 1)
    public String getGenwasteBrushreq() {
        return this.genwasteBrushreq;
    }

    public void setGenwasteBrushreq(String genwasteBrushreq) {
        this.genwasteBrushreq = genwasteBrushreq;
    }

    @Column(name = "`GENWASTE_INITOTAL`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteInitotal() {
        return this.genwasteInitotal;
    }

    public void setGenwasteInitotal(Float genwasteInitotal) {
        this.genwasteInitotal = genwasteInitotal;
    }

    @Column(name = "`GENWASTE_INTFMW`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteIntfmw() {
        return this.genwasteIntfmw;
    }

    public void setGenwasteIntfmw(Float genwasteIntfmw) {
        this.genwasteIntfmw = genwasteIntfmw;
    }

    @Column(name = "`GENWASTE_INTFMW_SCNRID`", nullable = true, length = 15)
    public String getGenwasteIntfmwScnrid() {
        return this.genwasteIntfmwScnrid;
    }

    public void setGenwasteIntfmwScnrid(String genwasteIntfmwScnrid) {
        this.genwasteIntfmwScnrid = genwasteIntfmwScnrid;
    }

    @Column(name = "`GENWASTE_INTFMW_INIDYAMTREQ`", nullable = true, scale = 2, precision = 8)
    public Double getGenwasteIntfmwInidyamtreq() {
        return this.genwasteIntfmwInidyamtreq;
    }

    public void setGenwasteIntfmwInidyamtreq(Double genwasteIntfmwInidyamtreq) {
        this.genwasteIntfmwInidyamtreq = genwasteIntfmwInidyamtreq;
    }

    @Column(name = "`GENWASTE_INTFMW_INIDYKGREQ`", nullable = true, scale = 2, precision = 8)
    public Double getGenwasteIntfmwInidykgreq() {
        return this.genwasteIntfmwInidykgreq;
    }

    public void setGenwasteIntfmwInidykgreq(Double genwasteIntfmwInidykgreq) {
        this.genwasteIntfmwInidykgreq = genwasteIntfmwInidykgreq;
    }

    @Column(name = "`GENWASTE_INTFMW_INIKNTAMT`", nullable = true, scale = 2, precision = 8)
    public Double getGenwasteIntfmwInikntamt() {
        return this.genwasteIntfmwInikntamt;
    }

    public void setGenwasteIntfmwInikntamt(Double genwasteIntfmwInikntamt) {
        this.genwasteIntfmwInikntamt = genwasteIntfmwInikntamt;
    }

    @Column(name = "`GENWASTE_INTFMW_ININWASTE`", nullable = true, scale = 2, precision = 8)
    public Double getGenwasteIntfmwIninwaste() {
        return this.genwasteIntfmwIninwaste;
    }

    public void setGenwasteIntfmwIninwaste(Double genwasteIntfmwIninwaste) {
        this.genwasteIntfmwIninwaste = genwasteIntfmwIninwaste;
    }

    @Column(name = "`GENWASTE_INTFMW_NUMBATCH`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteIntfmwNumbatch() {
        return this.genwasteIntfmwNumbatch;
    }

    public void setGenwasteIntfmwNumbatch(Float genwasteIntfmwNumbatch) {
        this.genwasteIntfmwNumbatch = genwasteIntfmwNumbatch;
    }

    @Column(name = "`GENWASTE_INTFMW_DYAMTRMV`", nullable = true, scale = 2, precision = 8)
    public Double getGenwasteIntfmwDyamtrmv() {
        return this.genwasteIntfmwDyamtrmv;
    }

    public void setGenwasteIntfmwDyamtrmv(Double genwasteIntfmwDyamtrmv) {
        this.genwasteIntfmwDyamtrmv = genwasteIntfmwDyamtrmv;
    }

    @Column(name = "`GENWASTE_SUEDE`", nullable = true, scale = 2, precision = 6)
    public Float getGenwasteSuede() {
        return this.genwasteSuede;
    }

    public void setGenwasteSuede(Float genwasteSuede) {
        this.genwasteSuede = genwasteSuede;
    }

    @Column(name = "`GENWASTE_CALCMETHOD`", nullable = true, length = 2)
    public String getGenwasteCalcmethod() {
        return this.genwasteCalcmethod;
    }

    public void setGenwasteCalcmethod(String genwasteCalcmethod) {
        this.genwasteCalcmethod = genwasteCalcmethod;
    }

    @Column(name = "`AMTREQ_CALCMETHOD`", nullable = true, length = 2)
    public String getAmtreqCalcmethod() {
        return this.amtreqCalcmethod;
    }

    public void setAmtreqCalcmethod(String amtreqCalcmethod) {
        this.amtreqCalcmethod = amtreqCalcmethod;
    }

    @Column(name = "`BAKBEF_M1_GENWASTE`", nullable = true, scale = 2, precision = 6)
    public Float getBakbefM1Genwaste() {
        return this.bakbefM1Genwaste;
    }

    public void setBakbefM1Genwaste(Float bakbefM1Genwaste) {
        this.bakbefM1Genwaste = bakbefM1Genwaste;
    }

    @Column(name = "`BAKBEF_M1_NWASTE`", nullable = true, scale = 2, precision = 6)
    public Float getBakbefM1Nwaste() {
        return this.bakbefM1Nwaste;
    }

    public void setBakbefM1Nwaste(Float bakbefM1Nwaste) {
        this.bakbefM1Nwaste = bakbefM1Nwaste;
    }

    @Column(name = "`PCSREQUIREDADJ`", nullable = true, scale = 0, precision = 5)
    public Short getPcsrequiredadj() {
        return this.pcsrequiredadj;
    }

    public void setPcsrequiredadj(Short pcsrequiredadj) {
        this.pcsrequiredadj = pcsrequiredadj;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns(value = {
            @JoinColumn(name = "`ORDNUM`", referencedColumnName = "`ORDNUM`", insertable = false, updatable = false),
            @JoinColumn(name = "`CLRSEQ`", referencedColumnName = "`CLRSEQ`", insertable = false, updatable = false)},
        foreignKey = @ForeignKey(name = "`FK_ORT160_2`"))
    @Fetch(FetchMode.JOIN)
    public Ort140 getOrt140() {
        return this.ort140;
    }

    public void setOrt140(Ort140 ort140) {
        if(ort140 != null) {
            this.ordnum = ort140.getOrdnum();
            this.clrseq = ort140.getClrseq();
        }

        this.ort140 = ort140;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns(value = {
            @JoinColumn(name = "`ORDNUM`", referencedColumnName = "`ORDNUM`", insertable = false, updatable = false),
            @JoinColumn(name = "`FABSEQ`", referencedColumnName = "`FABSEQ`", insertable = false, updatable = false)},
        foreignKey = @ForeignKey(name = "`FK_ORT160_1`"))
    @Fetch(FetchMode.JOIN)
    public Ort120 getOrt120() {
        return this.ort120;
    }

    public void setOrt120(Ort120 ort120) {
        if(ort120 != null) {
            this.ordnum = ort120.getOrdnum();
            this.fabseq = ort120.getFabseq();
        }

        this.ort120 = ort120;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ort160)) return false;
        final Ort160 ort160 = (Ort160) o;
        return Objects.equals(getOrdnum(), ort160.getOrdnum()) &&
                Objects.equals(getClrseq(), ort160.getClrseq()) &&
                Objects.equals(getFabseq(), ort160.getFabseq());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrdnum(),
                getClrseq(),
                getFabseq());
    }
}

