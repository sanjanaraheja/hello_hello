/**
 * This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.
 */
package com.hello_hello.settimezone.controller;

import com.hello_hello.settimezone.SetTimeZone;
import java.lang.String;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

@RestController
@RequestMapping(value = "/setTimeZone")
public class SetTimeZoneController {

    @Autowired
    private SetTimeZone setTimeZone;

    @RequestMapping(value = "/sampleJavaOperation", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "")
    public void sampleJavaOperation(@RequestParam(value = "name", required = false) String name, HttpServletRequest request) {
        setTimeZone.sampleJavaOperation(name, request);
    }
}
